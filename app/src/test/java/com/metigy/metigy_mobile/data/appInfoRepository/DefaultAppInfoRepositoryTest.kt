package com.metigy.metigy_mobile.data.appInfoRepository

import com.google.common.truth.Truth.assertThat
import com.metigy.metigy_mobile.core.utils.HostInfoUtils
import com.metigy.metigy_mobile.data.appmodels.AppVersionModel
import com.metigy.metigy_mobile.data.appmodels.Version
import com.metigy.metigy_mobile.ui.contentlist.SmAppAndInfo
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.impl.annotations.MockK
import org.junit.After
import org.junit.Before
import org.junit.Test

class DefaultAppInfoRepositoryTest {

    @MockK
    lateinit var mockHostInfoUtils: HostInfoUtils

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    @Test
    fun appVersion() {
        val appInfoRepository = DefaultAppInfoRepository(hostInfoUtils = mockHostInfoUtils)
        val appVersion = AppVersionModel(30, Version("3.2.2"))
        every { appInfoRepository.appVersion() } returns appVersion
        assertThat(appInfoRepository.appVersion()).isEqualTo(appVersion)
    }

    @Test
    fun isAppInstalled_true() {
        val appInfoRepository = DefaultAppInfoRepository(hostInfoUtils = mockHostInfoUtils)
        every { mockHostInfoUtils.isAppInstalled(any()) } returns true
        assertThat(appInfoRepository.isAppInstalled(SmAppAndInfo.FACEBOOK)).isTrue()
    }

    @Test
    fun isAppInstalled_false() {
        val appInfoRepository = DefaultAppInfoRepository(hostInfoUtils = mockHostInfoUtils)
        every { mockHostInfoUtils.isAppInstalled(any()) } returns false
        assertThat(appInfoRepository.isAppInstalled(SmAppAndInfo.FACEBOOK)).isFalse()
    }

    @Test
    fun getHelpUrl() {
        val appInfoRepository = DefaultAppInfoRepository(hostInfoUtils = mockHostInfoUtils)
        assertThat(appInfoRepository.helpUrl).isEqualTo("https://metigy.com/support-articles/faqs-tips/faqs/")
    }

    @Test
    fun getContactEmailUrl() {
        val appInfoRepository = DefaultAppInfoRepository(hostInfoUtils = mockHostInfoUtils)
        assertThat(appInfoRepository.contactEmailUrl).isEqualTo("mobile.product@metigy.com")
    }

    @Test
    fun getLearnMoreUrlForFacebookLogin() {
        val appInfoRepository = DefaultAppInfoRepository(hostInfoUtils = mockHostInfoUtils)
        assertThat(appInfoRepository.learnMoreUrlForFacebookLogin).isEqualTo("https://metigy.com/support/why-do-i-have-to-login-to-facebook-to-connect-my-instagram-account/")
    }

    @Test
    fun getLearnMoreUrlForInstagramContentPublishing() {
        val appInfoRepository = DefaultAppInfoRepository(hostInfoUtils = mockHostInfoUtils)
        assertThat(appInfoRepository.learnMoreUrlForInstagramContentPublishing)
            .isEqualTo("https://metigy.com/support/how-does-instagram-content-publishing-work-with-metigy/")
    }
}