package com.metigy.metigy_mobile.data.authentication

import com.google.common.truth.Truth.assertThat
import com.metigy.metigy_mobile.core.extension.toUriWrapper
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.core.utils.TimeProvider
import com.metigy.metigy_mobile.core.utils.UriWrapper
import com.metigy.metigy_mobile.core.utils.UrlProvider
import com.metigy.metigy_mobile.data.apimodels.LoginResponse
import com.metigy.metigy_mobile.data.apimodels.LoginResult
import com.metigy.metigy_mobile.data.apimodels.UserInfoResponse
import com.metigy.metigy_mobile.data.apimodels.UserInfoResult
import com.metigy.metigy_mobile.data.apiservice.ApiServiceForAppUrl
import com.metigy.metigy_mobile.data.appcache.AppCache
import com.metigy.metigy_mobile.data.appmodels.AuthenticationRequirementStatus
import com.metigy.metigy_mobile.data.appmodels.UserCredentialsModel
import com.metigy.metigy_mobile.data.appmodels.UserInfoModel
import com.metigy.metigy_mobile.data.authentication.DefaultAuthentication.Companion.VERIFICATION_KEY_HEADER_FIELD
import com.metigy.metigy_mobile.data.authentication.DefaultAuthentication.Companion.VERIFICATION_KEY_QUERY_PARAM
import com.metigy.metigy_mobile.data.authentication.DefaultAuthentication.Companion.VERIFICATION_KEY_REQUEST_METHOD
import com.metigy.metigy_mobile.data.preferences.AppPreferences
import com.metigy.metigy_mobile.data.session.Session
import io.mockk.*
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.net.HttpURLConnection
import java.net.URL
import java.time.Instant

class DefaultAuthenticationTest {

    @MockK(relaxed = true)
    private lateinit var mockApiService: ApiServiceForAppUrl

    @MockK(relaxed = true)
    private lateinit var mockPreferences: AppPreferences

    @MockK(relaxed = true)
    private lateinit var mockAppCache: AppCache

    @MockK(relaxed = true)
    private lateinit var mockSession: Session

    @MockK(relaxed = true)
    private lateinit var mockTimeProvider: TimeProvider

    @MockK(relaxed = true)
    private lateinit var mockUrlProvider: UrlProvider

    private lateinit var authentication: DefaultAuthentication

    private lateinit var spyAuthentication: DefaultAuthentication

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        authentication = DefaultAuthentication(
            apiService = mockApiService,
            preferences = mockPreferences,
            appCache = mockAppCache,
            session = mockSession,
            timeProvider = mockTimeProvider,
            urlProvider = mockUrlProvider
        )

        spyAuthentication = spyk(authentication)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    @Test
    fun getToken_when_null() {
        every { mockPreferences.getUserToken() } returns null
        assertThat(authentication.token).isNull()
    }

    @Test
    fun getToken_when_non_null() {
        every { mockPreferences.getUserToken() } returns "test_token"
        assertThat(authentication.token).isEqualTo("test_token")
    }


    @Test
    fun isUserLoggedInAtLaunch_token_null() {
        every { mockPreferences.getUserToken() } returns null
        assertThat(authentication.isUserLoggedInAtLaunch()).isFalse()
    }

    @Test
    fun isUserLoggedInAtLaunch_credentials_null() {
        every { mockPreferences.getUserToken() } returns null
        assertThat(authentication.isUserLoggedInAtLaunch()).isFalse()
    }

    @Test
    fun isUserLoggedInAtLaunch_credentials_and_token_available() {
        every { mockPreferences.getUserToken() } returns "test_token"
        every { mockPreferences.getUserCredentials() } returns UserCredentialsModel("test_user", "test_password")
        assertThat(authentication.isUserLoggedInAtLaunch()).isTrue()
    }

    @Test
    fun login_successful() = runBlocking {
        val loginResponse = LoginResponse(result = LoginResult(scj = "test_scj"))
        coEvery { mockApiService.login(any()) } returns loginResponse
        assertThat(authentication.login("email", "password")).isEqualTo(Result.Success("JWT test_scj"))
    }


    @Test
    fun login_scj_null() = runBlocking {
        val loginResponse = LoginResponse(result = LoginResult(scj = null))
        coEvery { mockApiService.login(any()) } returns loginResponse
        assertThat(authentication.login("email", "password")).isInstanceOf(Result.Error::class.java)
    }


    @Test
    fun login_api_error() = runBlocking {
        coEvery { mockApiService.login(any()) } throws Exception()
        assertThat(authentication.login("email", "password")).isInstanceOf(Result.Error::class.java)
    }


    @Test
    fun reAuthenticate_successful() = runBlocking {
        val userName = "userName"
        val password = "password"
        every { mockPreferences.getUserCredentials() } returns UserCredentialsModel(userName, password)

        spyAuthentication.reAuthenticate()

        coVerify {
            spyAuthentication.login(email = userName, password = password)
        }
    }

    @Test
    fun reAuthenticate_no_credential_available() = runBlocking {
        every { mockPreferences.getUserCredentials() } returns null
        runBlocking {
            spyAuthentication.reAuthenticate()
        }
        coVerify(exactly = 0) {
            spyAuthentication.login(email = "", password = "")
        }

        assertThat(spyAuthentication.reAuthenticate()).isInstanceOf(Result.Error::class.java)
    }


    @Test
    fun saveToken() {
        val token = "test_token"
        authentication.saveToken(token)
        verifyAll {
            mockSession.onLogin()
            mockPreferences.saveUserToken(token)
            mockPreferences.saveLastAuthenticationDate(mockTimeProvider.now())
        }
    }

    @Test
    fun saveLoginCredentials() {
        val credentials = UserCredentialsModel("username", "password")
        authentication.saveLoginCredentials(credentials)
        verify {
            mockPreferences.saveUserCredential(credentials)
        }
    }

    @Test
    fun clearLoginCredentials() {
        authentication.clearLoginCredentials()
        verify {
            mockPreferences.clearCredentials()
        }
    }

    @Test
    fun cacheSignUpCredentials() {
        val credentials = UserCredentialsModel("username", "password")
        authentication.cacheSignUpCredentials(credentials)
        verify {
            mockAppCache.saveSignUpCredentials(credentials)
        }
    }

    @Test
    fun getCachedSignUpCredentials_no_credentials_available() {
        every { mockAppCache.getSignUpCredentials() } returns null
        assertThat(authentication.getCachedSignUpCredentials()).isNull()
    }

    @Test
    fun getCachedSignUpCredentials_credentials_available() {
        val credentials = UserCredentialsModel("username", "password")
        every { mockAppCache.getSignUpCredentials() } returns credentials
        assertThat(authentication.getCachedSignUpCredentials()).isEqualTo(credentials)
    }

    @Test
    fun clearSignUpCredentialsCache() {
        authentication.clearSignUpCredentialsCache()
        verify {
            mockAppCache.clearSignUpCredentials()
        }
    }

    @Test
    fun logout() = runBlocking {
        val token = "test_token"
        every { spyAuthentication.token } returns token

        spyAuthentication.logout()

        coVerifyAll {
            mockPreferences.clearToken()
            mockPreferences.setPushInfoSent(false)
            mockApiService.logout(token)
        }
    }

    @Test
    fun lastAuthenticationDate() {
        val date = Instant.ofEpochMilli(123456)
        every { mockPreferences.getLastAuthenticationDate() } returns date
        assertThat(authentication.lastAuthenticationDate()).isEqualTo(date)
    }

    @Test
    fun lastAuthenticationDate_null() {
        every { mockPreferences.getLastAuthenticationDate() } returns null
        assertThat(authentication.lastAuthenticationDate()).isNull()
    }

    @Test
    fun authenticationStatus_token_and_credentials_available() {
        val credentials = UserCredentialsModel("username", password = "password")
        every { mockPreferences.getUserCredentials() } returns credentials
        every { spyAuthentication.token } returns "test_token"
        assertThat(spyAuthentication.authenticationStatus()).isEqualTo(AuthenticationRequirementStatus.AuthenticationNotNeeded)
    }

    @Test
    fun authenticationStatus_token_null_credentials_available() {
        val credentials = UserCredentialsModel("username", password = "password")
        every { mockPreferences.getUserCredentials() } returns credentials
        every { spyAuthentication.token } returns null
        assertThat(spyAuthentication.authenticationStatus()).isEqualTo(AuthenticationRequirementStatus.AuthenticationNeededCredentialsAvailable(userCredentials = credentials))
    }

    @Test
    fun authenticationStatus_token_available_credentials_null() {
        every { mockPreferences.getUserCredentials() } returns null
        every { spyAuthentication.token } returns "test_token"
        assertThat(spyAuthentication.authenticationStatus()).isEqualTo(AuthenticationRequirementStatus.AuthenticationNeededCredentialsNotAvailable)
    }

    @Test
    fun signUp_successful() = runBlocking {
        val firstName = "test_first_name"
        val lastName = "test_last_name"
        val email = "test_email"
        val password = "test_password"

        val id = 101
        val isVerified = false

        coEvery { mockApiService.signUp(any()) } returns UserInfoResponse(
            result = UserInfoResult(
                firstName = firstName,
                lastName = lastName,
                email = email,
                id = id,
                isVerified = isVerified
            )
        )

        assertThat(authentication.signUp(firstName = firstName, lastName = lastName, email = email, password = password)).isEqualTo(
            Result.Success(UserInfoModel(email = email, firstName = firstName, lastName = lastName, id = id, isVerified = isVerified))
        )
    }


    @Test
    fun signUp_api_error() = runBlocking {
        val firstName = "test_first_name"
        val lastName = "test_last_name"
        val email = "test_email"
        val password = "test_password"

        coEvery { mockApiService.signUp(any()) } throws Exception()

        assertThat(authentication.signUp(firstName = firstName, lastName = lastName, email = email, password = password)).isInstanceOf(Result.Error::class.java)
    }


    @Test
    fun signUp_invalid_result() = runBlocking {
        val firstName = "test_first_name"
        val lastName = "test_last_name"
        val email = "test_email"
        val password = "test_password"

        val isVerified = false

        coEvery { mockApiService.signUp(any()) } returns UserInfoResponse(
            result = UserInfoResult(
                firstName = firstName,
                lastName = lastName,
                email = email,
                id = null,
                isVerified = isVerified
            )
        )

        assertThat(authentication.signUp(firstName = firstName, lastName = lastName, email = email, password = password)).isInstanceOf(Result.Error::class.java)
    }


    @Test
    fun signUp_null_result() = runBlocking {
        val firstName = "test_first_name"
        val lastName = "test_last_name"
        val email = "test_email"
        val password = "test_password"

        coEvery { mockApiService.signUp(any()) } returns UserInfoResponse(result = null)

        assertThat(authentication.signUp(firstName = firstName, lastName = lastName, email = email, password = password)).isInstanceOf(Result.Error::class.java)
    }

    @Test
    fun getVerificationKey() = runBlocking {
        mockkStatic("com.metigy.metigy_mobile.core.extension.CommonKt") {
            val url = "test_url"
            val testKey = "test_key"
            val mockConnection = mockk<HttpURLConnection>(relaxed = true)
            val mockUrl = mockk<URL>(relaxed = true)
            val mockUriWrapper = mockk<UriWrapper>(relaxed = true)

            every { mockUrlProvider.url(url) } returns mockUrl
            every { mockUrl.openConnection() } returns mockConnection
            every { mockConnection.getHeaderField(VERIFICATION_KEY_HEADER_FIELD) } returns ""
            every { any<String>().toUriWrapper() } returns mockUriWrapper
            every { mockUriWrapper.getQueryParameter(any()) } returns testKey

            assertThat(authentication.getVerificationKey(url)).isEqualTo(Result.Success(testKey))
        }
    }

    @Test
    fun getVerificationKey_url_error() = runBlocking {
        val mockUrl = mockk<URL>(relaxed = true)
        every { mockUrlProvider.url(any()) } returns mockUrl
        every { mockUrl.openConnection() } throws Exception()
        assertThat(authentication.getVerificationKey("url")).isInstanceOf(Result.Error::class.java)
    }

    @Test
    fun verifyEmail_successful() = runBlocking {
        assertThat(authentication.verifyEmail("key", "token")).isEqualTo(Result.Success(Unit))
    }

    @Test
    fun verifyEmail_api_error() = runBlocking {
        coEvery { mockApiService.verifyEmail(any(), any()) } throws Exception()
        assertThat(authentication.verifyEmail("key", "token")).isInstanceOf(Result.Error::class.java)
    }

    @Test
    fun resendEmail_successful() = runBlocking {
        assertThat(authentication.resendEmail("key", "token")).isEqualTo(Result.Success(Unit))
    }


    @Test
    fun resendEmail_api_error() = runBlocking {
        coEvery { mockApiService.resendEmail(any(), any()) } throws Exception()

        assertThat(authentication.resendEmail("key", "token")).isInstanceOf(Result.Error::class.java)
    }

    @Test
    fun resetPassword_successful() = runBlocking {
        assertThat(authentication.resetPassword("email")).isEqualTo(Result.Success(Unit))
    }

    @Test
    fun resetPassword_api_error() = runBlocking {
        coEvery { mockApiService.resetPassword(any()) } throws Exception()
        assertThat(authentication.resetPassword("email")).isInstanceOf(Result.Error::class.java)
    }


    @Test
    fun constants() {
        assertThat(VERIFICATION_KEY_REQUEST_METHOD).isEqualTo("GET")
        assertThat(VERIFICATION_KEY_HEADER_FIELD).isEqualTo("Location")
        assertThat(VERIFICATION_KEY_QUERY_PARAM).isEqualTo("key")
    }
}