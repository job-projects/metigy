package com.metigy.metigy_mobile.data.appconfigrepository

import com.google.common.truth.Truth.assertThat
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.core.utils.TimezoneProvider
import com.metigy.metigy_mobile.data.apiservice.ApiServiceForAppUrl
import com.metigy.metigy_mobile.data.preferences.AppPreferences
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.time.Instant

class DefaultAppConfigTest {

    @MockK(relaxed = true)
    lateinit var mockApiService: ApiServiceForAppUrl

    @MockK(relaxed = true)
    lateinit var mockPreferences: AppPreferences

    @MockK(relaxed = true)
    lateinit var mockTimezoneProvider: TimezoneProvider

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    @Test
    fun getAppConfig_when_200() = runBlocking {
        val appConfig = DefaultAppConfig(apiService = mockApiService, preferences = mockPreferences, timezoneProvider = mockTimezoneProvider)
        assertThat(appConfig.getAppConfig()).isInstanceOf(Result.Success::class.java)
    }

    @Test
    fun getAppConfig_when_error() = runBlocking {
        val appConfig = DefaultAppConfig(apiService = mockApiService, preferences = mockPreferences, timezoneProvider = mockTimezoneProvider)

        coEvery { mockApiService.getAppConfig() } throws Exception()

        assertThat(appConfig.getAppConfig()).isInstanceOf(Result.Error::class.java)
    }


    @Test
    fun latestPromptCheckDate_returns_non_null() {
        val appConfig = DefaultAppConfig(apiService = mockApiService, preferences = mockPreferences, timezoneProvider = mockTimezoneProvider)
        val instant = Instant.ofEpochSecond(1234567)
        every { mockPreferences.getLatestPromptDate() } returns instant

        assertThat(appConfig.latestPromptCheckDate()).isEqualTo(instant)
    }

    @Test
    fun latestPromptCheckDate_returns_null() {
        val appConfig = DefaultAppConfig(apiService = mockApiService, preferences = mockPreferences, timezoneProvider = mockTimezoneProvider)
        every { mockPreferences.getLatestPromptDate() } returns null

        assertThat(appConfig.latestPromptCheckDate()).isNull()
    }
}