package com.metigy.metigy_mobile.testutil.fake

import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.data.appconfigrepository.AppConfig
import com.metigy.metigy_mobile.data.appmodels.AndroidAppConfigModel
import java.time.Instant

class FakeAppConfig : AppConfig {
    override suspend fun getAppConfig(): Result<AndroidAppConfigModel> {
        TODO("Not yet implemented")
    }

    override fun latestPromptCheckDate(): Instant? {
        TODO("Not yet implemented")
    }

}