package com.metigy.metigy_mobile.testutil.fake

import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.data.appmodels.AuthenticationRequirementStatus
import com.metigy.metigy_mobile.data.appmodels.UserCredentialsModel
import com.metigy.metigy_mobile.data.appmodels.UserInfoModel
import com.metigy.metigy_mobile.data.authentication.Authentication
import com.metigy.metigy_mobile.testutil.TEST_ERROR_MESSAGE
import com.metigy.metigy_mobile.testutil.TEST_JWT_TOKEN
import java.time.Instant

class FakeAuthentication : Authentication {
    var userToken: String? = null

    var shouldReturnErrorOnLogin = false

    var shouldReturnErrorOnGetVerificationKey = false

    var shouldReturnErrorOnVerifyEmail = false

    var shouldReturnErrorOnSignUp = false

    private val isSignUpUserVerified = false

    var shouldReturnErrorOnResendEmail = false

    var shouldReturnErrorOnResetPassword = false

    var loginCredentials: UserCredentialsModel? = null

    var cachedSignUpCredentialsModel: UserCredentialsModel? = null

    override val token: String?
        get() = userToken

    override fun isUserLoggedInAtLaunch() = token != null && loginCredentials != null

    override suspend fun login(email: String, password: String): Result<String> {
        return if (shouldReturnErrorOnLogin) {
            Result.Error(message = TEST_ERROR_MESSAGE)
        } else {
            Result.Success(TEST_JWT_TOKEN)
        }
    }

    override suspend fun reAuthenticate(): Result<String> = login("some_email", "some_password")

    override fun saveToken(token: String) {
        userToken = token
    }

    override fun saveLoginCredentials(loginCredentials: UserCredentialsModel) {
        this.loginCredentials = loginCredentials
    }

    override fun clearLoginCredentials() {
        loginCredentials = null
    }

    override fun cacheSignUpCredentials(signUpCredentials: UserCredentialsModel) {
        cachedSignUpCredentialsModel = signUpCredentials
    }

    override fun getCachedSignUpCredentials(): UserCredentialsModel? = cachedSignUpCredentialsModel

    override fun clearSignUpCredentialsCache() {
        cachedSignUpCredentialsModel = null
    }

    override fun authenticationStatus(): AuthenticationRequirementStatus {
        return if (loginCredentials != null) {
            if (userToken != null) {
                AuthenticationRequirementStatus.AuthenticationNotNeeded
            } else {
                AuthenticationRequirementStatus.AuthenticationNeededCredentialsAvailable(loginCredentials!!)
            }
        } else {
            AuthenticationRequirementStatus.AuthenticationNeededCredentialsNotAvailable
        }
    }

    override fun lastAuthenticationDate(): Instant? {
        TODO("Not yet implemented")
    }

    override suspend fun logout() {
        TODO("Not yet implemented")
    }

    override suspend fun signUp(firstName: String, lastName: String, email: String, password: String): Result<UserInfoModel> {
        return if (shouldReturnErrorOnSignUp) {
            Result.Error(message = TEST_ERROR_MESSAGE)
        } else {
            Result.Success(UserInfoModel(email, firstName, 1, isSignUpUserVerified, lastName))
        }
    }

    override suspend fun getVerificationKey(url: String): Result<String> {
        return if (shouldReturnErrorOnGetVerificationKey) {
            Result.Error(message = TEST_ERROR_MESSAGE)
        } else {
            Result.Success(TEST_JWT_TOKEN)
        }
    }

    override suspend fun verifyEmail(key: String, token: String): Result<Unit> {
        return if (shouldReturnErrorOnVerifyEmail) {
            Result.Error(message = TEST_ERROR_MESSAGE)
        } else {
            Result.Success(Unit)
        }
    }

    override suspend fun resendEmail(token: String, email: String): Result<Unit> {
        return if (shouldReturnErrorOnResendEmail) {
            Result.Error(message = TEST_ERROR_MESSAGE)
        } else {
            Result.Success(Unit)
        }
    }

    override suspend fun resetPassword(email: String): Result<Unit> {
        return if (shouldReturnErrorOnResetPassword) {
            Result.Error(message = TEST_ERROR_MESSAGE)
        } else {
            Result.Success(Unit)
        }
    }

    override fun setIsPreviouslyLoggedIn() {}
}