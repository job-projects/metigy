package com.metigy.metigy_mobile.testutil.fake

import com.metigy.metigy_mobile.core.utils.TimeProvider
import java.time.Instant

object FakeTimeProvider {
    fun newInstance(epochSecond: Long) = object : TimeProvider {
        override fun nowAsEpochSecond(): Long = epochSecond
        override fun now(): Instant = Instant.ofEpochSecond(epochSecond)
    }
}