package com.metigy.metigy_mobile.testutil

const val TEST_JWT_TOKEN = "JWT some_token_here"

const val TEST_ERROR_MESSAGE = "An error occurred"