package com.metigy.metigy_mobile.testutil.fake

import com.metigy.metigy_mobile.data.appInfoRepository.AppInfoRepository
import com.metigy.metigy_mobile.data.appmodels.AppVersionModel
import com.metigy.metigy_mobile.ui.contentlist.SmAppAndInfo

class FakeAppInfoRepository :AppInfoRepository {
    override fun appVersion(): AppVersionModel {
        TODO("Not yet implemented")
    }

    override fun isAppInstalled(app: SmAppAndInfo): Boolean {
        TODO("Not yet implemented")
    }

    override val helpUrl: String
        get() = TODO("Not yet implemented")
    override val contactEmailUrl: String
        get() = TODO("Not yet implemented")
    override val learnMoreUrlForFacebookLogin: String
        get() = TODO("Not yet implemented")
    override val learnMoreUrlForInstagramContentPublishing: String
        get() = TODO("Not yet implemented")
}