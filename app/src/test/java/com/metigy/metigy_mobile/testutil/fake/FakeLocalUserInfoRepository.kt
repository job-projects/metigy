package com.metigy.metigy_mobile.testutil.fake

import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.data.appmodels.UserInfoModel
import com.metigy.metigy_mobile.data.authentication.LocalUserInfoRepository

class FakeLocalUserInfoRepository : LocalUserInfoRepository {

    private var userInfoModel: UserInfoModel? = null

    override fun getUserInfo(): Result<UserInfoModel> {
        return userInfoModel?.let {
            Result.Success(it)
        } ?: run {
            Result.Error(messageId = R.string.user_not_found)
        }

    }

    override fun saveUserInfo(userInfoModel: UserInfoModel) {
        this.userInfoModel = userInfoModel
    }
}