package com.metigy.metigy_mobile.testutil.fake

import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.data.appmodels.UserInfoModel
import com.metigy.metigy_mobile.data.authentication.RemoteUserInfoRepository
import com.metigy.metigy_mobile.testutil.TEST_ERROR_MESSAGE

class FakeRemoteUserInfoRepository : RemoteUserInfoRepository {

    var shouldReturnError = false

    var isUserVerified = false

    override suspend fun getUserInfo(token: String): Result<UserInfoModel> {
        return if (shouldReturnError) {
            Result.Error(message = TEST_ERROR_MESSAGE)
        } else {
            Result.Success(UserInfoModel(email = "jon@doe.com", id = 0, isVerified = isUserVerified))
        }
    }
}