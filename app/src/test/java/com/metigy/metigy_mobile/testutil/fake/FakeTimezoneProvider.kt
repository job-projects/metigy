package com.metigy.metigy_mobile.testutil.fake

import com.metigy.metigy_mobile.core.utils.TimezoneProvider
import java.time.ZoneId

object FakeTimezoneProvider {
    const val ZONE_ID_SYDNEY = "Australia/Sydney"
    const val ZONE_ID_TOKYO = "Asia/Tokyo"
    const val ZONE_ID_GMT = "GMT"

    fun newInstance(zoneId: String): TimezoneProvider {
        return object : TimezoneProvider {
            override fun systemDefaultTimeZoneId(): ZoneId = ZoneId.of(zoneId)
        }
    }
}