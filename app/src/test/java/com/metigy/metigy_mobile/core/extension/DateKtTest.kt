package com.metigy.metigy_mobile.core.extension

import com.google.common.truth.Truth.assertThat
import com.metigy.metigy_mobile.testutil.fake.FakeTimeProvider
import com.metigy.metigy_mobile.testutil.fake.FakeTimezoneProvider
import org.junit.Assert.assertThrows
import org.junit.Test
import java.time.*
import java.time.format.DateTimeFormatter

class DateKtTest {

    @Test
    fun dateTimePatternValidation() {
        //Just expect to not throw any exception
        ZonedDateTime.of(LocalDateTime.now(), ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern(DATE_TIME_PATTERN_SCHEDULED_PREVIEW))
        ZonedDateTime.of(LocalDateTime.now(), ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern(DATE_TIME_PATTERN_APP_CONFIG))
        ZonedDateTime.of(LocalDateTime.now(), ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern(DATE_TIME_PATTERN_CONTENT_DATE_SELECTION))
    }

    @Test
    fun inSecondsToInstant() {
        val seconds = 423412313L
        assertThat(seconds.inSecondsToInstant()).isEqualTo(Instant.ofEpochSecond(seconds))
    }

    @Test
    fun inSecondsToInstant_zero() {
        assertThat(0.toLong().inSecondsToInstant()).isEqualTo(Instant.ofEpochSecond(0))
    }

    @Test
    fun inSecondsToInstant_negative() {
        val seconds = -12L
        assertThat(seconds.inSecondsToInstant()).isEqualTo(Instant.ofEpochSecond(seconds))
    }

    @Test
    fun formatToScheduledPreview() {
        val instant = Instant.ofEpochSecond(1630362844)
        assertThat(instant.formatToScheduledPreview(FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_SYDNEY))).isEqualTo("8:34 AM")
        assertThat(instant.formatToScheduledPreview(FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_TOKYO))).isEqualTo("7:34 AM")
    }

    @Test
    fun formatToContentDateSelection() {
        val instant = Instant.ofEpochSecond(1630362844)
        assertThat(instant.formatToContentDateSelection(FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_SYDNEY))).isEqualTo("31 Aug")
        assertThat(instant.formatToContentDateSelection(FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_GMT))).isEqualTo("30 Aug")
    }

    @Test
    fun formatToString() {
        assertThat(ZonedDateTime.of(2021, 9, 1, 4, 39, 52, 0, ZoneId.of(FakeTimezoneProvider.ZONE_ID_SYDNEY)).formatToString("hh")).isEqualTo("04")
        assertThat(
            ZonedDateTime.of(2021, 9, 1, 4, 39, 52, 0, ZoneId.of(FakeTimezoneProvider.ZONE_ID_SYDNEY)).formatToString("yyyy-MM-dd HH:mm:ss")
        ).isEqualTo("2021-09-01 04:39:52")
    }

    @Test
    fun formatToString_from_zoned_date_time_with_invalid_pattern() {
        assertThrows(Exception::class.java) {
            ZonedDateTime.of(2021, 9, 1, 4, 39, 52, 0, ZoneId.of(FakeTimezoneProvider.ZONE_ID_SYDNEY)).formatToString("hhh")
        }
    }

    @Test
    fun formatToString_from_instant_pattern_1() {
        assertThat(
            Instant.ofEpochSecond(1631063318).formatToString(pattern = "h:mm a", timezoneProvider = FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_SYDNEY))
        ).isEqualTo("11:08 AM")
        assertThat(
            Instant.ofEpochSecond(1631063318).formatToString(pattern = "h:mm a", timezoneProvider = FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_GMT))
        ).isEqualTo("1:08 AM")
    }

    @Test
    fun formatToString_from_instant_pattern_2() {
        assertThat(
            Instant.ofEpochSecond(1631063318)
                .formatToString(pattern = "yyyy-MM-dd HH:mm:ss", timezoneProvider = FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_SYDNEY))
        ).isEqualTo("2021-09-08 11:08:38")
        assertThat(
            Instant.ofEpochSecond(1631063318).formatToString(pattern = "yyyy-MM-dd HH:mm:ss", timezoneProvider = FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_GMT))
        ).isEqualTo("2021-09-08 01:08:38")
    }

    @Test
    fun formatToString_from_instant_pattern_3() {
        assertThat(
            Instant.ofEpochSecond(1631063318).formatToString(pattern = "dd MMM", timezoneProvider = FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_SYDNEY))
        ).isEqualTo("08 Sep")
        assertThat(
            Instant.ofEpochSecond(1631063318).formatToString(pattern = "dd MMM", timezoneProvider = FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_GMT))
        ).isEqualTo("08 Sep")
    }

    @Test
    fun formatToString_from_instant_with_invalid_pattern() {
        assertThrows(Exception::class.java) {
            Instant.ofEpochSecond(1631063318).formatToString(pattern = "dd mmm", timezoneProvider = FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_SYDNEY))
        }
    }

    @Test
    fun toZonedDateTime_1() {
        assertThat("2021-09-27 13:21:06".toZonedDateTime("yyyy-MM-dd HH:mm:ss", FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_SYDNEY))).isEqualTo(
            ZonedDateTime.of(
                2021, 9, 27, 13, 21, 6, 0,
                ZoneId.of("Australia/Sydney")
            )
        )
    }

    @Test
    fun toZonedDateTime_2() {
        assertThat("2021-09-27 13:21:06".toZonedDateTime("yyyy-MM-dd HH:mm:ss", FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_TOKYO))).isEqualTo(
            ZonedDateTime.of(
                2021, 9, 27, 13, 21, 6, 0,
                ZoneId.of("Asia/Tokyo")
            )
        )
    }

    @Test
    fun toZonedDateTime_time_not_matching_pattern_1() {
        assertThat("2021/09/27 13:21:06".toZonedDateTime("yyyy-MM-dd HH:mm:ss", FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_TOKYO))).isNull()
    }

    @Test
    fun toZonedDateTime_time_not_matching_pattern_2() {
        assertThat("2021-09-27 9:21:06".toZonedDateTime("yyyy-MM-dd HH:mm:ss", FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_TOKYO))).isNull()
    }

    @Test
    fun toZonedDateTime_invalid_pattern() {
        assertThat("2021-09-27 9:21:06".toZonedDateTime("yyyy-MM-dd HH:mm:sccs", FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_TOKYO))).isNull()
    }

    @Test
    fun toInstant_1() {
        assertThat("2021-09-27 13:21:06".toInstant("yyyy-MM-dd HH:mm:ss", FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_GMT))).isEqualTo(
            Instant.ofEpochSecond(
                1632748866
            )
        )
    }

    @Test
    fun toInstant_2() {
        assertThat("2021-09-27 13:21:06".toInstant("yyyy-MM-dd HH:mm:ss", FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_SYDNEY))).isEqualTo(
            Instant.ofEpochSecond(
                1632712866
            )
        )
    }

    @Test
    fun toInstant_time_not_matching_pattern_1() {
        assertThat("2021-09-27 13:21:06".toInstant("yyyy/MM/dd HH:mm:ss", FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_SYDNEY))).isNull()
    }

    @Test
    fun toInstant_time_not_matching_pattern_2() {
        assertThat("2021-09-27 3:21:06".toInstant("yyyy/MM/dd HH:mm:ss", FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_SYDNEY))).isNull()
    }

    @Test
    fun toInstant_invalid_pattern() {
        assertThat("2021-09-27 3:21:06".toInstant("yyyy/MM/dd HH:mm:sccs", FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_SYDNEY))).isNull()
    }

    @Test
    fun dateTimeFormatter() {
        //Just expect to not throw any exception
        "yyyy/MM/dd HH:mm:ss".dateTimeFormatter()
        "dd MMM".dateTimeFormatter()
        "h:mm a".dateTimeFormatter()
    }

    @Test
    fun dateTimeFormatter_invalid_pattern() {
        assertThat("yyyy/MM/dd HH:mm:sccs".dateTimeFormatter()).isNull()
    }

    @Test
    fun isTodayAtDeviceZone() {
        assertThat(
            Instant.ofEpochSecond(1631141770).isTodayAtDeviceZone(FakeTimeProvider.newInstance(1631146414), FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_SYDNEY))
        ).isTrue()
        assertThat(
            Instant.ofEpochSecond(1631141770).isTodayAtDeviceZone(FakeTimeProvider.newInstance(1631146414), FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_GMT))
        ).isFalse()
    }

    @Test
    fun isSameDayAtDeviceZone_1() {
        //0:0:0 vs 23:59:59 at the same day
        assertThat(
            Instant.ofEpochSecond(1632664800).isSameDayAtDeviceZone(Instant.ofEpochSecond(1632751199), FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_SYDNEY))
        ).isTrue()
    }

    @Test
    fun isSameDayAtDeviceZone_2() {
        //23:59:59 vs 1 second after
        assertThat(
            Instant.ofEpochSecond(1632751199).isSameDayAtDeviceZone(Instant.ofEpochSecond(1632751200), FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_SYDNEY))
        ).isFalse()
    }

    @Test
    fun toLocalDate() {
        assertThat(Instant.ofEpochSecond(1631139175).toLocalDate(FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_SYDNEY))).isEqualTo(LocalDate.of(2021, 9, 9))
        assertThat(Instant.ofEpochSecond(1631139175).toLocalDate(FakeTimezoneProvider.newInstance(FakeTimezoneProvider.ZONE_ID_GMT))).isEqualTo(LocalDate.of(2021, 9, 8))
    }

    @Test
    fun constants() {
        assertThat(DATE_TIME_PATTERN_SCHEDULED_PREVIEW).isEqualTo("h:mm a")
        assertThat(DATE_TIME_PATTERN_APP_CONFIG).isEqualTo("yyyy-MM-dd HH:mm:ss")
        assertThat(DATE_TIME_PATTERN_CONTENT_DATE_SELECTION).isEqualTo("dd MMM")
    }
}