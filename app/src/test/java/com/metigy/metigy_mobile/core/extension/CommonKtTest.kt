package com.metigy.metigy_mobile.core.extension

import com.google.common.truth.Truth.assertThat
import com.metigy.metigy_mobile.data.appmodels.Version
import org.junit.Test

class CommonKtTest {

    @Test
    fun orZero() {
        assertThat(null.orZero()).isEqualTo(0)
        assertThat(1.orZero()).isEqualTo(1)
        assertThat((-1).orZero()).isEqualTo(-1)
    }

    @Test
    fun orFalse() {
        assertThat(null.orFalse()).isFalse()
        assertThat(false.orFalse()).isFalse()
        assertThat(true.orFalse()).isTrue()
    }

    @Test
    fun removeFirst() {
        assertThat(listOf<String>().removeFirst()).isEmpty()
        assertThat(listOf(2, 5, 4, 1).removeFirst()).isEqualTo(listOf(5, 4, 1))
        assertThat(listOf(2, 5, 4, 1).removeFirst()).isNotEqualTo(listOf(5, 1, 4))
        assertThat(listOf(5).removeFirst()).isEmpty()
    }

    @Test
    fun moveToFirst() {
        assertThat(listOf(2, 4, 1, 5).moveToFirst(4)).isEqualTo(arrayListOf(4, 2, 1, 5))
    }

    @Test
    fun moveToFirst_with_duplicates() {
        assertThat(listOf(2, 4, 1, 4, 5).moveToFirst(4)).isEqualTo(arrayListOf(4, 2, 1, 4, 5))
    }

    @Test
    fun moveToFirst_when_element_not_exist() {
        assertThat(listOf(2, 1, 5).moveToFirst(4)).isEqualTo(arrayListOf(2, 1, 5))
    }


    @Test
    fun toVersion() {
        assertThat("1.4".toVersion()).isEqualTo(Version("1.4"))
        val longVersion = "3.2.1212.423.55.3434.56.565.6666"
        assertThat(longVersion.toVersion()).isEqualTo(Version(longVersion))
        assertThat("0.1".toVersion()).isEqualTo(Version("0.1"))
        assertThat("0".toVersion()).isEqualTo(Version("0"))
        assertThat("1".toVersion()).isEqualTo(Version("1"))
        assertThat("1-2.3".toVersion()).isNull()
        assertThat("1.a".toVersion()).isNull()
        assertThat(".1.2".toVersion()).isNull()
        assertThat("1_2".toVersion()).isNull()
        assertThat("-2".toVersion()).isNull()
    }

    @Test
    fun toggleElement() {
        val mutableSet1 = mutableSetOf(2, 3, 9, 6, 1)
        assertThat(mutableSet1.apply { toggleElement(3) }).isEqualTo(mutableSetOf(2, 9, 6, 1))
        val mutableSet2 = mutableSetOf(2, 3, 9, 6, 1)
        assertThat(mutableSet2.apply { toggleElement(10) }).isEqualTo(mutableSetOf(2, 3, 9, 6, 1, 10))
        val mutableSet3 = mutableSetOf<Int>()
        assertThat(mutableSet3.apply { toggleElement(10) }).isEqualTo(mutableSetOf(10))
    }

    @Test
    fun valid_email() {
        assertThat("".isValidEmail()).isFalse()
        assertThat("jon".isValidEmail()).isFalse()
        assertThat("jon.doe".isValidEmail()).isFalse()
        assertThat("jon.doe@".isValidEmail()).isFalse()
        assertThat("jon.doe@gmail".isValidEmail()).isFalse()
        assertThat("jon.doe@gmail.c".isValidEmail()).isTrue()
        assertThat("jon.doe@gmail.co".isValidEmail()).isTrue()
        assertThat("jon.doe@gmail.com".isValidEmail()).isTrue()
        assertThat("jon.doe@metigy.com.au".isValidEmail()).isTrue()
    }

    @Test
    fun valid_password() {
        assertThat("".isValidPassword()).isFalse()
        assertThat("p".isValidPassword()).isFalse()
        assertThat("password".isValidPassword()).isFalse()
        assertThat("Password".isValidPassword()).isFalse()
        assertThat("Passwo1".isValidPassword()).isFalse()
        assertThat("ssasa@#%^1121".isValidPassword()).isFalse()
        assertThat("             ".isValidPassword()).isFalse()
        assertThat("pass             ".isValidPassword()).isFalse()
        assertThat("Passwor1".isValidPassword()).isTrue()
        assertThat("Pass   wor234".isValidPassword()).isTrue()
        assertThat("ssaPW@#21".isValidPassword()).isTrue()
        assertThat("PA55word       ".isValidPassword()).isTrue()
    }
}