package com.metigy.metigy_mobile.ui.emailverification

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.data.appcache.DefaultAppCache
import com.metigy.metigy_mobile.data.appmodels.UserCredentialsModel
import com.metigy.metigy_mobile.testutil.TEST_ERROR_MESSAGE
import com.metigy.metigy_mobile.testutil.fake.FakeAuthentication
import com.metigy.metigy_mobile.testutil.getOrAwaitValue
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ResendEmailViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var fakeAuthentication: FakeAuthentication
    private lateinit var appCache: DefaultAppCache

    private lateinit var viewModel: ResendEmailViewModel

    private val userCredentialsModel = UserCredentialsModel("email", "password")

    @Before
    fun setUp() {
        fakeAuthentication = FakeAuthentication()
        appCache = DefaultAppCache()
        viewModel = ResendEmailViewModel(fakeAuthentication, appCache)
    }

    @Test
    fun resendEmail_1() {
        // Test when appCache signUpCredentials is null
        viewModel.resendEmail()

        assertThat(viewModel.error.getOrAwaitValue().getContentIfNotHandled()?.messageId)
            .isEqualTo(R.string.unknown_error)
    }

    @Test
    fun resendEmail_2() {
        /* Test when appCache signUpCredentials is not null
         * And login returns error
         */

        appCache.saveSignUpCredentials(userCredentialsModel)

        fakeAuthentication.shouldReturnErrorOnLogin = true

        viewModel.resendEmail()

        assertThat(viewModel.error.getOrAwaitValue().getContentIfNotHandled()?.message)
            .isEqualTo(TEST_ERROR_MESSAGE)
    }

    @Test
    fun resendEmail_3() {
        /* Test when appCache signUpCredentials is not null
         * And login returns success
         * And authentication.resendEmail() returns error
         */

        appCache.saveSignUpCredentials(userCredentialsModel)

        fakeAuthentication.shouldReturnErrorOnResendEmail = true

        viewModel.resendEmail()

        assertThat(viewModel.error.getOrAwaitValue().getContentIfNotHandled()?.message)
            .isEqualTo(TEST_ERROR_MESSAGE)
    }

    @Test
    fun resendEmail_4() {
        /* Test when appCache signUpCredentials is not null
         * And login returns success
         * And authentication.resendEmail() returns success
         */

        appCache.saveSignUpCredentials(userCredentialsModel)

        viewModel.resendEmail()

        assertThat(viewModel.displayEmailSentDialog.getOrAwaitValue()).isTrue()
    }

    @Test
    fun hideEmailSentDialog() {
        viewModel.hideEmailSentDialog()

        assertThat(viewModel.displayEmailSentDialog.getOrAwaitValue()).isFalse()
    }
}