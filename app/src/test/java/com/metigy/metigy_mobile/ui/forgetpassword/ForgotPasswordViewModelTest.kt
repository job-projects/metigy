package com.metigy.metigy_mobile.ui.forgetpassword

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.metigy.metigy_mobile.testutil.TEST_ERROR_MESSAGE
import com.metigy.metigy_mobile.testutil.captureValues
import com.metigy.metigy_mobile.testutil.fake.FakeAuthentication
import com.metigy.metigy_mobile.testutil.getOrAwaitValue
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ForgotPasswordViewModelTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var fakeAuthentication: FakeAuthentication

    private lateinit var viewModel: ForgotPasswordViewModel

    @Before
    fun setUp() {
        fakeAuthentication = FakeAuthentication()
        viewModel = ForgotPasswordViewModel(fakeAuthentication)
    }

    @Test
    fun resetPassword_1() {
        /* Test loading values are set
         * And authentication.resetPassword returns error
         */

        val loadingValues = viewModel.loading.captureValues()

        fakeAuthentication.shouldReturnErrorOnResetPassword = true

        viewModel.resetPassword("email")

        assertThat(loadingValues[0]).isTrue()
        assertThat(loadingValues[1]).isFalse()

        assertThat(viewModel.error.getOrAwaitValue().getContentIfNotHandled()?.message)
            .isEqualTo(TEST_ERROR_MESSAGE)
    }


    @Test
    fun resetPassword_2() {
        // Test when authentication.resetPassword returns success

        viewModel.resetPassword("email")

        assertThat(viewModel.navigateToMessageScreen.getOrAwaitValue().getContentIfNotHandled()).isEqualTo(Unit)
    }
}