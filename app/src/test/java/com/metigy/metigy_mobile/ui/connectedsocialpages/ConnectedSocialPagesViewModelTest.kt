package com.metigy.metigy_mobile.ui.connectedsocialpages

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.data.appInfoRepository.AppInfoRepository
import com.metigy.metigy_mobile.data.brandrepository.BrandRepository
import com.metigy.metigy_mobile.data.platformpagesrepository.PlatformPagesRepository
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ConnectedSocialPagesViewModelTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: ConnectedSocialPagesViewModel

    @MockK(relaxed = true)
    private lateinit var mockPlatformPagesRepository: PlatformPagesRepository

    @MockK(relaxed = true)
    private lateinit var mockBrandRepository: BrandRepository

    @MockK(relaxed = true)
    private lateinit var mockAppInfo: AppInfoRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    @Test
    fun getBrandGroupId_when_brand_group_id_is_cached() = runBlocking {
        coEvery { mockBrandRepository.listBrandGroups() } returns Result.Success(listOf())
        viewModel = ConnectedSocialPagesViewModel(
            platformPagesRepository = mockPlatformPagesRepository,
            brandRepository = mockBrandRepository,
            appInfo = mockAppInfo
        )
        viewModel.brandGroupId = 3
        viewModel.getBrandGroupId()
        coVerify(exactly = 1) { mockBrandRepository.listBrandGroups() }
    }

    @Test
    fun getBrandGroupId_when_brand_group_id_not_cached() = runBlocking {
        coEvery { mockBrandRepository.listBrandGroups() } returns Result.Success(listOf())
        viewModel = ConnectedSocialPagesViewModel(
            platformPagesRepository = mockPlatformPagesRepository,
            brandRepository = mockBrandRepository,
            appInfo = mockAppInfo
        )
        viewModel.getBrandGroupId()
        //1 call for constructor
        coVerify(exactly = 2) { mockBrandRepository.listBrandGroups() }
    }
}