package com.metigy.metigy_mobile.ui.login

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.metigy.metigy_mobile.data.appcache.DefaultAppCache
import com.metigy.metigy_mobile.data.appmodels.UserCredentialsModel
import com.metigy.metigy_mobile.testutil.TEST_ERROR_MESSAGE
import com.metigy.metigy_mobile.testutil.TEST_JWT_TOKEN
import com.metigy.metigy_mobile.testutil.fake.FakeAuthentication
import com.metigy.metigy_mobile.testutil.fake.FakeLocalUserInfoRepository
import com.metigy.metigy_mobile.testutil.fake.FakeRemoteUserInfoRepository
import com.metigy.metigy_mobile.testutil.getOrAwaitValue
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.impl.annotations.SpyK
import io.mockk.verify
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class LoginViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @SpyK
    private lateinit var fakeAuthentication: FakeAuthentication

    private lateinit var fakeRemoteUserInfoRepository: FakeRemoteUserInfoRepository

    @SpyK
    private lateinit var fakeLocalUserInfoRepository: FakeLocalUserInfoRepository

    @SpyK
    private lateinit var fakeAppCache: DefaultAppCache

    private lateinit var viewModel: LoginViewModel

    private val email = "test@email.com"
    private val password = "test_password"

    @Before
    fun setUp() {
        fakeAuthentication = FakeAuthentication()
        fakeRemoteUserInfoRepository = FakeRemoteUserInfoRepository()
        fakeLocalUserInfoRepository = FakeLocalUserInfoRepository()
        fakeAppCache = DefaultAppCache()

        MockKAnnotations.init(this)

        viewModel = LoginViewModel(fakeAuthentication, fakeRemoteUserInfoRepository, fakeLocalUserInfoRepository, fakeAppCache)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    @Test
    fun login_failure() {
        /* Should call saveCredentialsIfNeeded
         * Should set loading to false
         * Should return error when authentication.login() returns error
         */

        fakeAuthentication.shouldReturnErrorOnLogin = true

        viewModel.login(email, password, false)

        verify {
            viewModel.saveCredentialsIfNeeded(email, password, false)
        }

        assertThat(viewModel.loginLoading.getOrAwaitValue()).isFalse()

        assertThat(viewModel.loginErrorMessage.getOrAwaitValue().getContentIfNotHandled()?.message)
            .isEqualTo(TEST_ERROR_MESSAGE)
    }

    @Test
    fun login_success() {
        // Test when authentication.login() returns success

        viewModel.login(email, password, false)

        verify { viewModel.getUserInfo(email, password, TEST_JWT_TOKEN) }
    }

    @Test
    fun getUserInfo_success_for_unverified_user() {
        /* remoteUserInfoRepository.getUserInfo returns success
         * remoteUserInfoRepository.getUserInfo returns unverified user
         */

        assertThat(viewModel.navigateToEmailVerificationScreen.value).isNull()

        viewModel.getUserInfo(email, password, TEST_JWT_TOKEN)

        verify {
            fakeAppCache.saveSignUpCredentials(UserCredentialsModel(email, password))
        }

        assertThat(viewModel.navigateToEmailVerificationScreen.getOrAwaitValue().getContentIfNotHandled())
            .isEqualTo(Unit)
    }

    @Test
    fun getUserInfo_success_for_verified_user() {
        /* Test when authentication.login() returns success
         * remoteUserInfoRepository.getUserInfo returns success
         * remoteUserInfoRepository.getUserInfo returns verified user
         */

        assertThat(viewModel.navigateToHomeScreen.value).isNull()

        fakeRemoteUserInfoRepository.isUserVerified = true

        viewModel.getUserInfo(email, password, TEST_JWT_TOKEN)

        verify {
            fakeAuthentication.saveToken(any())
            fakeLocalUserInfoRepository.saveUserInfo(any())
        }

        assertThat(viewModel.navigateToHomeScreen.getOrAwaitValue().getContentIfNotHandled())
            .isEqualTo(Unit)
    }

    @Test
    fun getUserInfo_failure() {
        fakeRemoteUserInfoRepository.shouldReturnError = true

        viewModel.getUserInfo(email, password, TEST_JWT_TOKEN)

        assertThat(viewModel.loginErrorMessage.getOrAwaitValue().getContentIfNotHandled()?.message)
            .isEqualTo(TEST_ERROR_MESSAGE)
    }

    @Test
    fun saveCredentialsIfNeeded() {
        // Test when saveCredentials is true
        viewModel.saveCredentialsIfNeeded(email, password, true)
        verify {
            fakeAuthentication.saveLoginCredentials(UserCredentialsModel(email, password))
        }

        // Test when saveCredentials is false
        viewModel.saveCredentialsIfNeeded(email, password, false)
        verify {
            fakeAuthentication.clearLoginCredentials()
        }
    }
}