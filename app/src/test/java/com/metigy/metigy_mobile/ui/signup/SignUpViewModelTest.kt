package com.metigy.metigy_mobile.ui.signup

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.metigy.metigy_mobile.data.appmodels.UserCredentialsModel
import com.metigy.metigy_mobile.testutil.TEST_ERROR_MESSAGE
import com.metigy.metigy_mobile.testutil.captureValues
import com.metigy.metigy_mobile.testutil.fake.FakeAuthentication
import com.metigy.metigy_mobile.testutil.getOrAwaitValue
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.coVerify
import io.mockk.impl.annotations.SpyK
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class SignUpViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @SpyK
    private lateinit var fakeAuthentication: FakeAuthentication

    private lateinit var viewModel: SignUpViewModel

    private val firstName = "John"
    private val lastName = "Doe"
    private val email = "john@doe.com"
    private val password = "password"

    @Before
    fun setUp() {
        fakeAuthentication = FakeAuthentication()
        MockKAnnotations.init(this)
        viewModel = SignUpViewModel(fakeAuthentication)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    @Test
    fun signUpUser_1() {
        /* Test loading values are set
         * signUp and cacheSignUpCredentials are called
        **/

        val loadingValues = viewModel.loading.captureValues()

        viewModel.signUpUser(firstName, lastName, email, password)

        assertThat(loadingValues[0]).isTrue()
        assertThat(loadingValues[1]).isFalse()

        coVerify {
            fakeAuthentication.signUp(firstName, lastName, email, password)
            fakeAuthentication.cacheSignUpCredentials(UserCredentialsModel(email, password))
        }
    }

    @Test
    fun signUpUser_2() {
        // Test when signUp returns error

        fakeAuthentication.shouldReturnErrorOnSignUp = true

        viewModel.signUpUser(firstName, lastName, email, password)

        assertThat(viewModel.error.getOrAwaitValue().getContentIfNotHandled()?.message)
            .isEqualTo(TEST_ERROR_MESSAGE)
    }

    @Test
    fun signUpUser_3() {
        /* Test when signUp returns success
         * isVerified is false
         */

        viewModel.signUpUser(firstName, lastName, email, password)

        assertThat(viewModel.navigateToEmailVerification.getOrAwaitValue().getContentIfNotHandled()).isEqualTo(Unit)
    }
}