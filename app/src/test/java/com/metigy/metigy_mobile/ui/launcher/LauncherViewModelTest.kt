package com.metigy.metigy_mobile.ui.launcher

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.data.airship.NotificationContent
import com.metigy.metigy_mobile.data.appmodels.AuthenticationRequirementStatus
import com.metigy.metigy_mobile.data.appmodels.UserCredentialsModel
import com.metigy.metigy_mobile.testutil.TEST_ERROR_MESSAGE
import com.metigy.metigy_mobile.testutil.TEST_JWT_TOKEN
import com.metigy.metigy_mobile.testutil.fake.*
import com.metigy.metigy_mobile.testutil.getOrAwaitValue
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.impl.annotations.SpyK
import io.mockk.verifyOrder
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class LauncherViewModelTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: LauncherViewModel

    @SpyK
    private lateinit var fakeAuthentication: FakeAuthentication

    private lateinit var fakeRemoteUserInfoRepository: FakeRemoteUserInfoRepository

    @SpyK
    private lateinit var fakeLocalUserInfoRepository: FakeLocalUserInfoRepository

    @Before
    fun setUp() {
        fakeAuthentication = FakeAuthentication()
        fakeRemoteUserInfoRepository = FakeRemoteUserInfoRepository()
        fakeLocalUserInfoRepository = FakeLocalUserInfoRepository()
        val fakeAppConfig = FakeAppConfig()
        val fakeAppInfoRepository = FakeAppInfoRepository()

        MockKAnnotations.init(this)

        viewModel = LauncherViewModel(
            fakeAuthentication, fakeRemoteUserInfoRepository,
            fakeLocalUserInfoRepository, fakeAppConfig, fakeAppInfoRepository
        )
    }

    @After
    fun teardown() {
        clearAllMocks()
    }

    @Test
    fun findAuthenticationStatusAndNavigate() {
        // Test when both user token and user credentials are not available

        viewModel.findAuthenticationStatusAndNavigate()
        assertThat(viewModel.navigateToLoginScreen.getOrAwaitValue())
            .isInstanceOf(AuthenticationRequirementStatus.AuthenticationNeededCredentialsNotAvailable::class.java)

        // Test when user credentials are available
        fakeAuthentication.loginCredentials = UserCredentialsModel("some_email", "some_password")
        viewModel.findAuthenticationStatusAndNavigate()
        assertThat(viewModel.navigateToLoginScreen.getOrAwaitValue())
            .isInstanceOf(AuthenticationRequirementStatus.AuthenticationNeededCredentialsAvailable::class.java)

        // Test when both user credentials and user jwt token are available
        fakeAuthentication.userToken = TEST_JWT_TOKEN
        viewModel.findAuthenticationStatusAndNavigate()
        assertThat(viewModel.navigateToHomeScreen.getOrAwaitValue()).isNull()
    }

    @Test
    fun callVerificationUrlAndNavigate_1() {
        // Test when user credentials and jwt token are available
        fakeAuthentication.userToken = TEST_JWT_TOKEN
        fakeAuthentication.loginCredentials = UserCredentialsModel("some_email", "some_password")
        viewModel.callVerificationUrlAndNavigate("some_url")

        assertThat(viewModel.error.getOrAwaitValue().getContentIfNotHandled()?.messageId).isEqualTo(R.string.user_already_logged_in)
        assertThat(viewModel.navigateToHomeScreen.getOrAwaitValue()).isNull()
    }

    @Test
    fun callVerificationUrlAndNavigate_2() {
        // Test when user credentials and jwt token are not available and getVerificationKey returns error
        fakeAuthentication.loginCredentials = null
        fakeAuthentication.userToken = null
        fakeAuthentication.shouldReturnErrorOnGetVerificationKey = true
        viewModel.callVerificationUrlAndNavigate("some_url")

        assertThat(viewModel.error.getOrAwaitValue().getContentIfNotHandled()?.message).isEqualTo(TEST_ERROR_MESSAGE)
    }

    @Test
    fun callVerificationUrlAndNavigate_3() {
        /* Test when user credentials and jwt token are not available and getVerificationKey returns success
         * cachedSignUpCredentials is null
         */
        viewModel.callVerificationUrlAndNavigate("some_url")
        assertThat(viewModel.navigateToLoginScreen.getOrAwaitValue())
            .isInstanceOf(AuthenticationRequirementStatus.AuthenticationNeededCredentialsNotAvailable::class.java)
    }

    @Test
    fun callVerificationUrlAndNavigate_4() {
        /* Test when user credentials and jwt token are not available and getVerificationKey returns success
         * cachedSignUpCredentials is not null
         * login returns error
         */
        fakeAuthentication.cacheSignUpCredentials(UserCredentialsModel("some_email", "some_password"))
        fakeAuthentication.shouldReturnErrorOnLogin = true
        viewModel.callVerificationUrlAndNavigate("some_url")
        assertThat(viewModel.error.getOrAwaitValue().getContentIfNotHandled()?.message).isEqualTo(TEST_ERROR_MESSAGE)
    }

    @Test
    fun callVerificationUrlAndNavigate_5() {
        /* Test when user credentials and jwt token are not available and getVerificationKey returns success
         * cachedSignUpCredentials is not null
         * login returns success
         * verifyEmail returns error
         */
        fakeAuthentication.cacheSignUpCredentials(UserCredentialsModel("some_email", "some_password"))
        fakeAuthentication.shouldReturnErrorOnVerifyEmail = true
        viewModel.callVerificationUrlAndNavigate("some_url")
        assertThat(viewModel.error.getOrAwaitValue().getContentIfNotHandled()?.message).isEqualTo(TEST_ERROR_MESSAGE)
    }

    @Test
    fun callVerificationUrlAndNavigate_6() {
        /* Test when user credentials and jwt token are not available and getVerificationKey returns success
         * cachedSignUpCredentials is not null
         * login returns success
         * verifyEmail returns success
         * getUserInfo returns failure
         */
        fakeAuthentication.cacheSignUpCredentials(UserCredentialsModel("some_email", "some_password"))
        fakeRemoteUserInfoRepository.shouldReturnError = true
        viewModel.callVerificationUrlAndNavigate("some_url")
        assertThat(viewModel.error.getOrAwaitValue().getContentIfNotHandled()?.message).isEqualTo(TEST_ERROR_MESSAGE)
    }

    @Test
    fun callVerificationUrlAndNavigate_7() {
        /* Test when user credentials and jwt token are not available and getVerificationKey returns success
         * cachedSignUpCredentials is not null
         * login returns success
         * verifyEmail returns success
         * getUserInfo returns success
         */
        fakeAuthentication.cacheSignUpCredentials(UserCredentialsModel("some_email", "some_password"))

        viewModel.callVerificationUrlAndNavigate("some_url")

        verifyOrder {
            fakeAuthentication.saveToken(any())
            fakeLocalUserInfoRepository.saveUserInfo(any())
            fakeAuthentication.clearSignUpCredentialsCache()
        }
        assertThat(viewModel.navigateToSocialProfileConnect.getOrAwaitValue()).isEqualTo(Unit)
    }

    @Test
    fun handlePushNotification() {
        val brandGroupId = 1
        val notificationContent = NotificationContent(brandGroupId)

        // Test when isAppRunning is true
        viewModel.handlePushNotification(notificationContent, true)
        assertThat(viewModel.navigateToHomeScreen.getOrAwaitValue()).isEqualTo(notificationContent)

        // Test when isAppRunning is false and both user token and user credentials are not available

        viewModel.handlePushNotification(notificationContent, false)
        assertThat(viewModel.navigateToLoginScreen.getOrAwaitValue())
            .isInstanceOf(AuthenticationRequirementStatus.AuthenticationNeededCredentialsNotAvailable::class.java)

        // Test when isAppRunning is false and user credentials are available
        fakeAuthentication.loginCredentials = UserCredentialsModel("some_email", "some_password")
        viewModel.handlePushNotification(notificationContent, false)
        assertThat(viewModel.navigateToLoginScreen.getOrAwaitValue())
            .isInstanceOf(AuthenticationRequirementStatus.AuthenticationNeededCredentialsAvailable::class.java)

        // Test when isAppRunning is false and both user credentials and user jwt token are available
        fakeAuthentication.userToken = TEST_JWT_TOKEN
        viewModel.handlePushNotification(notificationContent, false)
        assertThat(viewModel.navigateToHomeScreen.getOrAwaitValue()).isEqualTo(notificationContent)
    }
}