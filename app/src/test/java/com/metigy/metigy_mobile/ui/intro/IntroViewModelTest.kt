package com.metigy.metigy_mobile.ui.intro

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.metigy.metigy_mobile.data.preferences.AppPreferences
import com.metigy.metigy_mobile.testutil.getOrAwaitValue
import io.mockk.every
import io.mockk.mockk
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class IntroViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: IntroViewModel

    @Before
    fun setUp() {
        val appPreferences = mockk<AppPreferences>()
        every { appPreferences.getIsPreviouslyLoggedIn() } returns false
        viewModel = IntroViewModel(appPreferences)
    }

    @Test
    fun num_intro_pages_is_4() {
        assertThat(viewModel.contentList).hasSize(4)
    }

    @Test
    fun setCurrentTabPosition() {
        viewModel.setCurrentTabPosition(1)
        val currentPosition = viewModel.currentTabPosition.getOrAwaitValue()
        assertThat(currentPosition).isEqualTo(1)
    }
}