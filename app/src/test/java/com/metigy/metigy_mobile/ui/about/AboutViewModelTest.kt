package com.metigy.metigy_mobile.ui.about

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.utils.StringResProvider
import com.metigy.metigy_mobile.data.appInfoRepository.AppInfoRepository
import com.metigy.metigy_mobile.data.appmodels.AppVersionModel
import com.metigy.metigy_mobile.data.appmodels.Version
import com.metigy.metigy_mobile.testutil.getOrAwaitValue
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class AboutViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val appInfoRepository = mockk<AppInfoRepository>(relaxed = true)

    private val stringResProvider = mockk<StringResProvider>(relaxed = true)

    private lateinit var viewModel: AboutViewModel

    private val version = Version("1.0.0")

    private val appVersionModel = AppVersionModel(10, version)

    @Before
    fun setUp() {
        every { appInfoRepository.appVersion() } returns appVersionModel
        every { stringResProvider.getString(R.string.version_colon) } returns "Version: "
        viewModel = AboutViewModel(appInfoRepository, stringResProvider)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    @Test
    fun set_app_version_from_repository_on_init() {
        Truth.assertThat(viewModel.appVersion.getOrAwaitValue()).isEqualTo(appVersionModel)
    }

    @Test
    fun prettifyVersion() {
        val prettifiedVersion = viewModel.prettifyVersion(appVersionModel)
        Truth.assertThat(prettifiedVersion).isEqualTo("Version: 1.0.0(10)")
    }
}