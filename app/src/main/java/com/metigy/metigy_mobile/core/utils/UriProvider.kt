package com.metigy.metigy_mobile.core.utils

import android.net.Uri

class UriWrapper(private val uri: Uri) {
    fun getQueryParameter(key: String): String? = uri.getQueryParameter(key)
}