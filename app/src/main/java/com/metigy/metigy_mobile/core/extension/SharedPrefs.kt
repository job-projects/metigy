package com.metigy.metigy_mobile.core.extension

import android.content.SharedPreferences

fun SharedPreferences.editAndApply(block: SharedPreferences.Editor.() -> Unit) {
    edit().apply {
        block()
    }.apply()
}