package com.metigy.metigy_mobile.core.consts

//Prefixing the log tags with the word MetigyTag to be able filter all the logs coming from only the app

const val TAG_DEBUG = "MTagDebug"
const val TAG_RETROFIT_ERROR = "MTagRetrofitError"
const val TAG_NAVIGATE = "MTagNavigation"
const val TAG_JSON_ERROR = "MTagJsonError"
const val TAG_OS_INTERACTION = "MTagOsInteraction"
const val TAG_AUTHENTICATION = "MTagAuthentication"
const val TAG_GENERAL = "MTagGeneral"
const val TAG_PUSH_NOTIFICATION = "MTagPushNotif"
const val TAG_IMAGE = "MTagImage"
const val TAG_CHANGE_STATUS = "MTagChangeStatus"
const val TAG_PERMISSION = "MTagPermission"
