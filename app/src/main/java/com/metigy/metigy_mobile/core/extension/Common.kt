package com.metigy.metigy_mobile.core.extension

import androidx.core.net.toUri
import com.metigy.metigy_mobile.core.consts.TAG_GENERAL
import com.metigy.metigy_mobile.core.utils.UriWrapper
import com.metigy.metigy_mobile.data.appmodels.Version
import timber.log.Timber
import java.util.regex.Pattern

fun Int?.orZero(): Int = this ?: 0

fun Boolean?.orFalse(): Boolean = this ?: false


fun <T> List<T>.removeFirst() = this.filterIndexed { index, _ ->
    index != 0
}

private fun <T> ArrayList<T>.moveToFirst(element: T) {
    if (remove(element)) {
        add(0, element)
    }
}

fun <T> List<T>.moveToFirst(element: T) =
    ArrayList(this).apply {
        moveToFirst(element)
    }.toList()


fun String?.toVersion(): Version? =
    try {
        Version(this)
    } catch (e: Exception) {
        Timber.tag(TAG_GENERAL).e(e, this.orEmpty())
        null
    }

//TODO Maybe we should avoid mutating things?
fun <T> MutableSet<T>.toggleElement(element: T) {
    if (contains(element)) {
        remove(element)
    } else {
        add(element)
    }
}

fun String.toUriWrapper() = UriWrapper(toUri())

fun String.isValidEmail(): Boolean {
    val emailPattern = Pattern.compile(
        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                ")+"
    )
    return emailPattern.matcher(this).matches()
}

fun String.isValidPassword(): Boolean {
    val passwordPattern = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}$")
    return passwordPattern.matcher(this).matches()
}