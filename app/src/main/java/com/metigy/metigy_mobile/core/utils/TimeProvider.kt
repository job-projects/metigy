package com.metigy.metigy_mobile.core.utils

import java.time.Instant
import javax.inject.Inject

interface TimeProvider {
    fun nowAsEpochSecond(): Long
    fun now(): Instant
}

class DefaultTimeProvider @Inject constructor() : TimeProvider {
    override fun nowAsEpochSecond(): Long = Instant.now().epochSecond

    override fun now(): Instant = Instant.now()
}