package com.metigy.metigy_mobile.core.ui.customviews

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.extension.formatToContentDateSelection
import com.metigy.metigy_mobile.core.extension.isTodayAtDeviceZone
import com.metigy.metigy_mobile.core.extension.setShapeDrawableBackgroundColor
import com.metigy.metigy_mobile.core.extension.setTextColorResId
import com.metigy.metigy_mobile.core.utils.TimeProvider
import com.metigy.metigy_mobile.core.utils.TimezoneProvider
import dagger.hilt.android.AndroidEntryPoint
import java.time.Instant
import javax.inject.Inject

@AndroidEntryPoint
class DateSelectionTextViewWidget(context: Context, attrs: AttributeSet) : AppCompatTextView(context, attrs) {

    @Inject
    lateinit var timeProvider: TimeProvider

    @Inject
    lateinit var timezoneProvider: TimezoneProvider

    //Is selecting or already selected?
    var selectingState: Boolean = false
        set(value) {
            field = value
            refreshView()
        }

    var dateText: String = ""
        set(value) {
            field = value
            refreshView()
        }

    fun setDate(date: Instant) {
        val prefix =
            if (date.isTodayAtDeviceZone(timeProvider, timezoneProvider)) {
                context.getString(R.string.today_with_comma)
            } else {
                ""
            }
        dateText = prefix.plus(date.formatToContentDateSelection(timezoneProvider))
    }

    private fun refreshView() {
        if (selectingState) {
            text = dateText.plus("  ▲")
            setTextColorResId(R.color.white)
            setShapeDrawableBackgroundColor(R.color.blue)
        } else {
            text = dateText.plus("  ▼")
            setTextColorResId(R.color.black)
            setShapeDrawableBackgroundColor(R.color.white)
        }
    }
}