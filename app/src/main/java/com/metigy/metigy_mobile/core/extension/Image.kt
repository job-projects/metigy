package com.metigy.metigy_mobile.core.extension

import android.content.ContentResolver
import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.metigy.metigy_mobile.core.consts.TAG_IMAGE
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.util.*


fun ImageView.loadImage(url: String?) {
    Glide.with(context)
        .load(url)
        .into(this)
}

suspend fun String.asImageUrlToBitmap(context: Context): Bitmap? {
    return withContext(Dispatchers.IO) {
        Glide.with(context)
            .asBitmap()
            .load(this@asImageUrlToBitmap)
            .submit()
            .get()
    }
}

fun Bitmap.saveToFile(imageFile: File): Boolean =
    try {
        val fOut: OutputStream = FileOutputStream(imageFile)
        this.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
        fOut.close()
        true
    } catch (e: Exception) {
        Timber.tag(TAG_IMAGE).e(e)
        false
    }

//TODO Find a neater way to handle writing file to storage.
fun Bitmap.saveToFile(context: Context, relativeFile: File, fileName: String): Boolean =
    try {
        val fos: OutputStream? =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                val resolver: ContentResolver = context.contentResolver
                val contentValues = ContentValues()
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, fileName)
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, relativeFile.path)
                val imageUri: Uri? = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
                imageUri?.let {
                    resolver.openOutputStream(it)
                }
            } else {
                val imagesDir = Environment.getExternalStoragePublicDirectory(relativeFile.path).toString()
                val image = File(imagesDir, fileName)
                FileOutputStream(image)
            }
        compress(Bitmap.CompressFormat.JPEG, 100, fos)
        fos?.close()
        true
    } catch (e: Exception) {
        Timber.tag(TAG_IMAGE).e(e)
        false
    }
