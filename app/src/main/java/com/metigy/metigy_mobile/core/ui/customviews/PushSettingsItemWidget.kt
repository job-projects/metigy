package com.metigy.metigy_mobile.core.ui.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.extension.applyAttributes
import com.metigy.metigy_mobile.databinding.WidgetPushSettingsItemBinding

class PushSettingsItemWidget(context: Context, attrs: AttributeSet) : FrameLayout(context, attrs) {
    init {
        val binding = WidgetPushSettingsItemBinding.inflate(LayoutInflater.from(context), this, true)
        applyAttributes(attrs, R.styleable.PushSettingsItem) {
            binding.titleTextView.text = it.getString(R.styleable.PushSettingsItem_itemTitleText)
            binding.bodyTextView.text = it.getString(R.styleable.PushSettingsItem_itemDescText)
        }

    }
}