package com.metigy.metigy_mobile.core.ui

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.metigy.metigy_mobile.core.extension.orZero

class HorizontalSpaceItemDecoration(private val space: Float) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        if (parent.getChildAdapterPosition(view) != parent.adapter?.itemCount.orZero() - 1) {
            outRect.right = space.toInt()
        }
    }
}