package com.metigy.metigy_mobile.core.di

import com.metigy.metigy_mobile.data.preferences.AppPreferences
import com.metigy.metigy_mobile.data.preferences.SharedPreferencesHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppPreferenceModule {
    @Provides
    @Singleton
    fun provideAppPreference(sharedPreferencesHelper: SharedPreferencesHelper): AppPreferences = sharedPreferencesHelper
}