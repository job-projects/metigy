package com.metigy.metigy_mobile.core.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.provider.Settings
import androidx.core.content.pm.PackageInfoCompat
import com.metigy.metigy_mobile.core.consts.TAG_OS_INTERACTION
import com.metigy.metigy_mobile.core.extension.toVersion
import com.metigy.metigy_mobile.data.appmodels.AppVersionModel
import com.metigy.metigy_mobile.ui.contentlist.SmAppAndInfo
import dagger.hilt.android.qualifiers.ApplicationContext
import timber.log.Timber
import javax.inject.Inject


class HostInfoUtils @Inject constructor(@ApplicationContext private val context: Context) {
    fun appVersion(): AppVersionModel {
        val packageInfo: PackageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
        val appVersion = packageInfo.versionName.toVersion() ?: error("Android not able to return a valid version")
        return AppVersionModel(
            versionCode = PackageInfoCompat.getLongVersionCode(packageInfo),
            versionName = appVersion
        )
    }


    @SuppressLint("HardwareIds")
    fun getUniqueId() = Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID).orEmpty()

    fun isAppInstalled(app: SmAppAndInfo): Boolean =
        try {
            context.packageManager.getPackageInfo(app.packageName, 0)
            Timber.tag(TAG_OS_INTERACTION).d("${app.name} found installed on the device")
            true
        } catch (e: PackageManager.NameNotFoundException) {
            Timber.tag(TAG_OS_INTERACTION).d("${app.name} is not installed")
            false
        }
}