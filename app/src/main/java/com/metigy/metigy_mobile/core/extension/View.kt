package com.metigy.metigy_mobile.core.extension

import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.res.TypedArray
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.ShapeDrawable
import android.net.Uri
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.metigy.metigy_mobile.BuildConfig
import com.metigy.metigy_mobile.core.consts.TAG_GENERAL
import com.metigy.metigy_mobile.core.consts.TAG_NAVIGATE
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.ui.launcher.LauncherActivity
import com.metigy.metigy_mobile.ui.main.MainActivity
import timber.log.Timber
import java.io.File

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.setBackgroundColorFromRes(@ColorRes colorId: Int) {
    try {
        setBackgroundColor(ContextCompat.getColor(context, colorId))
    } catch (e: Exception) {
        Timber.tag(TAG_GENERAL).e(e)
    }
}

fun View.setShapeDrawableBackgroundColor(@ColorRes colorResId: Int) {
    try {
        when (val shapeBackground = background) {
            is ShapeDrawable -> {
                shapeBackground.paint.color = ContextCompat.getColor(context, colorResId)
            }
            is GradientDrawable -> {
                shapeBackground.setColor(ContextCompat.getColor(context, colorResId))
            }
            is ColorDrawable -> {
                shapeBackground.color = ContextCompat.getColor(context, colorResId)
            }
        }
    } catch (e: Exception) {
        Timber.tag(TAG_GENERAL).e(e)
    }
}

fun View.setBackgroundTintFromRes(@ColorRes colorId: Int) {
    try {
        backgroundTintList = ContextCompat.getColorStateList(context, colorId)
    } catch (e: Exception) {
        Timber.tag(TAG_GENERAL).e(e)
    }
}

fun View.setBackgroundDrawable(@DrawableRes drawableId: Int) {
    try {
        background = ContextCompat.getDrawable(context, drawableId)
    } catch (e: Exception) {
        Timber.tag(TAG_GENERAL).e(e)
    }
}

fun TextInputEditText.textValue() = text.toString()

fun Fragment.showErrorToast(error: Result.Error) {
    showToast(error, context)
}

fun Fragment.showToast(messageResId: Int, duration: Int = Toast.LENGTH_LONG) {
    Toast.makeText(context, messageResId, duration).show()
}

fun AppCompatActivity.showErrorToast(error: Result.Error) {
    showToast(error, this)
}

//To be called after logout
fun Activity.relaunchApp() {
    finish()
    startActivity(Intent(this, LauncherActivity::class.java))
}

private fun showToast(error: Result.Error, context: Context?) {
    when {
        error.messageId != null -> {
            Toast.makeText(context, error.messageId, Toast.LENGTH_SHORT).show()
        }
        error.message != null -> {
            Toast.makeText(context, error.message, Toast.LENGTH_SHORT).show()
        }
    }
}

fun RecyclerView.verticalLayoutManager() {
    layoutManager = LinearLayoutManager(context)
}

fun RecyclerView.horizontalLayoutManager() {
    layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
}

fun RecyclerView.gridLayoutManager(spanCount: Int) {
    layoutManager = GridLayoutManager(context, spanCount)
}

fun View.showOrGone(show: Boolean) {
    if (show) {
        visible()
    } else {
        gone()
    }
}

fun View.showOrInvisible(show: Boolean) {
    if (show) {
        visible()
    } else {
        invisible()
    }
}

fun TextView.setStartDrawableStart(drawableId: Int, sizeInDP: Int, drawablePaddingInDP: Int? = null) {
    val drawable = ResourcesCompat.getDrawable(resources, drawableId, null)
    val width = sizeInDP.toPixel(context).toInt()
    val height = sizeInDP.toPixel(context).toInt()
    drawable?.setBounds(0, 0, width, height)

    drawablePaddingInDP?.let { padding ->
        compoundDrawablePadding = padding.toPixel(context).toInt()
    }
    setCompoundDrawables(drawable, null, null, null)
}

fun TextView.setFont(fontResId: Int) {
    try {
        typeface = ResourcesCompat.getFont(context, fontResId)
    } catch (e: Exception) {
        Timber.tag(TAG_GENERAL).e(e, "Font Id not found")
    }
}

fun TextView.setTextColorResId(@ColorRes colorResId: Int) {
    try {
        setTextColor(ContextCompat.getColor(context, colorResId))
    } catch (e: Exception) {
        Timber.tag(TAG_GENERAL).e(e, "Color Id not found")
    }
}

fun Fragment.navigate(navDirections: NavDirections) {
    try {
        findNavController().navigate(navDirections)
    } catch (e: Exception) {
        Timber.tag(TAG_NAVIGATE).e(e)
    }
}

fun Fragment.navigateUp() {
    findNavController().navigateUp()
}

fun Fragment.launchHomeScreen() {
    val intent = Intent(context, MainActivity::class.java).apply {
        addFlagsToClearBackstack()
    }
    startActivity(intent)
    activity?.finish()
}

fun Intent.addFlagsToClearBackstack() {
    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
}

fun Fragment.showSimpleDialog(
    @StringRes titleId: Int? = null,
    title: String = "",
    @StringRes messageId: Int? = null,
    message: String = "",
    @StringRes positiveButtonTextId: Int? = null,
    positiveButtonText: String = "",
    @StringRes negativeButtonTextId: Int? = null,
    onConfirm: () -> Unit,
    onCancel: () -> Unit = {},
    dismissible: Boolean = true
) {
    context?.showSimpleDialog(
        titleId = titleId,
        title = title,
        messageId = messageId,
        message = message,
        positiveButtonTextId = positiveButtonTextId,
        positiveButtonText = positiveButtonText,
        negativeButtonTextId = negativeButtonTextId,
        onConfirm = onConfirm,
        dismissible = dismissible
    )
}

fun Context.showSimpleDialog(
    @StringRes titleId: Int? = null,
    title: String = "",
    @StringRes messageId: Int? = null,
    message: String = "",
    @StringRes positiveButtonTextId: Int? = null,
    positiveButtonText: String = "",
    @StringRes negativeButtonTextId: Int? = null,
    onConfirm: () -> Unit,
    onCancel: () -> Unit = {},
    dismissible: Boolean = true
) {
    MaterialAlertDialogBuilder(this)
        .apply {
            if (titleId != null) setTitle(titleId) else setTitle(title)
            if (messageId != null) setMessage(messageId) else setMessage(message)
            if (positiveButtonTextId != null) setPositiveButton(positiveButtonTextId) { _, _ -> onConfirm() } else setPositiveButton(positiveButtonText) { _, _ -> onConfirm() }
            if (negativeButtonTextId != null) setNegativeButton(negativeButtonTextId) { _, _ -> onCancel() }
            if (dismissible.not()) setCancelable(false)
        }.show()
}

fun Fragment.intentForImageSharing(imageFile: File): Intent {
    val uri = FileProvider.getUriForFile(requireContext(), BuildConfig.APPLICATION_ID + ".provider", imageFile)
    val intent = Intent(Intent.ACTION_SEND)
    intent.putExtra(Intent.EXTRA_STREAM, uri)
    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
    intent.type = "image/png"
    return intent
}

fun Fragment.intentForTextSharing(text: String): Intent {
    val intent = Intent(Intent.ACTION_SEND)
    intent.type = "text/plain"
    intent.putExtra(Intent.EXTRA_TEXT, text)
    return intent
}

fun Fragment.intentForLaunchingBrowser(url: String): Intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))

fun Fragment.composeEmail(addresses: List<String>) {
    activity?.composeEmail(addresses)
}

fun Fragment.openUrl(url: String) {
    activity?.openUrl(url)
}

fun Activity.copyToClipboard(text: String) {
    val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
    val clip = ClipData.newPlainText("", text)
    clipboard?.setPrimaryClip(clip)
}

fun Activity.openUrl(url: String) {
    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
}

fun Activity.composeEmail(addresses: List<String>) {
    val intent = Intent(Intent.ACTION_SEND).apply {
        type = "*/*"
        putExtra(Intent.EXTRA_EMAIL, addresses.toTypedArray())
    }
    try {
        startActivity(intent)
    } catch (e: Exception) {
        Timber.tag(TAG_GENERAL).d("No email app found on the device")
    }
}

fun Int.toPixel(context: Context): Float =
    TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        this.toFloat(),
        context.resources.displayMetrics
    )


fun View.applyAttributes(set: AttributeSet, attrs: IntArray, apply: (TypedArray) -> Unit) {
    context.theme.obtainStyledAttributes(
        set, attrs, 0, 0
    ).apply {
        try {
            apply(this)
        } finally {
            recycle()
        }
    }
}

fun View.getDimension(dimensionResId: Int) = context.resources.getDimension(dimensionResId)


fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
    hideKeyboard(currentFocus ?: View(this))
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}