package com.metigy.metigy_mobile.core.ui.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.extension.applyAttributes
import com.metigy.metigy_mobile.core.extension.showOrGone
import com.metigy.metigy_mobile.databinding.WidgetSettingsSectionBodyItemBinding

class SettingsSectionBodyItemWidget(context: Context, attrs: AttributeSet) : FrameLayout(context, attrs) {

    init {
        val binding = WidgetSettingsSectionBodyItemBinding.inflate(LayoutInflater.from(context), this, true)
        applyAttributes(attrs, R.styleable.SettingsSectionBodyItem) {
            binding.itemNameTextView.text = it.getString(R.styleable.SettingsSectionBodyItem_itemNameText)
            binding.itemValueTextView.text = it.getString(R.styleable.SettingsSectionBodyItem_itemValueText)
            binding.dividerView.showOrGone(it.getBoolean(R.styleable.SettingsSectionBodyItem_isLastItem, false).not())
        }
    }
}