package com.metigy.metigy_mobile.core.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.databinding.DialogFragmentStackButtonBinding
import com.metigy.metigy_mobile.ui.socialplatformselection.SocialPlatformSelectionFragment

class StackedButtonDialogFragment : DialogFragment() {

    private lateinit var binding: DialogFragmentStackButtonBinding

    private val listener: OnButtonClickListener
        get() = parentFragment as OnButtonClickListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DialogFragmentStackButtonBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.titleTextView.text = arguments?.getString(BUNDLE_KEY_TITLE)
        binding.messageTextView.text = arguments?.getString(BUNDLE_KEY_MESSAGE)
        binding.firstButton.text = arguments?.getString(BUNDLE_KEY_BUTTON_FIRST)
        binding.secondButton.text = arguments?.getString(BUNDLE_KEY_BUTTON_SECOND)
        binding.thirdButton.text = arguments?.getString(BUNDLE_KEY_BUTTON_THIRD)

        binding.firstButton.setOnClickListener {
            listener.onFirstButtonClicked(tag)
            dismiss()
        }

        binding.secondButton.setOnClickListener {
            listener.onSecondButtonClicked(tag)
            dismiss()
        }

        binding.thirdButton.setOnClickListener {
            dismiss()
        }
    }

    interface OnButtonClickListener {
        fun onFirstButtonClicked(tag: String?)
        fun onSecondButtonClicked(tag: String?)
    }

    companion object {
        private const val BUNDLE_KEY_TITLE = "Title"
        private const val BUNDLE_KEY_MESSAGE = "Message"
        private const val BUNDLE_KEY_BUTTON_FIRST = "FirstButton"
        private const val BUNDLE_KEY_BUTTON_SECOND = "SecondButton"
        private const val BUNDLE_KEY_BUTTON_THIRD = "ThirdButton"

        fun createAndShow(
            fragmentManager: FragmentManager,
            title: String,
            message: String,
            firstButton: String,
            secondButton: String,
            thirdButton: String,
            tag: String?
        ) {
            val dialogFragment = StackedButtonDialogFragment()
            dialogFragment.arguments = Bundle().apply {
                putString(BUNDLE_KEY_TITLE, title)
                putString(BUNDLE_KEY_MESSAGE, message)
                putString(BUNDLE_KEY_BUTTON_FIRST, firstButton)
                putString(BUNDLE_KEY_BUTTON_SECOND, secondButton)
                putString(BUNDLE_KEY_BUTTON_THIRD, thirdButton)
            }
            dialogFragment.show(fragmentManager, tag)
        }

        fun launchHelpDialog(fragment: Fragment) {
            createAndShow(
                fragmentManager = fragment.childFragmentManager,
                title = fragment.getString(R.string.help_dialog_title),
                message = fragment.getString(R.string.help_dialog_message),
                firstButton = fragment.getString(R.string.support_centre),
                secondButton = fragment.getString(R.string.contact_us_via_email),
                thirdButton = fragment.getString(R.string.cancel),
                tag = SocialPlatformSelectionFragment.DIALOG_TAG_HELP
            )
        }
    }
}