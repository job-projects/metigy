package com.metigy.metigy_mobile.core.di

import com.metigy.metigy_mobile.BuildConfig
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RetrofitModule {

    @Provides
    @Singleton
    @Named(ApplicationModule.NAMED_RETROFIT_APP)
    fun provideAppUrlRetrofit(client: OkHttpClient, gsonConverterFactory: GsonConverterFactory): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL_APP)
            .addConverterFactory(gsonConverterFactory)
            .client(client)
            .build()
    }

    @Provides
    @Singleton
    @Named(ApplicationModule.NAMED_RETROFIT_SERVICE)
    fun provideServiceUrlRetrofit(client: OkHttpClient, gsonConverterFactory: GsonConverterFactory): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL_SERVICE)
            .addConverterFactory(gsonConverterFactory)
            .client(client.newBuilder().readTimeout(20, TimeUnit.SECONDS).build())
            .build()
    }


}