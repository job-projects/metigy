package com.metigy.metigy_mobile.core.result

sealed class Result<out R> {
    data class Success<T>(val data: T) : Result<T>()
    data class Error(val exception: Exception? = null, val message: String? = null, val messageId: Int? = null) : Result<Nothing>()
}