package com.metigy.metigy_mobile.core.di

import com.google.gson.GsonBuilder
import com.metigy.metigy_mobile.BuildConfig
import com.metigy.metigy_mobile.core.utils.DefaultTimeProvider
import com.metigy.metigy_mobile.core.utils.DefaultTimezoneProvider
import com.metigy.metigy_mobile.core.utils.TimeProvider
import com.metigy.metigy_mobile.core.utils.TimezoneProvider
import com.metigy.metigy_mobile.data.airship.AirshipPushNotification
import com.metigy.metigy_mobile.data.airship.AppPushNotification
import com.metigy.metigy_mobile.data.apiservice.ApiServiceForAppUrl
import com.metigy.metigy_mobile.data.apiservice.ApiServiceForServiceUrl
import com.metigy.metigy_mobile.data.appInfoRepository.AppInfoRepository
import com.metigy.metigy_mobile.data.appInfoRepository.DefaultAppInfoRepository
import com.metigy.metigy_mobile.data.appcache.AppCache
import com.metigy.metigy_mobile.data.appcache.DefaultAppCache
import com.metigy.metigy_mobile.data.appconfigrepository.AppConfig
import com.metigy.metigy_mobile.data.appconfigrepository.DefaultAppConfig
import com.metigy.metigy_mobile.data.authentication.*
import com.metigy.metigy_mobile.data.brandrepository.BrandRepository
import com.metigy.metigy_mobile.data.brandrepository.DefaultBrandRepository
import com.metigy.metigy_mobile.data.platformpagesrepository.DefaultPlatformPagesRepository
import com.metigy.metigy_mobile.data.platformpagesrepository.PlatformPagesRepository
import com.metigy.metigy_mobile.data.postrepository.ContentPersistence
import com.metigy.metigy_mobile.data.postrepository.ContentRepository
import com.metigy.metigy_mobile.data.postrepository.DefaultContentPersistence
import com.metigy.metigy_mobile.data.postrepository.DefaultContentRepository
import com.metigy.metigy_mobile.data.socialaccountconnect.DefaultSocialAccountConnect
import com.metigy.metigy_mobile.data.socialaccountconnect.SocialAccountConnect
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class ApplicationModule {

    @Provides
    @Singleton
    fun provideAuthentication(authentication: DefaultAuthentication): Authentication = authentication

    @Provides
    @Singleton
    fun provideApiServiceForAppUrl(@Named(NAMED_RETROFIT_APP) retrofit: Retrofit): ApiServiceForAppUrl = retrofit.create(ApiServiceForAppUrl::class.java)

    @Provides
    @Singleton
    fun provideApiServiceForServiceUrl(@Named(NAMED_RETROFIT_SERVICE) retrofit: Retrofit): ApiServiceForServiceUrl = retrofit.create(ApiServiceForServiceUrl::class.java)

    @Provides
    @Singleton
    fun providesGsonConverterFactory(): GsonConverterFactory = GsonConverterFactory.create(GsonBuilder().setLenient().create())

    @Provides
    fun providesOkHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        return OkHttpClient.Builder().addInterceptor(interceptor).build()
    }

    @Provides
    @Singleton
    fun provideAppInfoRepository(appInfoRepository: DefaultAppInfoRepository): AppInfoRepository = appInfoRepository

    @Provides
    @Singleton
    fun provideBrandRepository(brandRepository: DefaultBrandRepository): BrandRepository = brandRepository

    @Provides
    @Singleton
    fun providePostRepository(postRepository: DefaultContentRepository): ContentRepository = postRepository

    @Provides
    @Singleton
    fun providePlatformPagesRepository(platformPagesRepository: DefaultPlatformPagesRepository): PlatformPagesRepository = platformPagesRepository

    @Provides
    @Singleton
    fun provideRemoteUserInfoRepository(remoteUserInfoRepository: DefaultRemoteUserInfoRepository): RemoteUserInfoRepository = remoteUserInfoRepository

    @Provides
    @Singleton
    fun provideLocalUserInfoRepository(localUserInfoRepository: DefaultLocalUserInfoRepository): LocalUserInfoRepository = localUserInfoRepository

    @Provides
    @Singleton
    fun provideTimeProvider(timeProvider: DefaultTimeProvider): TimeProvider = timeProvider

    @Provides
    @Singleton
    fun provideTimezoneProvider(timezoneProvider: DefaultTimezoneProvider): TimezoneProvider = timezoneProvider

    @Provides
    @Singleton
    fun provideAppCache(appCache: DefaultAppCache): AppCache = appCache

    @Provides
    @Singleton
    fun provideAppPushNotification(appPushNotification: AirshipPushNotification): AppPushNotification = appPushNotification

    @Provides
    @Singleton
    fun provideContentPersistence(contentPersistence: DefaultContentPersistence): ContentPersistence = contentPersistence

    @Provides
    @Singleton
    fun provideAppConfig(appConfig: DefaultAppConfig): AppConfig = appConfig

    @Provides
    @Singleton
    fun provideSocialAccountConnect(socialAccountConnect: DefaultSocialAccountConnect): SocialAccountConnect = socialAccountConnect

    companion object {
        const val NAMED_RETROFIT_APP = "App"
        const val NAMED_RETROFIT_SERVICE = "Service"

    }
}