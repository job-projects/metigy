package com.metigy.metigy_mobile.core.utils

import android.content.Context
import androidx.annotation.StringRes
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class StringResProvider @Inject constructor(@ApplicationContext private val context: Context)  {
    fun getString(@StringRes resId: Int, vararg formatArgs: Any) = context.getString(resId, *formatArgs)
}