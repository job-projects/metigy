package com.metigy.metigy_mobile.core.extension

import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.consts.TAG_RETROFIT_ERROR
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.data.apimodels.ErrorResponse
import com.metigy.metigy_mobile.data.apiservice.API_AUTHORIZATION_PREFIX
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.HttpException
import timber.log.Timber
import java.io.IOException

fun Map<String, String>.toRequestBody(): RequestBody {
    val multipartBuilder = MultipartBody.Builder().setType(MultipartBody.FORM)
    forEach { (t, u) ->
        multipartBuilder.addFormDataPart(t, u)
    }
    return multipartBuilder.build()
}

fun Exception.toAppError(): Result.Error =
    when (this) {
        is IOException -> { //SocketTimeoutException and UnknownHostException are in this group
            Timber.tag(TAG_RETROFIT_ERROR).d(this)
            Result.Error(exception = this, messageId = R.string.connection_error)
        }
        is HttpException -> {
            val jsonError = this.response()?.errorBody()?.string().orEmpty()
            val errorResponse = jsonError.asJson(ErrorResponse::class.java)
            errorResponse?.let {
                Timber.tag(TAG_RETROFIT_ERROR).d("Error Code: ${code()}. ${it.error}")
                Result.Error(exception = this, message = it.error.orEmpty())
            } ?: run {
                Timber.tag(TAG_RETROFIT_ERROR).d("Failed to parse the error:\t$jsonError")
                Result.Error(exception = this, messageId = R.string.unknown_error)
            }
        }
        else -> {
            Timber.tag(TAG_RETROFIT_ERROR).d(this)
            Result.Error(exception = this, messageId = R.string.unknown_error)
        }
    }

fun String.jwtPrefix(): String = API_AUTHORIZATION_PREFIX + this