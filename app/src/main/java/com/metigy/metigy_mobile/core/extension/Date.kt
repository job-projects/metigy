package com.metigy.metigy_mobile.core.extension

import com.metigy.metigy_mobile.core.consts.TAG_DEBUG
import com.metigy.metigy_mobile.core.utils.TimeProvider
import com.metigy.metigy_mobile.core.utils.TimezoneProvider
import timber.log.Timber
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.*

const val DATE_TIME_PATTERN_SCHEDULED_PREVIEW = "h:mm a"
const val DATE_TIME_PATTERN_APP_CONFIG = "yyyy-MM-dd HH:mm:ss"
const val DATE_TIME_PATTERN_CONTENT_DATE_SELECTION = "dd MMM"

fun Long.inSecondsToInstant(): Instant = Instant.ofEpochSecond(this)

fun Instant.formatToScheduledPreview(timezoneProvider: TimezoneProvider): String =
    formatToString(pattern = DATE_TIME_PATTERN_SCHEDULED_PREVIEW, timezoneProvider = timezoneProvider)

fun Instant.formatToContentDateSelection(timezoneProvider: TimezoneProvider): String =
    formatToString(pattern = DATE_TIME_PATTERN_CONTENT_DATE_SELECTION, timezoneProvider = timezoneProvider)

fun ZonedDateTime.formatToString(pattern: String): String = format(pattern.dateTimeFormatter() ?: error("Invalid pattern"))

fun Instant.formatToString(pattern: String, timezoneProvider: TimezoneProvider): String =
    ZonedDateTime.ofInstant(this, timezoneProvider.systemDefaultTimeZoneId()).formatToString(pattern)

fun String.toZonedDateTime(pattern: String, timezoneProvider: TimezoneProvider): ZonedDateTime? =
    pattern.dateTimeFormatter()?.let { formatter ->
        try {
            LocalDateTime.parse(this, formatter).atZone(timezoneProvider.systemDefaultTimeZoneId())
        } catch (e: Exception) {
            Timber.tag(TAG_DEBUG).e(e)
            null
        }
    }

fun String.toInstant(pattern: String, timezoneProvider: TimezoneProvider): Instant? = toZonedDateTime(pattern, timezoneProvider)?.toInstant()

fun String.dateTimeFormatter(): DateTimeFormatter? =
    try {
        DateTimeFormatter.ofPattern(this, Locale.ENGLISH)
    } catch (e: Exception) {
        Timber.tag(TAG_DEBUG).e(e)
        null
    }

fun Instant.isTodayAtDeviceZone(timeProvider: TimeProvider, timezoneProvider: TimezoneProvider) = toLocalDate(timezoneProvider) == timeProvider.now().toLocalDate(timezoneProvider)

fun Instant.isSameDayAtDeviceZone(instant: Instant, timezoneProvider: TimezoneProvider) = this.toLocalDate(timezoneProvider) == instant.toLocalDate(timezoneProvider)

fun Instant.toLocalDate(timezoneProvider: TimezoneProvider): LocalDate = LocalDateTime.ofInstant(this, timezoneProvider.systemDefaultTimeZoneId()).toLocalDate()