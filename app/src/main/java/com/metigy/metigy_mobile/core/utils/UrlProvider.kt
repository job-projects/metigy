package com.metigy.metigy_mobile.core.utils

import java.net.URL
import javax.inject.Inject

class UrlProvider @Inject constructor() {
    fun url(url: String): URL = URL(url)
}