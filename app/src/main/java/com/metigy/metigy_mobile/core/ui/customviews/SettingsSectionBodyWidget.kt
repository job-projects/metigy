package com.metigy.metigy_mobile.core.ui.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.core.content.res.ResourcesCompat
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.databinding.WidgetSettingsSectionBodyBinding

class SettingsSectionBodyWidget(context: Context, attrs: AttributeSet) : LinearLayout(context, attrs) {
    init {
        orientation = VERTICAL
        setBackgroundColor(ResourcesCompat.getColor(resources, R.color.white, null))
        WidgetSettingsSectionBodyBinding.inflate(LayoutInflater.from(context), this, true)
    }
}