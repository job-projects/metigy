package com.metigy.metigy_mobile.core.ui.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.extension.applyAttributes
import com.metigy.metigy_mobile.databinding.WidgetSimpleToolbarBinding

class SimpleToolbarWidget(context: Context, attrs: AttributeSet) : FrameLayout(context, attrs) {

    var onBackClicked: () -> Unit = {}

    init {
        val binding = WidgetSimpleToolbarBinding.inflate(LayoutInflater.from(context), this, true)
        applyAttributes(attrs, R.styleable.SimpleToolbar) {
            binding.pageTitleTextView.text = it.getString(R.styleable.SimpleToolbar_toolbarTitle)
        }
        binding.backButton.setOnClickListener {
            onBackClicked()
        }
    }
}