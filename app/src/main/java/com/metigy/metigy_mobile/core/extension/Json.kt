package com.metigy.metigy_mobile.core.extension

import com.google.gson.Gson
import com.metigy.metigy_mobile.core.consts.TAG_JSON_ERROR
import timber.log.Timber

fun <T> String.asJson(type: Class<T>): T? =
    try {
        Gson().fromJson(this, type)
    } catch (e: Exception) {
        Timber.tag(TAG_JSON_ERROR).e(e, "Failed to parse the json:\t$this")
        null
    }