package com.metigy.metigy_mobile.core.extension

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

fun <T> LiveData<List<T>>.valueOrEmpty() = value.orEmpty()

fun <T> MutableLiveData<T>.refresh() {
    value = value
}