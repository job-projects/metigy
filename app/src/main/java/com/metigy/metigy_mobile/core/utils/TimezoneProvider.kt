package com.metigy.metigy_mobile.core.utils

import java.time.ZoneId
import javax.inject.Inject

interface TimezoneProvider {
    fun systemDefaultTimeZoneId(): ZoneId
}

class DefaultTimezoneProvider @Inject constructor() : TimezoneProvider {
    override fun systemDefaultTimeZoneId(): ZoneId = ZoneId.systemDefault()
}