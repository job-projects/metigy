package com.metigy.metigy_mobile.core.ui.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.extension.applyAttributes
import com.metigy.metigy_mobile.databinding.WidgetSettingsSectionHeadingBinding

class SettingsSectionHeadingWidget(context: Context, attrs: AttributeSet) : FrameLayout(context, attrs) {

    init {
        val binding: WidgetSettingsSectionHeadingBinding = WidgetSettingsSectionHeadingBinding.inflate(LayoutInflater.from(context), this, true)
        applyAttributes(attrs, R.styleable.SettingsSectionHeading) {
            binding.settingsSectionTitleTextView.text = it.getString(R.styleable.SettingsSectionHeading_titleText)
            binding.settingsSectionDescTextView.text = it.getString(R.styleable.SettingsSectionHeading_descriptionText)
            binding.settingsSectionImageView.setImageDrawable(it.getDrawable(R.styleable.SettingsSectionHeading_sectionIconSrc))
        }
    }
}