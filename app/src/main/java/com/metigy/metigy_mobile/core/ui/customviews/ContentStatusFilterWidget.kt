package com.metigy.metigy_mobile.core.ui.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.extension.setTextColorResId
import com.metigy.metigy_mobile.core.extension.setFont
import com.metigy.metigy_mobile.data.appmodels.ContentStatus
import com.metigy.metigy_mobile.data.appmodels.ContentStatus.*
import com.metigy.metigy_mobile.databinding.WidgetContentStatusFilterBinding

class ContentStatusFilterWidget(context: Context, attrs: AttributeSet) : FrameLayout(context, attrs) {

    var onStatusClicked: (ContentStatus) -> Unit = {}
    val binding: WidgetContentStatusFilterBinding = WidgetContentStatusFilterBinding.inflate(LayoutInflater.from(context), this, true)

    init {

        binding.scheduledTextView.setOnClickListener {
            onStatusClicked(SCHEDULED)
        }
        binding.postedTextView.setOnClickListener {
            onStatusClicked(POSTED)
        }
        binding.draftsTextView.setOnClickListener {
            onStatusClicked(DRAFT)
        }
    }

    fun setStatusSelected(status: ContentStatus) {
        when (status) {
            SCHEDULED -> {
                binding.scheduledTextView.apply {
                    setFont(R.font.sf_pro_text_black)
                    setTextColorResId(R.color.white)
                }
                binding.postedTextView.apply {
                    setFont(R.font.sf_pro_text_medium)
                    setTextColorResId(R.color.gray_text)
                }
                binding.draftsTextView.apply {
                    setFont(R.font.sf_pro_text_medium)
                    setTextColorResId(R.color.gray_text)
                }
            }
            POSTED -> {
                binding.scheduledTextView.apply {
                    setFont(R.font.sf_pro_text_medium)
                    setTextColorResId(R.color.gray_text)
                }
                binding.postedTextView.apply {
                    setFont(R.font.sf_pro_text_black)
                    setTextColorResId(R.color.white)
                }
                binding.draftsTextView.apply {
                    setFont(R.font.sf_pro_text_medium)
                    setTextColorResId(R.color.gray_text)
                }
            }
            DRAFT -> {
                binding.scheduledTextView.apply {
                    setFont(R.font.sf_pro_text_medium)
                    setTextColorResId(R.color.gray_text)
                }
                binding.postedTextView.apply {
                    setFont(R.font.sf_pro_text_medium)
                    setTextColorResId(R.color.gray_text)
                }
                binding.draftsTextView.apply {
                    setFont(R.font.sf_pro_text_black)
                    setTextColorResId(R.color.white)
                }
            }
        }
    }
}