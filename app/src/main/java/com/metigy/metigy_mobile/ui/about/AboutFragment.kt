package com.metigy.metigy_mobile.ui.about

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.metigy.metigy_mobile.core.extension.navigateUp
import com.metigy.metigy_mobile.core.extension.showOrInvisible
import com.metigy.metigy_mobile.data.appInfoRepository.ABOUT_APP_VIDEO_URL
import com.metigy.metigy_mobile.databinding.FragmentAboutBinding
import com.metigy.metigy_mobile.ui.hostactivities.RegistrationActivity
import com.metigy.metigy_mobile.ui.video.VideoPlayerActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AboutFragment : Fragment() {

    private lateinit var binding: FragmentAboutBinding

    private val viewModel: AboutViewModel by viewModels()

    //TODO So dirty
    private val isLoggedIn: Boolean by lazy {
        (requireActivity() is RegistrationActivity).not()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentAboutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUi()
        startObserving()
    }

    private fun setupUi() {
        binding.backButton.setOnClickListener {
            if (isLoggedIn) {
                requireActivity().onBackPressed()
            } else {
                navigateUp()
            }
        }

        binding.loginButton.setOnClickListener {
            navigateUp()
        }

        binding.introPreviewImageView.setOnClickListener {
            startActivity(Intent(context, VideoPlayerActivity::class.java).apply {
                putExtra(VideoPlayerActivity.BUNDLE_KEY_URL, ABOUT_APP_VIDEO_URL)
            })
        }

        binding.alreadyCustomerTextView.showOrInvisible(isLoggedIn.not())
        binding.loginButton.showOrInvisible(isLoggedIn.not())
    }

    private fun startObserving() {
        viewModel.appVersion.observe(viewLifecycleOwner) { appVersion ->
            binding.versionTextView.text = viewModel.prettifyVersion(appVersion)
        }
    }
}