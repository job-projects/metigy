package com.metigy.metigy_mobile.ui.hostfragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.metigy.metigy_mobile.databinding.FragmentHomeHostBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeHostFragment : Fragment() {
    private lateinit var binding:FragmentHomeHostBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentHomeHostBinding.inflate(inflater, container, false)
        return binding.root
    }
}