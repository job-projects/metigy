package com.metigy.metigy_mobile.ui.settings.pushnotification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.metigy.metigy_mobile.core.extension.navigateUp
import com.metigy.metigy_mobile.databinding.FragmentPushSettingsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PushSettingsFragment : Fragment() {

    private lateinit var binding: FragmentPushSettingsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentPushSettingsBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUi()
    }

    private fun setupUi() {
        binding.toolbarLayout.onBackClicked = {
            navigateUp()
        }
    }
}