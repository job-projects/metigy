package com.metigy.metigy_mobile.ui.forgetpassword

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.metigy.metigy_mobile.core.result.Event
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.data.authentication.Authentication
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ForgotPasswordViewModel @Inject constructor(
    private val authentication: Authentication
) : ViewModel() {

    private val _error = MutableLiveData<Event<Result.Error>>()
    val error: LiveData<Event<Result.Error>>
        get() = _error

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    private val _newPasswordSent = MutableLiveData<Unit>()
    val newPasswordSent: LiveData<Unit>
        get() = _newPasswordSent

    fun resetPassword(email: String) {
        viewModelScope.launch {
            _loading.value = true
            val result = authentication.resetPassword(email)
            _loading.value = false
            when (result) {
                is Result.Success -> {
                    _newPasswordSent.value = Unit
                }
                is Result.Error -> {
                    _error.value = Event(result)
                }
            }
        }
    }
}