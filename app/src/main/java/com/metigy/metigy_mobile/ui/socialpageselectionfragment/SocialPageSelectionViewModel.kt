package com.metigy.metigy_mobile.ui.socialpageselectionfragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.metigy.metigy_mobile.core.result.Event
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.data.appInfoRepository.AppInfoRepository
import com.metigy.metigy_mobile.data.appmodels.SocialPageInfoModel
import com.metigy.metigy_mobile.data.appmodels.SocialPlatform
import com.metigy.metigy_mobile.data.brandrepository.BrandRepository
import com.metigy.metigy_mobile.data.platformpagesrepository.PlatformPagesRepository
import com.metigy.metigy_mobile.data.socialaccountconnect.SocialAccountConnect
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SocialPageSelectionViewModel @Inject constructor(
    private val socialAccountConnect: SocialAccountConnect,
    private val brandRepository: BrandRepository,
    private val platformPagesRepository: PlatformPagesRepository,
    private val appInfoRepository: AppInfoRepository
) : ViewModel() {

    private val _socialPageInfoList = MutableLiveData<List<SocialPageInfoModel>>()
    val socialPageInfoList: LiveData<List<SocialPageInfoModel>>
        get() = _socialPageInfoList

    private val _pageConnectionDone = MutableLiveData<Event<Unit>>()
    val pageConnectionDone: LiveData<Event<Unit>>
        get() = _pageConnectionDone

    private val _pageListLoading = MutableLiveData<Boolean>()
    val pageListLoading: LiveData<Boolean>
        get() = _pageListLoading

    private val _pageConnectingProgress = MutableLiveData<Boolean>()
    val pageConnectingProgress: LiveData<Boolean>
        get() = _pageConnectingProgress

    private val _error = MutableLiveData<Event<Result.Error>>()
    val error: LiveData<Event<Result.Error>>
        get() = _error

    val learnMoreUrl: String
        get() = appInfoRepository.learnMoreUrlForInstagramContentPublishing


    fun loadPages(socialAccountUuid: String, socialPlatform: SocialPlatform) {
        viewModelScope.launch {
            _pageListLoading.value = true
            val result = socialAccountConnect.getPages(socialAccountUuid, socialPlatform)
            _pageListLoading.value = false
            when (result) {
                is Result.Success -> {
                    _socialPageInfoList.value = result.data.apply { }
                }
                is Result.Error -> {
                    _error.value = Event(result)
                }
            }
        }
    }

    fun onConnectClicked(selectedPages: List<SocialPageInfoModel>, uuid: String, socialPlatform: SocialPlatform) {
        viewModelScope.launch {
            _pageConnectingProgress.value = true
            val listBrandResult = brandRepository.listBrandGroups()
            _pageConnectingProgress.value = false
            when (listBrandResult) {
                is Result.Success -> {
                    listBrandResult.data.firstOrNull()?.id?.let { brandGroupId ->
                        _pageConnectingProgress.value = true
                        val brandResult = brandRepository.getBrand(brandGroupId)
                        _pageConnectingProgress.value = false
                        when (brandResult) {
                            is Result.Success -> {
                                brandResult.data.firstOrNull()?.id?.let { brandId ->
                                    _pageConnectingProgress.value = true

                                    //TODO For Rahul: It's a bug. Currently it's ignoring errors and it navigates to the next screen anyway, which is not a correct behavior. It must navigate only when adding page is done successfully.
                                    platformPagesRepository.addPage(
                                        pageExternalIds = selectedPages.map {
                                            it.externalId
                                        }, brandId = brandId, brandGroupId = brandGroupId, uuid = uuid, socialPlatform = socialPlatform
                                    )
                                    _pageConnectingProgress.value = false
                                    _pageConnectionDone.value = Event(Unit)
                                } ?: run {
                                    //TODO Backend illegal state
                                }
                            }
                            is Result.Error -> {
                                _error.value = Event(brandResult)
                            }
                        }
                    } ?: run {
                        //TODO Backend illegal state
                    }
                }
                is Result.Error -> {
                    _error.value = Event(listBrandResult)
                }
            }
        }
    }
}