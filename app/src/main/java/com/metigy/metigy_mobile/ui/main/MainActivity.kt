package com.metigy.metigy_mobile.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.findNavController
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.consts.TAG_DEBUG
import com.metigy.metigy_mobile.databinding.ActivityMainBinding
import com.metigy.metigy_mobile.ui.about.AboutActivity
import com.metigy.metigy_mobile.ui.hostfragments.HomeHostFragment
import com.metigy.metigy_mobile.ui.hostfragments.SettingsHostFragment
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), AccountBottomSheetDialogFragment.AccountBottomSheetActions {

    private lateinit var binding: ActivityMainBinding

    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isRunning = true
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupViewPagerAndBottomAppBar()
    }

    override fun onStart() {
        super.onStart()
        viewModel.reAuthenticateIfNeeded()
    }

    override fun onDestroy() {
        isRunning = false
        super.onDestroy()
    }

    private fun setupViewPagerAndBottomAppBar() {
        binding.viewPager.adapter = MainPagerAdapter(this)
        binding.viewPager.isUserInputEnabled = false

        binding.plannerTextView.setOnClickListener {
            binding.viewPager.setCurrentItem(TAB_NUMBER_HOME, false)
        }

        binding.settingsTextView.setOnClickListener {
            binding.viewPager.setCurrentItem(TAB_NUMBER_SETTINGS, false)
        }

        val cornerSize = resources.getDimension(R.dimen.bottom_app_bar_corner_size)
        val bottomBarBackground = binding.bottomAppBar.background as MaterialShapeDrawable

        val shapeAppearanceModel = bottomBarBackground.shapeAppearanceModel.toBuilder()
            .setTopLeftCorner(CornerFamily.ROUNDED, cornerSize)
            .setTopRightCorner(CornerFamily.ROUNDED, cornerSize)
            .build()

        val backgroundDrawable = MaterialShapeDrawable(shapeAppearanceModel).apply {
            setTint(ContextCompat.getColor(this@MainActivity, R.color.black))
            setStroke(1f, ContextCompat.getColor(this@MainActivity, R.color.white))
        }

        binding.bottomAppBar.background = backgroundDrawable
    }

    override fun onAboutClicked() {
        Timber.tag(TAG_DEBUG).d("About")
        startActivity(Intent(this, AboutActivity::class.java))
    }

    override fun onLogoutClicked() {
        //TODO Remove it later
    }

    private inner class MainPagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {
        override fun getItemCount(): Int = NUMBER_OF_TOP_LEVEL_SCREENS

        override fun createFragment(position: Int): Fragment =
            if (position == TAB_NUMBER_HOME) {
                HomeHostFragment()
            } else {
                SettingsHostFragment()
            }
    }

    override fun onBackPressed() {
        val rootFragment = findNavController(R.id.nav_host_fragment).navigateUp().not()
        if (rootFragment) {
            super.onBackPressed()
        }
    }

    companion object {
        const val NUMBER_OF_TOP_LEVEL_SCREENS = 2

        const val TAB_NUMBER_HOME = 0
        const val TAB_NUMBER_SETTINGS = 1

        var isRunning = false

        const val BUNDLE_KEY_NOTIFICATION_CONTENT = "notificationContent" //Must match the argument name in nav_graph_home.xml
    }
}
