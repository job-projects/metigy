package com.metigy.metigy_mobile.ui.socialplatformselection

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.utils.StringResProvider
import com.metigy.metigy_mobile.data.appmodels.SocialPageType
import com.metigy.metigy_mobile.databinding.ItemSocialPlatformToSelectBinding
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.android.EntryPointAccessors
import dagger.hilt.components.SingletonComponent

class SocialPlatformRecyclerViewAdapter(private val context: Context) : RecyclerView.Adapter<SocialPlatformRecyclerViewAdapter.SocialPlatformViewHolder>() {
    private val stringProvider: StringResProvider by lazy {
        val adapterEntryPoint = EntryPointAccessors.fromApplication(context, AdapterEntryPoint::class.java)
        adapterEntryPoint.stringResProvider()
    }

    var data: List<SocialPlatformToConnect> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var onPlatformClicked: (SocialPlatformToConnect) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SocialPlatformViewHolder =
        SocialPlatformViewHolder(ItemSocialPlatformToSelectBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: SocialPlatformViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    inner class SocialPlatformViewHolder(private val binding: ItemSocialPlatformToSelectBinding) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                onPlatformClicked(data[adapterPosition])
            }
        }

        fun bind(platform: SocialPlatformToConnect) {
            val socialPlatformForView = platform.socialPlatform.toSocialPlatformForView()
            binding.socialPlatformImageView.setImageResource(socialPlatformForView.coloredIconId)
            binding.socialPageNameTextView.setText(socialPlatformForView.platformNameResId)
            binding.accountTypesTextView.text = accountTypesToText(platform.socialPlatform.allPageTypes)
        }

        private fun accountTypesToText(accountTypes: List<SocialPageType>) = accountTypes.joinToString(separator = stringProvider.getString(R.string.spaced_and)) { pageType ->
            stringProvider.getString(pageType.toSocialPageTypeForView().typeNameResId)
        } + stringProvider.getString(R.string.space_pages)
    }

    @EntryPoint
    @InstallIn(SingletonComponent::class)
    interface AdapterEntryPoint {
        fun stringResProvider(): StringResProvider
    }
}


