package com.metigy.metigy_mobile.ui.launcher

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.metigy.metigy_mobile.core.extension.addFlagsToClearBackstack
import com.metigy.metigy_mobile.core.extension.orFalse
import com.metigy.metigy_mobile.core.extension.showErrorToast
import com.metigy.metigy_mobile.core.result.EventObserver
import com.metigy.metigy_mobile.data.airship.NotificationContent
import com.metigy.metigy_mobile.data.appmodels.AuthenticationRequirementStatus
import com.metigy.metigy_mobile.ui.hostactivities.RegistrationActivity
import com.metigy.metigy_mobile.ui.main.MainActivity
import com.metigy.metigy_mobile.ui.message.MessageActivity
import com.metigy.metigy_mobile.ui.socialprofileconnect.SocialProfileConnectActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LauncherActivity : AppCompatActivity() {

    private val viewModel: LauncherViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.checkAppConfig()
        startObserving()
    }

    private fun startObserving() {
        viewModel.navigateToLoginScreen.observe(this) { status ->
            when (status) {
                is AuthenticationRequirementStatus.AuthenticationNeededCredentialsAvailable -> {
                    val intent = Intent(this, RegistrationActivity::class.java).apply {
                        putExtra(RegistrationActivity.BUNDLE_KEY_USER_CREDENTIALS, status.userCredentials)
                        addFlagsToClearBackstack()
                    }
                    startActivity(intent)
                    finish()
                }
                AuthenticationRequirementStatus.AuthenticationNeededCredentialsNotAvailable -> {
                    val intent = Intent(this, RegistrationActivity::class.java).apply {
                        addFlagsToClearBackstack()
                    }
                    startActivity(intent)
                    finish()
                }
                else -> {
                    //Must never happen
                }
            }
        }

        viewModel.navigateToHomeScreen.observe(this) { notificationContent ->
            val intent = Intent(this, MainActivity::class.java).apply {
                addFlagsToClearBackstack()

                putExtra(MainActivity.BUNDLE_KEY_NOTIFICATION_CONTENT, notificationContent)
            }
            startActivity(intent)
            finish()
        }

        viewModel.navigateToSocialProfileConnect.observe(this) {
            val intent = Intent(this, SocialProfileConnectActivity::class.java).apply {
                addFlagsToClearBackstack()
            }
            startActivity(intent)
            finish()
        }

        viewModel.error.observe(this, EventObserver { error ->
            showErrorToast(error)
            finish()
        })

        viewModel.appIsGoodToGo.observe(this, EventObserver {

            val pushContent = pushNotificationContent()
            val emailVerificationUrl = emailVerificationUrl()
            when {
                emailVerificationUrl != null -> {
                    viewModel.callVerificationUrlAndNavigate(emailVerificationUrl)
                }
                pushContent != null -> {
                    viewModel.handlePushNotification(pushContent, isAppRunning())
                }
                else -> {
                    viewModel.findAuthenticationStatusAndNavigate()
                }
            }

        })

        viewModel.forceUpdateMessage.observe(this) { forceUpdate ->
            val intent = MessageActivity.intent(this, forceUpdate.title, forceUpdate.description).apply {
                addFlagsToClearBackstack()
            }
            startActivity(intent)
        }
    }

    private fun emailVerificationUrl() = intent?.data?.toString()

    private fun pushNotificationContent() = intent?.getParcelableExtra<NotificationContent>(BUNDLE_KEY_PUSH_NOTIFICATION_CONTENT)

    private fun isAppRunning() = intent?.getBooleanExtra(BUNDLE_KEY_PUSH_NOTIFICATION_APP_RUNNING, false).orFalse()

    companion object {
        const val BUNDLE_KEY_PUSH_NOTIFICATION_CONTENT = "PushNotificationContent"
        const val BUNDLE_KEY_PUSH_NOTIFICATION_APP_RUNNING = "PushNotificationAppRunning"
    }
}