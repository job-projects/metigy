package com.metigy.metigy_mobile.ui.socialplatformselection

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.result.Event
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.data.appInfoRepository.AppInfoRepository
import com.metigy.metigy_mobile.data.appmodels.SocialPlatform
import com.metigy.metigy_mobile.data.socialaccountconnect.SocialAccountConnect
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SocialPlatformSelectionViewModel @Inject constructor(
    private val socialAccountConnect: SocialAccountConnect,
    private val appInfo: AppInfoRepository,
) : ViewModel() {
    val platformList: List<SocialPlatformToConnect> = listOf(
        SocialPlatformToConnect.FACEBOOK, SocialPlatformToConnect.INSTAGRAM, SocialPlatformToConnect.TWITTER, SocialPlatformToConnect.LINKEDIN
    )

    private val _navigateToPageSelection = MutableLiveData<Event<SocialPlatformAndUuid>>()
    val navigateToPageSelection: LiveData<Event<SocialPlatformAndUuid>>
        get() = _navigateToPageSelection

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    private val _error = MutableLiveData<Event<Result.Error>>()
    val error: LiveData<Event<Result.Error>>
        get() = _error

    var lastPlatformClicked: SocialPlatformToConnect? = null

    val helpUrl: String
        get() = appInfo.helpUrl

    val supportEmail: String
        get() = appInfo.contactEmailUrl

    val learnMoreUrl: String
        get() = appInfo.learnMoreUrlForFacebookLogin

    var brandGroupId: Int? = null

    fun sendSocialLoginTokenToServer(socialToken: String, socialPlatform: SocialPlatform) {
        viewModelScope.launch {
            _loading.value = true
            val result = socialAccountConnect.sendSocialTokenToServer(socialToken = socialToken, socialPlatform = socialPlatform)
            _loading.value = false
            when (result) {
                is Result.Success -> {
                    _navigateToPageSelection.value = Event(SocialPlatformAndUuid(socialPlatform = socialPlatform, uuid = result.data))
                }
                is Result.Error -> {
                    _error.value = Event(result)
                }
            }
        }
    }

}

enum class SocialPlatformToConnect(val socialPlatform: SocialPlatform, val connectConfirmMessageResId: Int) {
    INSTAGRAM(socialPlatform = SocialPlatform.INSTAGRAM, connectConfirmMessageResId = R.string.instagram_auth_confirm_message),
    FACEBOOK(socialPlatform = SocialPlatform.FACEBOOK, connectConfirmMessageResId = R.string.facebook_auth_confirm_message),
    TWITTER(socialPlatform = SocialPlatform.TWITTER, connectConfirmMessageResId = R.string.twitter_auth_confirm_message),
    LINKEDIN(socialPlatform = SocialPlatform.LINKEDIN, connectConfirmMessageResId = R.string.linkedin_auth_confirm_message);
}

data class SocialPlatformAndUuid(val socialPlatform: SocialPlatform, val uuid: String)