package com.metigy.metigy_mobile.ui.main.dropdown

import android.graphics.Rect
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.extension.invisible
import com.metigy.metigy_mobile.core.extension.loadImage
import com.metigy.metigy_mobile.core.extension.visible
import com.metigy.metigy_mobile.databinding.ItemDropDownPreviewItemBinding

class DropDownPreviewRecyclerViewAdapter : RecyclerView.Adapter<DropDownPreviewRecyclerViewAdapter.PreviewViewHolder>() {

    var onItemClicked: () -> Unit = {}

    var data: List<DropDownPreviewLogoItem> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PreviewViewHolder =
        PreviewViewHolder(ItemDropDownPreviewItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: PreviewViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size


    inner class PreviewViewHolder(private val binding: ItemDropDownPreviewItemBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener {
                onItemClicked()
            }
        }

        fun bind(item: DropDownPreviewLogoItem) {
            when (item) {
                is DropDownPreviewLogoItem.BrandGroup -> {
                    binding.logoImageView.loadImage(item.url)
                    binding.logoImageView.visible()
                    binding.plusImageView.invisible()
                }
                is DropDownPreviewLogoItem.SocialPlatform -> {
                    binding.logoImageView.setImageResource(item.socialPlatform.toSocialPlatformForView().coloredIconId)
                    binding.logoImageView.visible()
                    binding.plusImageView.invisible()
                }
                DropDownPreviewLogoItem.Plus -> {
                    binding.logoImageView.setImageResource(R.drawable.ic_connect_social_profile)
                    binding.logoImageView.invisible()
                    binding.plusImageView.visible()
                }
            }
        }
    }

    class ItemDecorator : ItemDecoration() {
        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
            val position = parent.getChildAdapterPosition(view)
            if (position != 0) outRect.left = -(parent.context.resources.getDimension(R.dimen.brand_group_drop_down_preview_image_size) / 3.0).toInt()
        }
    }
}


sealed class DropDownPreviewLogoItem {
    data class BrandGroup(val url: String) : DropDownPreviewLogoItem()
    data class SocialPlatform(val socialPlatform: com.metigy.metigy_mobile.data.appmodels.SocialPlatform) : DropDownPreviewLogoItem()
    object Plus : DropDownPreviewLogoItem()
}