package com.metigy.metigy_mobile.ui.emailverification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.extension.showErrorToast
import com.metigy.metigy_mobile.core.extension.showOrInvisible
import com.metigy.metigy_mobile.core.extension.showSimpleDialog
import com.metigy.metigy_mobile.core.result.EventObserver
import com.metigy.metigy_mobile.databinding.FragmentResendEmailBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ResendEmailFragment : Fragment() {
    private lateinit var binding: FragmentResendEmailBinding

    private val viewModel: ResendEmailViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentResendEmailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUi()
        startObserving()
    }

    private fun setupUi() {
        binding.resendEmailButton.setOnClickListener {
            viewModel.resendEmail()
        }
    }

    private fun startObserving() {
        viewModel.displayEmailSentDialog.observe(viewLifecycleOwner) { display ->
            if (display) {
                showSimpleDialog(
                    titleId = R.string.resend_email,
                    messageId = R.string.verification_email_sent,
                    positiveButtonTextId = R.string.ok,
                    onConfirm = {
                        viewModel.hideEmailSentDialog()
                    }
                )
            }
        }

        viewModel.error.observe(viewLifecycleOwner, EventObserver { error ->
            showErrorToast(error)
        })

        viewModel.loading.observe(viewLifecycleOwner) { loading ->
            binding.progressBar.showOrInvisible(loading)
            binding.resendEmailButton.showOrInvisible(loading.not())
        }
    }
}