package com.metigy.metigy_mobile.ui.connectedsocialpages

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.extension.refresh
import com.metigy.metigy_mobile.core.extension.valueOrEmpty
import com.metigy.metigy_mobile.core.result.Event
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.data.appInfoRepository.AppInfoRepository
import com.metigy.metigy_mobile.data.appmodels.PlatformPageModel
import com.metigy.metigy_mobile.data.brandrepository.BrandRepository
import com.metigy.metigy_mobile.data.platformpagesrepository.PlatformPagesRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ConnectedSocialPagesViewModel @Inject constructor(
    private val platformPagesRepository: PlatformPagesRepository,
    private val brandRepository: BrandRepository,
    private val appInfo: AppInfoRepository
) : ViewModel() {

    private val _connectedPages = MutableLiveData<List<PlatformPageModel>>()
    val connectedPages: LiveData<List<PlatformPageModel>>
        get() = _connectedPages

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    private val _error = MutableLiveData<Event<Result.Error>>()
    val error: LiveData<Event<Result.Error>>
        get() = _error

    val helpUrl: String
        get() = appInfo.helpUrl

    val supportEmail: String
        get() = appInfo.contactEmailUrl

    var brandGroupId: Int? = null

    init {
        onLoadingConnectedPages()
    }

    fun onLoadingConnectedPages() {
        viewModelScope.launch {
            when (val brandGroupResult = getBrandGroupId()) {
                is Result.Success -> {
                    loadConnectedPages(brandGroupResult.data)
                }
                is Result.Error -> {
                    _error.value = Event(brandGroupResult)
                }
            }
        }
    }

    fun removePage(pageId: Int) {
        viewModelScope.launch {
            val result = platformPagesRepository.removePage(pageId, brandGroupId ?: error("Illegal state. BrandGroupId not found"))
            when (result) {
                is Result.Success -> {
                    _connectedPages.value = _connectedPages.valueOrEmpty().filterNot {
                        it.id == pageId
                    }
                }
                is Result.Error -> {
                    _connectedPages.refresh()
                    _error.value = Event(result)
                }
            }
        }
    }


    suspend fun getBrandGroupId(): Result<Int> {
        return brandGroupId?.let {
            Result.Success(it)
        } ?: run {
            _loading.value = true
            val brandGroups = brandRepository.listBrandGroups()
            _loading.value = false
            when (brandGroups) {
                is Result.Success -> {
                    brandGroups.data.firstOrNull()?.id?.let {
                        brandGroupId = it
                        Result.Success(it)
                    } ?: Result.Error(messageId = R.string.unknown_error)
                }
                is Result.Error -> {
                    brandGroups
                }
            }
        }
    }

    private suspend fun loadConnectedPages(brandGroupId: Int) {
        _loading.value = true
        val pagesResult = platformPagesRepository.getPlatformPages(brandGroupId = brandGroupId)
        _loading.value = false
        when (pagesResult) {
            is Result.Success -> {
                _connectedPages.value = pagesResult.data.apply { }
            }
            is Result.Error -> {
                _error.value = Event(pagesResult)
            }
        }
    }
}