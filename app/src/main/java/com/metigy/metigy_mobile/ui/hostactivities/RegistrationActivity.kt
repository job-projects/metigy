package com.metigy.metigy_mobile.ui.hostactivities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.metigy.metigy_mobile.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegistrationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
    }

    companion object{
        const val BUNDLE_KEY_USER_CREDENTIALS = "UserCredentials"
    }
}