package com.metigy.metigy_mobile.ui.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.metigy.metigy_mobile.core.extension.navigate
import com.metigy.metigy_mobile.core.extension.relaunchApp
import com.metigy.metigy_mobile.databinding.FragmentSettingsBinding
import com.metigy.metigy_mobile.ui.main.MainViewModel
import com.metigy.metigy_mobile.ui.main.dropdown.BrandGroupSelectionArrayAdapter
import com.metigy.metigy_mobile.ui.main.dropdown.PageSelectionArrayAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SettingsFragment : Fragment() {

    private lateinit var binding: FragmentSettingsBinding

    private val viewModel: SettingsViewModel by viewModels()

    private val parentActivityViewModel: MainViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSettingsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUi()
        startObserving()
    }

    private fun setupUi() {
        binding.logoutLayout.setOnClickListener {
            viewModel.logout()
            activity?.relaunchApp()
        }

        binding.pushNotificationItem.setOnClickListener {
            navigate(SettingsFragmentDirections.toPushSettingsFragment())
        }

        binding.timeZoneItem.setOnClickListener {
            navigate(SettingsFragmentDirections.toTimeZoneSettingsFragment())
        }

    }

    //FIXME Think of a way to avoid repeating this code (Same code is in ContentListFragment)
    private fun startObserving() {
        parentActivityViewModel.selectedBrandGroupAndPages.observe(viewLifecycleOwner) { pair ->

            val pageSelectionArrayAdapter = PageSelectionArrayAdapter.newInstance(
                requireContext(), pair, parentActivityViewModel.deselectedPageSet,
                parentActivityViewModel.buildBrandGroupDropDownPreviewList(parentActivityViewModel.pageListToSocialPlatformList(pair.second), pair.first)
            )

            pageSelectionArrayAdapter.onPreviewClicked = {
                binding.spinner.performClick()
            }

            pageSelectionArrayAdapter.onBrandGroupSwitchClicked = {
                val brandGroupArrayAdapter = BrandGroupSelectionArrayAdapter.newInstance(requireContext(), parentActivityViewModel.getAllBrandGroups(), pair.first.id)
                brandGroupArrayAdapter.onBrandGroupClicked = { id ->
                    parentActivityViewModel.brandGroupSelected(id)
                }
                binding.spinner.adapter = brandGroupArrayAdapter
            }

            pageSelectionArrayAdapter.onPageClicked = {
                parentActivityViewModel.selectedPagesChanged()
            }
            binding.spinner.adapter = pageSelectionArrayAdapter
        }

    }
}