package com.metigy.metigy_mobile.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.extension.*
import com.metigy.metigy_mobile.core.result.EventObserver
import com.metigy.metigy_mobile.data.appmodels.UserCredentialsModel
import com.metigy.metigy_mobile.databinding.FragmentLoginBinding
import com.metigy.metigy_mobile.ui.hostactivities.RegistrationActivity.Companion.BUNDLE_KEY_USER_CREDENTIALS
import com.metigy.metigy_mobile.ui.intro.IntroFragmentDirections
import com.metigy.metigy_mobile.ui.intro.LoginScreenActions
import com.metigy.metigy_mobile.ui.main.MainActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment : Fragment() {

    private lateinit var binding: FragmentLoginBinding

    private val viewModel: LoginViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        readUserCredentialsFromParentActivity()?.let { userCredential ->
            viewModel.savedCredentials = userCredential
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUi()
        startObserving()
    }

    private fun setupUi() {
        binding.backButton.setOnClickListener {
            (parentFragment as LoginScreenActions).onBackClicked()
        }

        binding.loginButton.setOnClickListener {
            hideKeyboard()
            viewModel.login(
                email = binding.userNameEditText.textValue(),
                password = binding.passwordEditText.textValue(),
                saveCredentials = rememberCredentials()
            )
        }

        binding.signUpNowTextView.setOnClickListener {
            parentFragment?.navigate(IntroFragmentDirections.toSignUpFragment())
        }

        binding.forgotPasswordButton.setOnClickListener {
            parentFragment?.navigate(IntroFragmentDirections.toForgotPasswordFragment())
        }

        binding.logoTextView.setStartDrawableStart(drawableId = R.drawable.logo_simple, sizeInDP = 32)

        loadCredentials()

        toggleButtonState(binding.userNameEditText.textValue(), binding.passwordEditText.textValue())
    }

    private fun startObserving() {
        viewModel.navigateToHomeScreen.observe(viewLifecycleOwner, EventObserver {
            startActivity(Intent(activity, MainActivity::class.java))
            requireActivity().finish()
        }
        )

        viewModel.navigateToEmailVerificationScreen.observe(viewLifecycleOwner, EventObserver {
            navigate(IntroFragmentDirections.toEmailVerificationFragment())
        })

        viewModel.loginErrorMessage.observe(viewLifecycleOwner, EventObserver { error ->
            showErrorToast(error)
        })

        viewModel.loginLoading.observe(viewLifecycleOwner) { loading ->
            binding.progressBar.showOrInvisible(loading)
            binding.loginButton.showOrInvisible(loading.not())
        }

        binding.userNameEditText.doAfterTextChanged {
            binding.userNameTextInputLayout.error = if (it.toString().isValidEmail()) null else getString(R.string.error_invalid_email)
            toggleButtonState(it.toString(), binding.passwordEditText.textValue())
        }

        binding.passwordEditText.doAfterTextChanged {
            binding.passwordTextInputLayout.error = if (it.toString().isBlank().not()) null else getString(R.string.error_blank_password)
            toggleButtonState(binding.userNameEditText.textValue(), it.toString())
        }
    }

    private fun toggleButtonState(email: String, password: String) {
        binding.loginButton.isEnabled = email.isValidEmail() && password.isBlank().not()
    }

    private fun rememberCredentials(): Boolean = binding.rememberCheckBox.isChecked

    private fun loadCredentials() {
        viewModel.savedCredentials?.let { userCredentials ->
            binding.userNameEditText.setText(userCredentials.userName)
            binding.passwordEditText.setText(userCredentials.password)
            binding.rememberCheckBox.isChecked = true
        }
    }

    private fun readUserCredentialsFromParentActivity() = activity?.intent?.getParcelableExtra<UserCredentialsModel>(BUNDLE_KEY_USER_CREDENTIALS)
}