package com.metigy.metigy_mobile.ui.socialpageselectionfragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.extension.*
import com.metigy.metigy_mobile.core.result.EventObserver
import com.metigy.metigy_mobile.databinding.FragmentSocialPageSelectionBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SocialPageSelectionFragment : Fragment() {

    private lateinit var binding: FragmentSocialPageSelectionBinding

    private val viewModel: SocialPageSelectionViewModel by viewModels()

    private val args: SocialPageSelectionFragmentArgs by navArgs()

    private val adapter: SocialPageToSelectRecyclerViewAdapter by lazy {
        SocialPageToSelectRecyclerViewAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.loadPages(args.uuid, args.socialPlatform)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSocialPageSelectionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUi()
        startObserving()
    }

    private fun setupUi() {
        binding.titleTextView.text = getString(R.string.which_page_to_connect, getString(args.socialPlatform.toSocialPlatformForView().platformNameResId))
        binding.learnMoreButton.setOnClickListener {
            openUrl(viewModel.learnMoreUrl)
        }

        binding.cancelButton.setOnClickListener {
            navigateUp()
        }

        binding.socialPlatformImageView.setImageResource(args.socialPlatform.toSocialPlatformForView().coloredIconId)
        binding.socialPageRecyclerView.gridLayoutManager(2)
        binding.socialPageRecyclerView.adapter = adapter
        adapter.atLeastOnePageIsSelected = { somethingIsSelected ->
            binding.connectButton.showOrGone(somethingIsSelected)
        }

        binding.swipeRefreshLayout.setOnRefreshListener {
            viewModel.loadPages(args.uuid, args.socialPlatform)
        }

        binding.connectButton.setOnClickListener {
            viewModel.onConnectClicked(selectedPages = adapter.selectedPages, uuid = args.uuid, socialPlatform = args.socialPlatform)
        }
    }

    private fun startObserving() {
        viewModel.socialPageInfoList.observe(viewLifecycleOwner) { pageInfoList ->
            adapter.data = pageInfoList
        }

        viewModel.error.observe(viewLifecycleOwner, EventObserver { error ->
            showErrorToast(error)
        })

        viewModel.pageListLoading.observe(viewLifecycleOwner) { loading ->
            binding.swipeRefreshLayout.isRefreshing = loading
        }

        viewModel.pageConnectionDone.observe(viewLifecycleOwner, EventObserver {
            navigate(SocialPageSelectionFragmentDirections.toConnectedSocialPagesFragment())
        })

        viewModel.pageConnectingProgress.observe(viewLifecycleOwner) { progress ->
            binding.connectButton.isEnabled = progress.not()
            binding.swipeRefreshLayout.isRefreshing = progress
        }
    }
}