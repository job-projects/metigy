package com.metigy.metigy_mobile.ui.forgetpassword

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.extension.*
import com.metigy.metigy_mobile.core.result.EventObserver
import com.metigy.metigy_mobile.databinding.FragmentForgotPasswordBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ForgotPasswordFragment : Fragment() {

    private lateinit var binding: FragmentForgotPasswordBinding

    private val viewModel: ForgotPasswordViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentForgotPasswordBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUi()
        startObserving()
    }

    private fun setupUi() {
        setResetButtonState(binding.emailEditText.textValue())

        binding.resetPasswordButton.setOnClickListener {
            hideKeyboard()
            viewModel.resetPassword(binding.emailEditText.textValue())
        }

        binding.emailEditText.doAfterTextChanged { editable ->
            val email = editable.toString()
            binding.emailTextInputLayout.error = if (email.isValidEmail()) null else getString(R.string.error_invalid_email)
            setResetButtonState(email)
        }


        binding.cancelTextView.setOnClickListener {
            navigateUp()
        }

        binding.doneButton.setOnClickListener {
            navigateUp()
        }
    }

    private fun startObserving() {
        viewModel.error.observe(viewLifecycleOwner, EventObserver { error ->
            showErrorToast(error)
        })

        viewModel.loading.observe(viewLifecycleOwner) { loading ->
            binding.progressBar.showOrInvisible(loading)
            binding.resetPasswordButton.showOrInvisible(loading.not())
        }

        viewModel.newPasswordSent.observe(viewLifecycleOwner) {
            binding.resetPasswordButton.invisible()
            binding.resetPasswordMessageTextView.visible()
            binding.emailTextInputLayout.invisible()
            binding.doneButton.visible()
        }
    }

    private fun setResetButtonState(email: String) {
        binding.resetPasswordButton.isEnabled = email.isValidEmail()
    }
}