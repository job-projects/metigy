package com.metigy.metigy_mobile.ui.message

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.metigy.metigy_mobile.core.extension.openUrl
import com.metigy.metigy_mobile.databinding.ActivityMessageBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MessageActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMessageBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMessageBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupUi()
    }

    private fun setupUi() {
        val title = titleFromExtras()
        val description = descriptionFromExtras()

        binding.messageTitleTextView.text = title
        binding.messageDescriptionTextView.text = description

        binding.actionButton.setOnClickListener {
            try {
                openUrl("market://details?id=$packageName")
            } catch (e: ActivityNotFoundException) {
                openUrl("https://play.google.com/store/apps/details?id=$packageName")
            }
        }
    }

    private fun titleFromExtras() = intent.getStringExtra(BUNDLE_KEY_TITLE).orEmpty()
    private fun descriptionFromExtras() = intent.getStringExtra(BUNDLE_KEY_DESCRIPTION).orEmpty()

    companion object {
        private const val BUNDLE_KEY_TITLE = "key"
        private const val BUNDLE_KEY_DESCRIPTION = "description"

        fun intent(context: Context, title: String, description: String) =
            Intent(context, MessageActivity::class.java).apply {
                putExtra(BUNDLE_KEY_TITLE, title)
                putExtra(BUNDLE_KEY_DESCRIPTION, description)
            }
    }
}