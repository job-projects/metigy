package com.metigy.metigy_mobile.ui.video

import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.widget.MediaController
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.metigy.metigy_mobile.core.extension.gone
import com.metigy.metigy_mobile.core.extension.visible
import com.metigy.metigy_mobile.databinding.ActivityVideoBinding
import dagger.hilt.android.AndroidEntryPoint

//TODO: This class needs to be written in a neater way. Or use another library as player.

@AndroidEntryPoint
class VideoPlayerActivity : AppCompatActivity() {

    private lateinit var binding: ActivityVideoBinding

    private val viewModel:VideoPlayerViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityVideoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupUi()
    }

    private fun setupUi() {
        binding.backButton.setOnClickListener {
            onBackPressed()
        }
        setupVideoPlayer(getUrlFromBundle())
    }

    private fun setupVideoPlayer(url: String) {
        val uri = Uri.parse(url)
        binding.videoView.setVideoURI(uri)
        binding.progressBar.visible()
        binding.videoView.setOnPreparedListener {
            binding.progressBar.gone()
        }

        val mediaController = MediaController(this)
        binding.videoView.setMediaController(mediaController)
        mediaController.setMediaPlayer(binding.videoView)
        binding.videoView.seekTo(viewModel.currentPosition)
        binding.videoView.start()
    }

    private fun getUrlFromBundle() = intent?.getStringExtra(BUNDLE_KEY_URL).orEmpty()

    override fun onResume() {
        super.onResume()
        binding.videoView.seekTo(viewModel.currentPosition)
        binding.videoView.start()
    }

    override fun onPause() {
        viewModel.currentPosition = binding.videoView.currentPosition
        super.onPause()

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            binding.videoView.pause();
        }
    }

    override fun onStop() {
        super.onStop()
        releasePlayer()
    }

    private fun releasePlayer(){
        binding.videoView.stopPlayback()
    }

    companion object{
        const val BUNDLE_KEY_URL = "Url"
    }
}