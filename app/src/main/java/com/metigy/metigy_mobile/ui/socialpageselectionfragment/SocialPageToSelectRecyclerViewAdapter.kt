package com.metigy.metigy_mobile.ui.socialpageselectionfragment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.metigy.metigy_mobile.core.extension.loadImage
import com.metigy.metigy_mobile.core.extension.showOrGone
import com.metigy.metigy_mobile.data.appmodels.SocialPageInfoModel
import com.metigy.metigy_mobile.databinding.ItemSocialPageToSelectBinding

class SocialPageToSelectRecyclerViewAdapter : RecyclerView.Adapter<SocialPageToSelectRecyclerViewAdapter.SocialPageViewHolder>() {
    var data: List<SocialPageInfoModel> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    val selectedPagePositions: MutableSet<Int> = mutableSetOf()

    val selectedPages: List<SocialPageInfoModel>
        get() = data.filterIndexed { index, _ ->
            selectedPagePositions.contains(index)
        }

    var atLeastOnePageIsSelected: (Boolean) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SocialPageViewHolder =
        SocialPageViewHolder(ItemSocialPageToSelectBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: SocialPageViewHolder, position: Int) {
        holder.bind(data[position], position)
    }

    override fun getItemCount(): Int = data.size

    inner class SocialPageViewHolder(private val binding: ItemSocialPageToSelectBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(socialPageInfo: SocialPageInfoModel, position: Int) {
            binding.socialPageImageView.loadImage(socialPageInfo.imageUrl)
            binding.socialPageNameTextView.text = socialPageInfo.name
            binding.socialPlatformImageView.setImageResource(socialPageInfo.socialPlatform.toSocialPlatformForView().coloredIconId)
            binding.pageTypeTextView.setText(socialPageInfo.socialPageType.toSocialPageTypeForView().typeNameResId)
            binding.selectedLayout.showOrGone(selectedPagePositions.contains(position))

            binding.root.setOnClickListener {
                if (selectedPagePositions.contains(position)) {
                    selectedPagePositions.remove(position)
                } else {
                    selectedPagePositions.add(position)
                }
                atLeastOnePageIsSelected(selectedPagePositions.isNotEmpty())
                notifyDataSetChanged()
            }
        }
    }
}