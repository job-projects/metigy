package com.metigy.metigy_mobile.ui.signup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.metigy.metigy_mobile.core.result.Event
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.data.appmodels.UserCredentialsModel
import com.metigy.metigy_mobile.data.authentication.Authentication
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignUpViewModel @Inject constructor(
    private val authentication: Authentication
) : ViewModel() {

    private val _navigateToEmailVerification = MutableLiveData<Event<Unit>>()
    val navigateToEmailVerification: LiveData<Event<Unit>>
        get() = _navigateToEmailVerification

    private val _error = MutableLiveData<Event<Result.Error>>()
    val error: LiveData<Event<Result.Error>>
        get() = _error

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    fun signUpUser(firstName: String, lastName: String, email: String, password: String) {
        viewModelScope.launch {
            _loading.value = true
            val result = authentication.signUp(firstName, lastName, email, password)
            _loading.value = false

            authentication.cacheSignUpCredentials(UserCredentialsModel(userName = email, password = password))

            when (result) {
                is Result.Success -> {
                    if (result.data.isVerified) {

                    } else {
                        _navigateToEmailVerification.value = Event(Unit)
                    }
                }
                is Result.Error -> {
                    _error.value = Event(result)
                }
            }
        }
    }
}