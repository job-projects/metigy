package com.metigy.metigy_mobile.ui.contentlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.consts.TAG_GENERAL
import com.metigy.metigy_mobile.core.extension.orZero
import com.metigy.metigy_mobile.core.extension.removeFirst
import com.metigy.metigy_mobile.core.extension.valueOrEmpty
import com.metigy.metigy_mobile.core.result.Event
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.core.utils.TimeProvider
import com.metigy.metigy_mobile.data.apiservice.ApiContentStatus
import com.metigy.metigy_mobile.data.appInfoRepository.AppInfoRepository
import com.metigy.metigy_mobile.data.appmodels.*
import com.metigy.metigy_mobile.data.brandrepository.BrandRepository
import com.metigy.metigy_mobile.data.platformpagesrepository.PlatformPagesRepository
import com.metigy.metigy_mobile.data.postrepository.ContentPersistence
import com.metigy.metigy_mobile.data.postrepository.ContentRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File
import java.time.Instant
import javax.inject.Inject

@HiltViewModel
class ContentListViewModel @Inject constructor(
    private val contentRepository: ContentRepository,
    private val platformPagesRepository: PlatformPagesRepository,
    private val contentPersistence: ContentPersistence,
    private val appInfoRepository: AppInfoRepository,
    private val brandRepository: BrandRepository,
    private val timeProvider: TimeProvider
) : ViewModel() {

    private var brandContentFullList: List<BrandContentAndState> = listOf()
        set(value) {
            field = value
            applyCurrentFiltersToContentList()
        }

    private val _brandContentFilteredList = MutableLiveData<List<BrandContentAndState>>()
    val brandContentFilteredList: LiveData<List<BrandContentAndState>>
        get() = _brandContentFilteredList

    private val _error = MutableLiveData<Event<Result.Error>>()
    val error: LiveData<Event<Result.Error>>
        get() = _error

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    private val _startLocalSharing = MutableLiveData<Event<Unit>>()
    val startLocalSharing: LiveData<Event<Unit>>
        get() = _startLocalSharing

    private val _brandGroupName = MutableLiveData<String>()
    val brandGroupName: LiveData<String>
        get() = _brandGroupName

    private val _selectedStatusFilter = MutableLiveData<ContentStatus>()
    val selectedStatusFilter: LiveData<ContentStatus>
        get() = _selectedStatusFilter

    private val _selectedDate = MutableLiveData<Instant>()
    val selectedDate: LiveData<Instant>
        get() = _selectedDate

    var contentToShareLocally: ContentToBeSharedLocally? = null

    private val selectedDateOrDefault: Instant
        get() = _selectedDate.value ?: timeProvider.now()

    var selectedBrandGroupId: Int = -1 //TODO Refactor later

    var deselectedPageIds: Set<Int> = mutableSetOf()
        set(value) {
            field = value
            applyCurrentFiltersToContentList()
        }

    init {
        setStatusAndApplyFilters(ContentStatus.SCHEDULED)
    }

    fun loadContents() {
        getBrandPosts(brandGroupId = selectedBrandGroupId, dateTime = selectedDateOrDefault)
        getBrandGroupName(brandGroupId = selectedBrandGroupId)
    }

    fun statusFilterSelectedByUser(status: ContentStatus) {
        setStatusAndApplyFilters(status)
    }

    fun setInitialDate() {
        _selectedDate.value = timeProvider.now()
    }

    fun dateFilterSelectedByUser(dateTime: Instant) {
        _selectedDate.value = dateTime
        loadContents()
    }

    private fun setDateAndLoadData(dateTime: Instant) {
        _selectedDate.value = dateTime
        getBrandPosts(brandGroupId = selectedBrandGroupId, dateTime)
    }

    private fun setStatusAndApplyFilters(status: ContentStatus) {
        _selectedStatusFilter.value = status
        applyCurrentFiltersToContentList()
    }

    //TODO Needs to be unit tested
    private fun applyCurrentFiltersToContentList() {
        _selectedStatusFilter.value?.let { status ->

            _brandContentFilteredList.value = brandContentFullList.filter { brandContentAndState ->
                status.toApiContentStatusList(status).any {
                    it == brandContentAndState.brandContent.contentStatus
                }
            }.map { brandContentAndState ->
                brandContentAndState.copy(
                    brandContent = brandContentAndState.brandContent.copy(
                        pageIds = brandContentAndState.brandContent.pageIds.filterNot { pageId ->
                            deselectedPageIds.contains(
                                pageId
                            )
                        },
                        pageInfoList = brandContentAndState.brandContent.pageInfoList.filterNot { pageInfo ->
                            deselectedPageIds.contains(pageInfo.id)
                        }
                    )
                )
            }.filterNot { brandContentAndState ->
                brandContentAndState.brandContent.pageInfoList.isEmpty()
            }
        }
    }

    private fun getBrandPosts(brandGroupId: Int, dateTime: Instant) {
        viewModelScope.launch {
            _loading.value = true
            val brandContentDeferred = viewModelScope.async { contentRepository.getBrandPosts(brandGroupId, dateTime) }
            val platformPagesDeferred = viewModelScope.async { platformPagesRepository.getPlatformPages(brandGroupId) }

            val brandContentResult = brandContentDeferred.await()
            val platformPagesResult = platformPagesDeferred.await()
            _loading.value = false

            when (brandContentResult) {
                is Result.Error -> {
                    _error.value = Event(brandContentResult)
                }
                is Result.Success -> {

                    when (platformPagesResult) {
                        is Result.Success -> {

                            val allPageInfo = platformPagesResult.data.map {
                                PageInfo(socialPlatform = it.socialPlatform, socialPageType = it.socialPageType, pageName = it.name, id = it.id)
                            }

                            brandContentFullList = brandContentResult.data.map { brandContent ->
                                brandContent.copy(pageInfoList = brandContent.pageIds.mapNotNull { pageId ->
                                    allPageInfo.find { pageInfo ->
                                        pageInfo.id == pageId
                                    }
                                })
                            }.map { brandContent ->
                                BrandContentAndState(brandContent)
                            }
                        }
                        is Result.Error -> {
                            _error.value = Event(platformPagesResult)
                        }
                    }
                }
            }

        }
    }

    private fun getBrandGroupName(brandGroupId: Int) {
        viewModelScope.launch {
            when (val result = brandRepository.listBrandGroups()) {
                is Result.Success -> {
                    _brandGroupName.value = result.data.find { brandGroup ->
                        brandGroup.id == brandGroupId
                    }?.name.orEmpty()
                }
                is Result.Error -> {
                    Timber.tag(TAG_GENERAL).e("Not able to find brand group name for id: $brandGroupId")
                }
            }
        }
    }

    fun declineContent(brandContentToChangeStatus: BrandContentModel) {
        viewModelScope.launch {
            onContentLoading(brandContentToChangeStatus, true)
            val result = contentRepository.declineContent(contentId = brandContentToChangeStatus.id, brandGroupId = brandContentToChangeStatus.brandGroupId)
            onContentLoading(brandContentToChangeStatus, false)
            when (result) {
                is Result.Success -> {
                    removeContent(brandContentToChangeStatus)
                }
                is Result.Error -> {
                    _error.value = Event(result)
                }
            }
        }
    }

    fun approveContent(brandContentToChangeStatus: BrandContentModel) {
        viewModelScope.launch {
            onContentLoading(brandContentToChangeStatus, true)
            val result = contentRepository.approveContent(contentId = brandContentToChangeStatus.id, brandGroupId = brandContentToChangeStatus.brandGroupId)

            onContentLoading(brandContentToChangeStatus, false)

            when (result) {
                is Result.Success -> {
                    _brandContentFilteredList.value = _brandContentFilteredList.valueOrEmpty().map { brandContentAndState ->
                        if (brandContentAndState.brandContent.id == brandContentToChangeStatus.id) brandContentAndState.copy(
                            brandContent = brandContentToChangeStatus.copy(contentStatus = ApiContentStatus.APPROVED),
                            loading = false
                        ) else brandContentAndState
                    }
                }
                is Result.Error -> {
                    _error.value = Event(result)
                }
            }
        }
    }

    fun postContent(brandContentToChangeStatus: BrandContentModel) {
        viewModelScope.launch {

            val appAndShareInfoList = brandContentToChangeStatus.pageInfoList.filter { pageInfo ->
                pageInfo.socialPageType == SocialPageType.PERSONAL
            }.distinctBy { pageInfo ->
                pageInfo.socialPlatform
            }.mapNotNull { pageInfo ->
                SmAppAndInfo.fromSocialPlatform(pageInfo.socialPlatform)
            }.sortedBy { appAndNativeShareInfo ->
                appAndNativeShareInfo.order
            }

            if (appAndShareInfoList.isNotEmpty()) {
                val url = brandContentToChangeStatus.imageUrl()

                val file = if (url != null) contentPersistence.saveContentImage(url, brandContentToChangeStatus.id) else null

                contentToShareLocally = ContentToBeSharedLocally(
                    imageFile = file,
                    text = brandContentToChangeStatus.message.orEmpty(),
                    smAppList = appAndShareInfoList,
                    brandContent = brandContentToChangeStatus
                )
                _startLocalSharing.value = Event(Unit)
            }

            if (brandContentToChangeStatus.pageInfoList.any {
                    it.socialPageType == SocialPageType.BUSINESS
                }) {
                onContentLoading(brandContentToChangeStatus, true)

                when (val result = contentRepository.postContent(brandGroupId = brandContentToChangeStatus.brandGroupId, contentId = brandContentToChangeStatus.id)) {
                    is Result.Success -> {
                        removeContent(brandContentToChangeStatus)
                    }
                    is Result.Error -> {
                        onContentLoading(brandContentToChangeStatus, false)
                        _error.value = Event(result)
                    }
                }
            }
        }
    }

    fun isAppInstalled(app: SmAppAndInfo): Boolean = appInfoRepository.isAppInstalled(app)

    private fun onContentLoading(brandContent: BrandContentModel, loading: Boolean) {
        _brandContentFilteredList.value = _brandContentFilteredList.valueOrEmpty().map { brandContentAndState ->
            if (brandContentAndState.brandContent.id == brandContent.id) brandContentAndState.copy(loading = loading) else brandContentAndState
        }
    }

    private fun removeContent(brandContent: BrandContentModel) {
        _brandContentFilteredList.value = _brandContentFilteredList.valueOrEmpty().filterNot { brandContentAndState ->
            brandContentAndState.brandContent.id == brandContent.id
        }
    }

    fun removeFirstSmApp() {
        contentToShareLocally?.let {
            contentToShareLocally = it.copy(smAppList = it.smAppList.removeFirst())
        }
    }

    fun onContentSharedLocally() {
        viewModelScope.launch {
            contentToShareLocally?.let {
                val brandContent = it.brandContent
                contentRepository.changeStatusForLocalSharing(
                    brandGroupId = brandContent.brandGroupId,
                    contentId = brandContent.id,
                    pageId = brandContent.pageIds.firstOrNull().orZero()
                )
            }
        }
    }
}

data class ContentToBeSharedLocally(
    val imageFile: File?,
    val text: String,
    val smAppList: List<SmAppAndInfo>,
    val statusUpdated: Boolean = false,
    val brandContent: BrandContentModel
)

//SMApp = Social Media App
enum class SmAppAndInfo(
    val order: Int,
    val socialPlatform: SocialPlatform,
    val appNameResId: Int,
    val packageName: String,
    val websiteUrl: String,
    val titleResId: Int?,
    val descriptionResId: Int?
) {
    FACEBOOK(
        order = 1,
        socialPlatform = SocialPlatform.FACEBOOK,
        appNameResId = R.string.facebook,
        packageName = "com.facebook.katana",
        websiteUrl = "https://facebook.com",
        titleResId = null,
        descriptionResId = null
    ),
    Instagram(
        order = 2,
        socialPlatform = SocialPlatform.INSTAGRAM,
        appNameResId = R.string.instagram,
        packageName = "com.instagram.android",
        websiteUrl = "https://instagram.com",
        titleResId = R.string.instagram_share_dialog_title,
        descriptionResId = R.string.instagram_share_dialog_description
    );

    //Be careful!!!
    //If you want to add more apps here, you have to add their package name in the manifest file too.

    companion object {
        fun fromSocialPlatform(socialPlatform: SocialPlatform) = values().associateBy(SmAppAndInfo::socialPlatform)[socialPlatform]
    }
}