package com.metigy.metigy_mobile.ui.socialplatformselection

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.consts.TAG_AUTHENTICATION
import com.metigy.metigy_mobile.core.consts.TAG_DEBUG
import com.metigy.metigy_mobile.core.extension.*
import com.metigy.metigy_mobile.core.result.EventObserver
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.core.ui.StackedButtonDialogFragment
import com.metigy.metigy_mobile.data.appmodels.SocialPlatform
import com.metigy.metigy_mobile.databinding.FragmentSocialPlatformSelectionBinding
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber


@AndroidEntryPoint
class SocialPlatformSelectionFragment : Fragment(), StackedButtonDialogFragment.OnButtonClickListener {

    private lateinit var binding: FragmentSocialPlatformSelectionBinding

    private val viewModel: SocialPlatformSelectionViewModel by viewModels()

    private val socialPlatformListAdapter: SocialPlatformRecyclerViewAdapter by lazy {
        SocialPlatformRecyclerViewAdapter(requireContext())
    }

    private val facebookCallbackManager: CallbackManager by lazy {
        CallbackManager.Factory.create()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        LoginManager.getInstance().registerCallback(
            facebookCallbackManager,
            object : FacebookCallback<LoginResult?> {
                override fun onSuccess(loginResult: LoginResult?) {
                    loginResult?.let {
                        Timber.tag(TAG_AUTHENTICATION).d("Facebook or Insta auth:\t${loginResult.accessToken.token}")
                        viewModel.sendSocialLoginTokenToServer(loginResult.accessToken.token, viewModel.lastPlatformClicked?.socialPlatform ?: SocialPlatform.FACEBOOK)
                    } ?: run {
                        showErrorToast(Result.Error(messageId = R.string.unknown_error))
                    }
                }

                override fun onCancel() {
                    Timber.tag(TAG_DEBUG).d("Authorization canceled")
                }

                override fun onError(exception: FacebookException) {
                    showErrorToast(Result.Error(messageId = R.string.authorization_failed))
                }
            })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSocialPlatformSelectionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUi()
        startObserving()
    }

    private fun setupUi() {
        binding.helpButton.setOnClickListener {
            StackedButtonDialogFragment.launchHelpDialog(this)
        }

        binding.socialProfileRecyclerView.verticalLayoutManager()
        binding.socialProfileRecyclerView.adapter = socialPlatformListAdapter

        socialPlatformListAdapter.data = viewModel.platformList
        socialPlatformListAdapter.onPlatformClicked = { socialPlatformToConnect ->
            val socialPlatformForView = socialPlatformToConnect.socialPlatform.toSocialPlatformForView()
            StackedButtonDialogFragment.createAndShow(
                fragmentManager = childFragmentManager,
                title = getString(R.string.connecting_your_social_profile_page, getString(socialPlatformForView.platformNameResId)),
                message = getString(socialPlatformToConnect.connectConfirmMessageResId),
                firstButton = getString(R.string.authorise_with_social_platform, getString(socialPlatformForView.platformNameResId)),
                secondButton = getString(R.string.learn_more),
                thirdButton = getString(R.string.cancel),
                tag = socialPlatformToConnect.name
            )
        }

        binding.connectLaterButton.setOnClickListener {
            launchHomeScreen()
        }
    }

    private fun startObserving() {
        viewModel.navigateToPageSelection.observe(viewLifecycleOwner, EventObserver { platformAndUuid ->
            navigate(SocialPlatformSelectionFragmentDirections.toSocialPageSelectionFragment(platformAndUuid.uuid, socialPlatform = platformAndUuid.socialPlatform))
        })

        viewModel.loading.observe(viewLifecycleOwner) { loading ->
            binding.progressBar.showOrInvisible(loading)
            binding.socialProfileRecyclerView.showOrInvisible(loading.not())
        }

        viewModel.error.observe(viewLifecycleOwner, EventObserver { error ->
            showErrorToast(error)
        })
    }

    private fun callFacebookLogin() {
        LoginManager.getInstance().logIn(
            this, listOf(
                "pages_manage_ads",
                "pages_manage_metadata",
                "pages_read_engagement",
                "pages_read_user_content",
                "pages_manage_posts",
                "pages_manage_engagement",
                "read_insights",
                "business_management",
                "ads_read",
                "ads_management"
            )
        )
    }

    private fun callInstagramLogin() {
        LoginManager.getInstance().logIn(
            this,
            listOf(
                "instagram_basic",
                "pages_manage_ads",
                "pages_manage_metadata",
                "pages_read_engagement",
                "pages_read_user_content",
                "instagram_manage_insights",
                "instagram_content_publish"
            )
        )
    }

    override fun onFirstButtonClicked(tag: String?) {
        when (tag) {
            DIALOG_TAG_HELP -> {
                openUrl(viewModel.helpUrl)
            }
            else -> {
                val platformToConnect = SocialPlatformToConnect.valueOf(tag.orEmpty())
                if (platformToConnect == SocialPlatformToConnect.FACEBOOK) {
                    callFacebookLogin()
                } else if (platformToConnect == SocialPlatformToConnect.INSTAGRAM) {
                    callInstagramLogin()
                }

                viewModel.lastPlatformClicked = platformToConnect
            }
        }
    }

    override fun onSecondButtonClicked(tag: String?) {
        when (tag) {
            DIALOG_TAG_HELP -> {
                composeEmail(listOf(viewModel.supportEmail))
            }
            else -> {
                //Social platform authorization
                openUrl(viewModel.learnMoreUrl)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        facebookCallbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    companion object {
        const val DIALOG_TAG_HELP = "Help"
    }
}