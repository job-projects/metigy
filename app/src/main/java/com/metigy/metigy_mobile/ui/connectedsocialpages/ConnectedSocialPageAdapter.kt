package com.metigy.metigy_mobile.ui.connectedsocialpages

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.extension.invisible
import com.metigy.metigy_mobile.core.extension.visible
import com.metigy.metigy_mobile.core.utils.StringResProvider
import com.metigy.metigy_mobile.data.appmodels.PlatformPageModel
import com.metigy.metigy_mobile.databinding.ItemConnectedSocialPageBinding
import com.metigy.metigy_mobile.ui.socialplatformselection.SocialPlatformRecyclerViewAdapter
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.android.EntryPointAccessors
import dagger.hilt.components.SingletonComponent

class ConnectedSocialPageAdapter(private val context: Context) : RecyclerView.Adapter<ConnectedSocialPageAdapter.ConnectedPageViewHolder>() {
    private val stringProvider: StringResProvider by lazy {
        val adapterEntryPoint = EntryPointAccessors.fromApplication(context, SocialPlatformRecyclerViewAdapter.AdapterEntryPoint::class.java)
        adapterEntryPoint.stringResProvider()
    }

    var data: List<PlatformPageModel> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var removeState: Boolean = false
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var onRemoveClicked: (PlatformPageModel) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConnectedPageViewHolder =
        ConnectedPageViewHolder(ItemConnectedSocialPageBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ConnectedPageViewHolder, position: Int) {
        holder.bind(platformPage = data[position])
    }

    override fun getItemCount(): Int = data.size

    inner class ConnectedPageViewHolder(private val binding: ItemConnectedSocialPageBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(platformPage: PlatformPageModel) {
            binding.socialPageNameTextView.text = platformPage.name
            binding.socialPlatformImageView.setImageResource(platformPage.socialPlatform.toSocialPlatformForView().coloredIconId)
            binding.accountTypesTextView.text =
                stringProvider.getString(R.string.social_page_type_space_page, stringProvider.getString(platformPage.socialPageType.toSocialPageTypeForView().typeNameResId))

            binding.tickOrRemoveImageView.setImageResource(
                if (removeState) R.drawable.ic_cross else R.drawable.ic_tick_circle
            )

            binding.removeProgressBar.invisible()
            binding.tickOrRemoveImageView.visible()

            binding.tickOrRemoveImageView.setOnClickListener {
                if (removeState) {
                    onRemoveClicked(platformPage)
                    binding.tickOrRemoveImageView.invisible()
                    binding.removeProgressBar.visible()
                }
            }
        }
    }

    @EntryPoint
    @InstallIn(SingletonComponent::class)
    interface AdapterEntryPoint {
        fun stringResProvider(): StringResProvider
    }

}