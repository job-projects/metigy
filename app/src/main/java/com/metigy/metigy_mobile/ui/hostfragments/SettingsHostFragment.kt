package com.metigy.metigy_mobile.ui.hostfragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.metigy.metigy_mobile.databinding.FragmentSettingsHostBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SettingsHostFragment : Fragment() {

    private lateinit var binding: FragmentSettingsHostBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSettingsHostBinding.inflate(inflater, container, false)
        return binding.root
    }
}