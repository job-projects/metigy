package com.metigy.metigy_mobile.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.metigy.metigy_mobile.core.consts.TAG_AUTHENTICATION
import com.metigy.metigy_mobile.core.consts.TAG_DEBUG
import com.metigy.metigy_mobile.core.result.Event
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.core.utils.TimeProvider
import com.metigy.metigy_mobile.data.appmodels.BrandGroupModel
import com.metigy.metigy_mobile.data.appmodels.PlatformPageModel
import com.metigy.metigy_mobile.data.appmodels.SocialPlatform
import com.metigy.metigy_mobile.data.authentication.Authentication
import com.metigy.metigy_mobile.data.brandrepository.BrandRepository
import com.metigy.metigy_mobile.data.platformpagesrepository.PlatformPagesRepository
import com.metigy.metigy_mobile.ui.main.dropdown.DropDownPreviewLogoItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import timber.log.Timber
import java.time.temporal.ChronoUnit
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val authentication: Authentication,
    private val brandRepository: BrandRepository,
    private val platformPagesRepository: PlatformPagesRepository,
    private val timeProvider: TimeProvider
) : ViewModel() {

    private val _error = MutableLiveData<Event<Result.Error>>()
    val error: LiveData<Event<Result.Error>>
        get() = _error

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    private val _brandGroupListAndPages = MutableLiveData<Map<BrandGroupModel, List<PlatformPageModel>>>()
    val brandGroupListAndPages: LiveData<Map<BrandGroupModel, List<PlatformPageModel>>>
        get() = _brandGroupListAndPages

    private val _selectedBrandGroupAndPages = MutableLiveData<Pair<BrandGroupModel, List<PlatformPageModel>>>()
    val selectedBrandGroupAndPages: LiveData<Pair<BrandGroupModel, List<PlatformPageModel>>>
        get() = _selectedBrandGroupAndPages

    val deselectedPageSet: MutableSet<Int> = mutableSetOf()

    private val _deselectedPageSetObservable = MutableLiveData<Set<Int>>()
    val deselectedPageSetObservable: LiveData<Set<Int>>
        get() = _deselectedPageSetObservable

    init {
        loadBrandGroups()
    }

    fun reAuthenticateIfNeeded() {
        if (authentication.isUserLoggedInAtLaunch()) {
            val lastAuthDate = authentication.lastAuthenticationDate()
            if (lastAuthDate == null || timeProvider.now().minus(2, ChronoUnit.HOURS) >= lastAuthDate) {
                Timber.tag(TAG_AUTHENTICATION).d("Re-authentication is needed")
                viewModelScope.launch {
                    when (val result = authentication.reAuthenticate()) {
                        is Result.Success -> {
                            authentication.saveToken(token = result.data)
                            Timber.tag(TAG_AUTHENTICATION).d("Re-authentication done successfully")
                        }
                        is Result.Error -> {
                            Timber.tag(TAG_AUTHENTICATION).e(result.exception, "Re-authentication failed")
                        }
                    }
                }
            }

        }
    }

    fun getAllBrandGroups() = _brandGroupListAndPages.value?.keys?.toList() ?: listOf()

    fun brandGroupSelected(id: Int) {
        _brandGroupListAndPages.value?.keys?.find { brandGroup ->
            brandGroup.id == id
        }?.let { selectedBrandGroup ->
            val pages = _brandGroupListAndPages.value?.get(selectedBrandGroup) ?: listOf()
            _selectedBrandGroupAndPages.value = selectedBrandGroup to pages
        }
    }

    fun selectedPagesChanged() {
        _deselectedPageSetObservable.value = deselectedPageSet
    }

    private fun loadBrandGroups() {
        viewModelScope.launch {
            _loading.value = true
            val result = brandRepository.listBrandGroups()
            _loading.value = false
            when (result) {
                is Result.Success -> {
                    result.data.let { brandGroupList ->
                        loadPagesForAllBrandGroups(brandGroupList)
                    }
                }
                is Result.Error -> {
                    Timber.tag(TAG_DEBUG).d(result.toString())
                    _error.value = Event(result)
                }
            }
        }
    }

    //TODO Needs to be unit tested specially the "filterIsInstance" part
    private suspend fun loadPagesForAllBrandGroups(brandGroupList: List<BrandGroupModel>) {
        _brandGroupListAndPages.value = brandGroupList.map { brandGroup ->
            brandGroup to viewModelScope.async { platformPagesRepository.getPlatformPages(brandGroupId = brandGroup.id) }
        }.map { pair ->
            pair.first to pair.second.await()
        }.filterIsInstance<Pair<BrandGroupModel, Result.Success<List<PlatformPageModel>>>>()
            .map { pair ->
                pair.first to pair.second.data
            }.toMap().also {
                it.entries.firstOrNull()?.let { brandGroupAndPages ->
                    _selectedBrandGroupAndPages.value = brandGroupAndPages.toPair()
                }
            }
    }

    fun pageListToSocialPlatformList(pageList: List<PlatformPageModel>) =
        pageList.map {
            it.socialPlatform
        }.distinct()

    fun buildBrandGroupDropDownPreviewList(list: List<SocialPlatform>, brandGroup: BrandGroupModel) =
        listOf(
            DropDownPreviewLogoItem.BrandGroup(brandGroup.url.orEmpty()),
            *list.map {
                DropDownPreviewLogoItem.SocialPlatform(it)
            }.toTypedArray(),
            DropDownPreviewLogoItem.Plus
        )

}