package com.metigy.metigy_mobile.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.metigy.metigy_mobile.core.consts.TAG_AUTHENTICATION
import com.metigy.metigy_mobile.core.result.Event
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.data.appcache.AppCache
import com.metigy.metigy_mobile.data.appmodels.UserCredentialsModel
import com.metigy.metigy_mobile.data.authentication.Authentication
import com.metigy.metigy_mobile.data.authentication.LocalUserInfoRepository
import com.metigy.metigy_mobile.data.authentication.RemoteUserInfoRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val authentication: Authentication,
    private val remoteUserInfoRepository: RemoteUserInfoRepository,
    private val localUserInfoRepository: LocalUserInfoRepository,
    private val appCache: AppCache
) : ViewModel() {

    private val _navigateToHomeScreen = MutableLiveData<Event<Unit>>()
    val navigateToHomeScreen: LiveData<Event<Unit>>
        get() = _navigateToHomeScreen

    private val _navigateToEmailVerificationScreen = MutableLiveData<Event<Unit>>()
    val navigateToEmailVerificationScreen: LiveData<Event<Unit>>
        get() = _navigateToEmailVerificationScreen

    private val _loginErrorMessage = MutableLiveData<Event<Result.Error>>()
    val loginErrorMessage: LiveData<Event<Result.Error>>
        get() = _loginErrorMessage

    private val _loginLoading = MutableLiveData<Boolean>().apply { value = false }
    val loginLoading: LiveData<Boolean>
        get() = _loginLoading

    var savedCredentials: UserCredentialsModel? = null

    fun login(email: String, password: String, saveCredentials: Boolean) {
        viewModelScope.launch {
            saveCredentialsIfNeeded(email = email, password = password, saveCredentials = saveCredentials)

            _loginLoading.value = true
            val authResult = authentication.login(email, password)
            _loginLoading.value = false

            when (authResult) {
                is Result.Success -> getUserInfo(email, password, authResult.data)
                is Result.Error -> _loginErrorMessage.value = Event(authResult)
            }
        }
    }

    fun getUserInfo(email: String, password: String, token: String) {
        viewModelScope.launch {
            _loginLoading.value = true
            val userInfoResult = remoteUserInfoRepository.getUserInfo(token = token)
            _loginLoading.value = false
            when (userInfoResult) {
                is Result.Success -> {
                    if (userInfoResult.data.isVerified) {
                        authentication.setIsPreviouslyLoggedIn()
                        authentication.saveToken(token)
                        localUserInfoRepository.saveUserInfo(userInfoResult.data)
                        _navigateToHomeScreen.value = Event(Unit)
                    } else {
                        appCache.saveSignUpCredentials(UserCredentialsModel(userName = email, password = password))
                        _navigateToEmailVerificationScreen.value = Event(Unit)
                    }
                }
                is Result.Error -> {
                    _loginErrorMessage.value = Event(userInfoResult)
                    Timber.tag(TAG_AUTHENTICATION).e(userInfoResult.message ?: "User not found")
                }
            }
        }
    }

    fun saveCredentialsIfNeeded(email: String, password: String, saveCredentials: Boolean) {
        if (saveCredentials) {
            authentication.saveLoginCredentials(UserCredentialsModel(userName = email, password = password))
        } else {
            authentication.clearLoginCredentials()
        }
    }
}