package com.metigy.metigy_mobile.ui.video

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class VideoPlayerViewModel @Inject constructor(): ViewModel() {
    var currentPosition : Int = 0
}