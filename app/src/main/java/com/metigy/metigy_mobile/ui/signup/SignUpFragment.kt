package com.metigy.metigy_mobile.ui.signup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.extension.*
import com.metigy.metigy_mobile.core.result.EventObserver
import com.metigy.metigy_mobile.databinding.FragmentSignUpBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignUpFragment : Fragment() {

    private lateinit var binding: FragmentSignUpBinding

    private val viewModel: SignUpViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSignUpBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUi()
        startObserving()
    }

    private fun setupUi() {
        binding.signUpButton.setOnClickListener {
            viewModel.signUpUser(
                firstName = binding.firstNameEditText.textValue(),
                lastName = binding.lastNameEditText.textValue(),
                email = binding.emailAddressEditText.textValue(),
                password = binding.passwordEditText.textValue()
            )
        }

        binding.logoTextView.setStartDrawableStart(drawableId = R.drawable.logo_simple, sizeInDP = 32)

        binding.signInButton.setOnClickListener {
            navigateUp()
        }

        binding.cancelButton.setOnClickListener {
            navigateUp()
        }
    }

    private fun startObserving() {
        viewModel.navigateToEmailVerification.observe(viewLifecycleOwner, EventObserver {
            navigate(SignUpFragmentDirections.toEmailVerificationFragment())
        })

        viewModel.error.observe(viewLifecycleOwner, EventObserver { error ->
            showErrorToast(error)
        })

        viewModel.loading.observe(viewLifecycleOwner) { loading ->
            binding.progressBar.showOrInvisible(loading)
            binding.signUpButton.showOrInvisible(loading.not())
        }
    }
}