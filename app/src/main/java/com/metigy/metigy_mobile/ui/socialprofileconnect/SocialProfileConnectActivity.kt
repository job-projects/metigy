package com.metigy.metigy_mobile.ui.socialprofileconnect

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.metigy.metigy_mobile.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SocialProfileConnectActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_social_profile_connect)
    }
}