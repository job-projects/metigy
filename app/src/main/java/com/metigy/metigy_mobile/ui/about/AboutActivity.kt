package com.metigy.metigy_mobile.ui.about

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.databinding.ActivityAboutBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AboutActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAboutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAboutBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onBackPressed() {
        val rootFragment = findNavController(R.id.nav_host_fragment).navigateUp().not()
        if (rootFragment) {
            super.onBackPressed()
        }
    }

}