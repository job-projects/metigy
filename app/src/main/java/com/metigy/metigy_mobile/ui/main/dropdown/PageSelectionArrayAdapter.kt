package com.metigy.metigy_mobile.ui.main.dropdown

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.metigy.metigy_mobile.core.extension.horizontalLayoutManager
import com.metigy.metigy_mobile.core.extension.loadImage
import com.metigy.metigy_mobile.core.extension.showOrInvisible
import com.metigy.metigy_mobile.core.extension.toggleElement
import com.metigy.metigy_mobile.data.appmodels.BrandGroupModel
import com.metigy.metigy_mobile.data.appmodels.PlatformPageModel
import com.metigy.metigy_mobile.databinding.ItemDropDownBrandGroupBinding
import com.metigy.metigy_mobile.databinding.ItemDropDownClickableBinding
import com.metigy.metigy_mobile.databinding.ItemDropDownConnectNewSocialProfileBinding
import com.metigy.metigy_mobile.databinding.ItemDropDownSocialPageBinding

class PageSelectionArrayAdapter(
    context: Context,
    data: List<DropDownBrandGroupAndPage>,
    private val deselectedPageSet: MutableSet<Int>,
    private val previewList: List<DropDownPreviewLogoItem>
) : ArrayAdapter<DropDownBrandGroupAndPage>(context, 0, data) {

    var onBrandGroupSwitchClicked: () -> Unit = {}

    var onPageClicked: () -> Unit = {}

    var onPreviewClicked: () -> Unit = {}

    val layoutInflater: LayoutInflater
        get() = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val binding = ItemDropDownClickableBinding.inflate(layoutInflater, parent, false)
        binding.recyclerView.horizontalLayoutManager()
        binding.recyclerView.addItemDecoration(DropDownPreviewRecyclerViewAdapter.ItemDecorator())
        binding.recyclerView.adapter = DropDownPreviewRecyclerViewAdapter().apply {
            onItemClicked = {
                onPreviewClicked()
            }
            data = previewList
        }

        binding.root.setOnClickListener {
            onPreviewClicked()
        }

        return convertView ?: binding.root
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {

        return (getItem(position) ?: error("Illegal State in ArrayAdapter. getItem(position: $position) returns null")).let { item ->
            when (item) {
                is DropDownBrandGroupAndPage.BrandGroup -> {
                    val binding = ItemDropDownBrandGroupBinding.inflate(layoutInflater, parent, false)
                    setItemForBrandGroup(binding, item)
                    binding.root
                }
                is DropDownBrandGroupAndPage.PlatformPage -> {
                    val binding = ItemDropDownSocialPageBinding.inflate(layoutInflater, parent, false)
                    setItemForSocialPage(binding, item)
                    binding.root
                }
                DropDownBrandGroupAndPage.ConnectNewProfile -> {
                    val binding = ItemDropDownConnectNewSocialProfileBinding.inflate(layoutInflater, parent, false)
                    binding.root
                }
            }
        }
    }

    override fun isEnabled(position: Int) = false

    private fun setItemForBrandGroup(binding: ItemDropDownBrandGroupBinding, dropDownItem: DropDownBrandGroupAndPage.BrandGroup) {
        binding.brandOrPageNameTextView.text = dropDownItem.name
        binding.brandGroupOrPageLogoImageView.loadImage(dropDownItem.logoUrl)

        binding.root.setOnClickListener {
            onBrandGroupSwitchClicked()
        }
    }


    private fun setItemForSocialPage(binding: ItemDropDownSocialPageBinding, dropDownItem: DropDownBrandGroupAndPage.PlatformPage) {
        binding.pageNameTextView.text = dropDownItem.name
        binding.pageLogoImageView.setImageResource(dropDownItem.logoResId)

        binding.isSelectedImageView.showOrInvisible(deselectedPageSet.contains(dropDownItem.id).not())

        binding.root.setOnClickListener {
            val wasSelected = deselectedPageSet.contains(dropDownItem.id).not()
            deselectedPageSet.toggleElement(dropDownItem.id)
            binding.isSelectedImageView.showOrInvisible(wasSelected.not())
            binding.pageLogoImageView.alpha = if (wasSelected) 0.5f else 1f
            binding.pageNameTextView.alpha = if (wasSelected) 0.5f else 1f
            onPageClicked()
        }
    }


    companion object {
        private fun createData(pair: Pair<BrandGroupModel, List<PlatformPageModel>>) =
            arrayListOf<DropDownBrandGroupAndPage>(
                DropDownBrandGroupAndPage.BrandGroup(id = pair.first.id, name = pair.first.name, logoUrl = pair.first.url.orEmpty())
            ).apply {
                addAll(
                    pair.second.map {
                        DropDownBrandGroupAndPage.PlatformPage(id = it.id, name = it.name.orEmpty(), logoResId = it.socialPlatform.toSocialPlatformForView().coloredIconId)
                    }
                )
                add(DropDownBrandGroupAndPage.ConnectNewProfile)
            }.toList()

        fun newInstance(context: Context, pair: Pair<BrandGroupModel, List<PlatformPageModel>>, deselectedPageSet: MutableSet<Int>, previewList: List<DropDownPreviewLogoItem>) =
            PageSelectionArrayAdapter(context, createData(pair), deselectedPageSet, previewList)
    }
}