package com.metigy.metigy_mobile.ui.intro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.metigy.metigy_mobile.core.extension.gone
import com.metigy.metigy_mobile.core.extension.navigate
import com.metigy.metigy_mobile.core.extension.visible
import com.metigy.metigy_mobile.data.appmodels.IntroPagerContent
import com.metigy.metigy_mobile.databinding.FragmentIntroBinding
import com.metigy.metigy_mobile.databinding.FragmentPagerIntroBinding
import com.metigy.metigy_mobile.ui.login.LoginFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class IntroFragment : Fragment(), LoginScreenActions {

    private lateinit var binding: FragmentIntroBinding

    private val viewModel: IntroViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentIntroBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUi()
        startObserving()
    }

    private fun setupUi() {
        binding.introViewPager.adapter = IntroViewPagerAdapter(this, viewModel.contentList)

        binding.introViewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                viewModel.setCurrentTabPosition(position)
            }
        })

        TabLayoutMediator(binding.indicator, binding.introViewPager) { _, _ -> }.attach()

        if (viewModel.getIfPreviouslyLoggedIn())
            binding.introViewPager.currentItem = viewModel.loginScreenPosition
    }

    private fun startObserving() {
        viewModel.currentTabPosition.observe(viewLifecycleOwner) { position ->
            if (position != viewModel.loginScreenPosition) {
                binding.indicator.visible()
            } else {
                binding.indicator.gone()
            }
        }
    }

    override fun onBackClicked() {
        binding.introViewPager.currentItem = viewModel.lastIntroScreenPosition
    }
}


class IntroViewPagerAdapter(fragment: Fragment, private val contentList: List<IntroPagerContent>) : FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int = contentList.size + 1

    override fun createFragment(position: Int): Fragment {
        return if (position != contentList.size) IntroPagerFragment().apply {
            arguments = Bundle().apply {
                putParcelable(IntroPagerFragment.BUNDLE_CONTENT_KEY, contentList[position])
            }
        } else {
            LoginFragment()
        }
    }
}

class IntroPagerFragment : Fragment() {
    private lateinit var binding: FragmentPagerIntroBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPagerIntroBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.takeIf { bundle ->
            bundle.containsKey(BUNDLE_CONTENT_KEY)
        }?.apply {
            getParcelable<IntroPagerContent>(BUNDLE_CONTENT_KEY)?.let { content ->
                binding.introHeadingText.setText(content.headingResId)
                binding.introBodyText.setText(content.bodyResId)
                binding.introImageView.setImageResource(content.imageResId)
            }
        }
    }

    companion object {
        const val BUNDLE_CONTENT_KEY = "Content"
    }
}

interface LoginScreenActions {
    fun onBackClicked()
}