package com.metigy.metigy_mobile.ui.emailverification

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.result.Event
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.data.appcache.AppCache
import com.metigy.metigy_mobile.data.authentication.Authentication
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ResendEmailViewModel @Inject constructor(
    private val authentication: Authentication,
    private val appCache: AppCache
) : ViewModel() {

    private val _error = MutableLiveData<Event<Result.Error>>()
    val error: LiveData<Event<Result.Error>>
        get() = _error

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    private val _displayEmailSentDialog = MutableLiveData<Boolean>()
    val displayEmailSentDialog: LiveData<Boolean>
        get() = _displayEmailSentDialog

    fun resendEmail() {
        viewModelScope.launch {
            appCache.getSignUpCredentials()?.let { userCredentials ->
                _loading.value = true
                when (val loginResult = authentication.login(email = userCredentials.userName, password = userCredentials.password)) {
                    is Result.Success -> {
                        when (val resendEmailResult = authentication.resendEmail(loginResult.data, email = userCredentials.userName)) {
                            is Result.Success -> {
                                _displayEmailSentDialog.value = true
                            }
                            is Result.Error -> {
                                _error.value = Event(resendEmailResult)
                            }
                        }
                    }
                    is Result.Error -> {
                        _error.value = Event(loginResult)
                    }
                }
                _loading.value = false

            } ?: run {
                _error.value = Event(Result.Error(messageId = R.string.unknown_error))
            }
        }
    }

    fun hideEmailSentDialog() {
        _displayEmailSentDialog.value = false
    }
}