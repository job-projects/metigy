package com.metigy.metigy_mobile.ui.about

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.utils.StringResProvider
import com.metigy.metigy_mobile.data.appInfoRepository.AppInfoRepository
import com.metigy.metigy_mobile.data.appmodels.AppVersionModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class AboutViewModel @Inject constructor(
    appInfoRepository: AppInfoRepository,
    private val stringResProvider: StringResProvider
) : ViewModel() {

    private val _appVersion = MutableLiveData<AppVersionModel>()
    val appVersion: LiveData<AppVersionModel>
        get() = _appVersion

    init {
        _appVersion.value = appInfoRepository.appVersion()
    }

    //Fragment logics
    fun prettifyVersion(versionModel: AppVersionModel) = stringResProvider.getString(R.string.version_colon) + versionModel.versionName + "(" + versionModel.versionCode + ")"
}