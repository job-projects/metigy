package com.metigy.metigy_mobile.ui.main.dropdown

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.extension.*
import com.metigy.metigy_mobile.data.appmodels.BrandGroupModel
import com.metigy.metigy_mobile.databinding.ItemDropDownBrandGroupBinding
import com.metigy.metigy_mobile.databinding.ItemDropDownConnectNewSocialProfileBinding

class BrandGroupSelectionArrayAdapter(context: Context, data: List<DropDownBrandGroupAndPage>) :
    ArrayAdapter<DropDownBrandGroupAndPage>(context, 0, data) {

    var onBrandGroupClicked: (Int) -> Unit = {}

    val layoutInflater: LayoutInflater
        get() = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return convertView ?: layoutInflater.inflate(R.layout.item_drop_down_clickable, parent, false)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {

        return (getItem(position) ?: error("Illegal State in ArrayAdapter. getItem(position: $position) returns null")).let { item ->
            when (item) {
                is DropDownBrandGroupAndPage.BrandGroup -> {
                    val binding = ItemDropDownBrandGroupBinding.inflate(layoutInflater, parent, false)
                    setItemForBrandGroup(binding, item, position == 0)
                    binding.root
                }
                DropDownBrandGroupAndPage.ConnectNewProfile -> {
                    val binding = ItemDropDownConnectNewSocialProfileBinding.inflate(layoutInflater, parent, false)
                    binding.root
                }
                //Must never happen
                else -> {
                    val binding = ItemDropDownConnectNewSocialProfileBinding.inflate(layoutInflater, parent, false)
                    binding.root
                }
            }
        }
    }

    override fun isEnabled(position: Int) = false

    private fun setItemForBrandGroup(binding: ItemDropDownBrandGroupBinding, dropDownItem: DropDownBrandGroupAndPage.BrandGroup, isSelected: Boolean) {
        binding.brandOrPageNameTextView.text = dropDownItem.name
        binding.brandGroupOrPageLogoImageView.loadImage(dropDownItem.logoUrl)
        if (isSelected) {
            binding.root.setBackgroundDrawable(R.drawable.shape_background_brand_group_drop_down_selected)
            binding.brandOrPageNameTextView.setTextColorResId(R.color.white)
            binding.switchBrandImageView.setImageResource(R.drawable.ic_switch_brand_active)
            binding.switchBrandImageView.visible()
        } else {
            binding.switchBrandImageView.invisible()
        }
        binding.root.setOnClickListener {
            onBrandGroupClicked(dropDownItem.id)
        }
    }

    companion object {
        private fun createData(brandGroupList: List<BrandGroupModel>, selectedBrandGroupId: Int) =
            listOf(
                *(brandGroupList.map {
                    DropDownBrandGroupAndPage.BrandGroup(id = it.id, name = it.name, logoUrl = it.url.orEmpty())
                }).toTypedArray(),
                DropDownBrandGroupAndPage.ConnectNewProfile
            ).let { list ->
                list.find {
                    it is DropDownBrandGroupAndPage.BrandGroup && it.id == selectedBrandGroupId
                }?.let {
                    list.moveToFirst(it)
                } ?: list
            }

        fun newInstance(context: Context, brandGroupList: List<BrandGroupModel>, selectedBrandGroupId: Int) =
            BrandGroupSelectionArrayAdapter(context, createData(brandGroupList, selectedBrandGroupId))
    }
}