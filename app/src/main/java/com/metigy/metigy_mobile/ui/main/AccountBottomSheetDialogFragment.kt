package com.metigy.metigy_mobile.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.databinding.BottomSheetAccountBinding

class AccountBottomSheetDialogFragment : BottomSheetDialogFragment() {
    private lateinit var binding: BottomSheetAccountBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppBottomSheetDialogTheme)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = BottomSheetAccountBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val actions = (activity as AccountBottomSheetActions)
        binding.aboutAppLayout.setOnClickListener {
            dismiss()
            actions.onAboutClicked()
        }

        binding.logoutLayout.setOnClickListener {
            actions.onLogoutClicked()
        }

        binding.cancelLayout.setOnClickListener {
            dismiss()
        }
    }

    interface AccountBottomSheetActions{
        fun onAboutClicked()
        fun onLogoutClicked()
    }
}