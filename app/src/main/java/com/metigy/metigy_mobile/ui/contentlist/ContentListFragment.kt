package com.metigy.metigy_mobile.ui.contentlist

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.consts.TAG_DEBUG
import com.metigy.metigy_mobile.core.consts.TAG_PERMISSION
import com.metigy.metigy_mobile.core.extension.*
import com.metigy.metigy_mobile.core.result.EventObserver
import com.metigy.metigy_mobile.core.utils.TimezoneProvider
import com.metigy.metigy_mobile.databinding.FragmentContentListBinding
import com.metigy.metigy_mobile.ui.main.MainViewModel
import com.metigy.metigy_mobile.ui.main.dropdown.BrandGroupSelectionArrayAdapter
import com.metigy.metigy_mobile.ui.main.dropdown.PageSelectionArrayAdapter
import com.urbanairship.UAirship.getPackageName
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import java.io.File
import java.time.Instant
import javax.inject.Inject


@AndroidEntryPoint
class ContentListFragment : Fragment(), CalendarModal.CalendarActions {
    private val viewModel: ContentListViewModel by viewModels()

    private val parentActivityViewModel: MainViewModel by activityViewModels()

    @Inject
    lateinit var timezoneProvider: TimezoneProvider

    private lateinit var socialMediaAppOrBrowserLauncher: ActivityResultLauncher<Intent>

    private val adapter: ContentListRecyclerViewAdapter by lazy {
        ContentListRecyclerViewAdapter(timezoneProvider)
    }

    private lateinit var binding: FragmentContentListBinding

    private lateinit var requestPermissionLauncher: ActivityResultLauncher<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        socialMediaAppOrBrowserLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            //Assuming that local sharing has been done successfully
            Timber.tag(TAG_DEBUG).d("Sharing done either Successfully or not")

            viewModel.contentToShareLocally?.let { content ->
                postToNextSocialMediaApp(content, false)
            }
        }

        requestPermissionLauncher =
            registerForActivityResult(
                ActivityResultContracts.RequestPermission()
            ) { isGranted: Boolean ->
                if (isGranted) {
                    // Permission is granted. Continue the action or workflow in your
                    // app.
                    viewModel.contentToShareLocally?.let { content ->
                        postToNextSocialMediaApp(content, true)
                    }
                } else {
                    // Explain to the user that the feature is unavailable because the
                    // features requires a permission that the user has denied. At the
                    // same time, respect the user's decision. Don't link to system
                    // settings in an effort to convince the user to change their
                    // decision.
                    Timber.tag(TAG_PERMISSION).d("Just denied permission")

                    //TODO Show a dialog to the user
                }
            }

        viewModel.setInitialDate()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentContentListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUi()
        startObserving()
    }

    private fun setupUi() {
        binding.recyclerView.apply {
            verticalLayoutManager()
            itemAnimator = null
            adapter = this@ContentListFragment.adapter
        }

        binding.swipeRefresh.setOnRefreshListener {
            viewModel.loadContents()
        }

        binding.statusFilterLayout.onStatusClicked = { contentStatus ->
            viewModel.statusFilterSelectedByUser(contentStatus)
        }

        binding.dateSelectionTextView.apply {
            setOnClickListener {
                selectingState = true
                CalendarModal.createAndShow(childFragmentManager, viewModel.selectedDate.value ?: Instant.now())
            }
        }
    }

    private fun startObserving() {
        viewModel.brandContentFilteredList.observe(viewLifecycleOwner) { brandContentAndStateListFiltered ->

            adapter.data = brandContentAndStateListFiltered

            adapter.onDeclineClicked = { content ->
                viewModel.declineContent(content)
            }

            adapter.onApprovedClicked = { content ->
                viewModel.approveContent(content)
            }

            adapter.onPostNowClicked = { content ->
                viewModel.postContent(content)
            }

            adapter.onKeepScheduledClicked = {
                showToast(R.string.post_kept_scheduled, Toast.LENGTH_LONG)
            }
        }

        viewModel.selectedStatusFilter.observe(viewLifecycleOwner) { status ->
            binding.statusFilterLayout.setStatusSelected(status)
        }

        viewModel.selectedDate.observe(viewLifecycleOwner) { dateTime ->
            binding.dateSelectionTextView.setDate(dateTime)
        }

        viewModel.error.observe(viewLifecycleOwner, EventObserver { error ->
            showErrorToast(error)
        })

        viewModel.loading.observe(viewLifecycleOwner) { loading ->
            binding.swipeRefresh.isRefreshing = loading
            binding.recyclerView.showOrGone(loading.not())
        }

        viewModel.startLocalSharing.observe(
            viewLifecycleOwner,
            EventObserver {
                checkPermission()
            }
        )

        parentActivityViewModel.selectedBrandGroupAndPages.observe(viewLifecycleOwner) { pair ->
            viewModel.selectedBrandGroupId = pair.first.id
            viewModel.loadContents()
        }

        parentActivityViewModel.deselectedPageSetObservable.observe(viewLifecycleOwner) { pageIds ->
            viewModel.deselectedPageIds = pageIds
        }

        parentActivityViewModel.selectedBrandGroupAndPages.observe(viewLifecycleOwner) { pair ->

            val pageSelectionArrayAdapter = PageSelectionArrayAdapter.newInstance(
                requireContext(), pair, parentActivityViewModel.deselectedPageSet,
                parentActivityViewModel.buildBrandGroupDropDownPreviewList(parentActivityViewModel.pageListToSocialPlatformList(pair.second), pair.first)
            )

            pageSelectionArrayAdapter.onPreviewClicked = {
                binding.spinner.performClick()
            }

            pageSelectionArrayAdapter.onBrandGroupSwitchClicked = {
                val brandGroupArrayAdapter = BrandGroupSelectionArrayAdapter.newInstance(requireContext(), parentActivityViewModel.getAllBrandGroups(), pair.first.id)
                brandGroupArrayAdapter.onBrandGroupClicked = { id ->
                    parentActivityViewModel.brandGroupSelected(id)
                }
                binding.spinner.adapter = brandGroupArrayAdapter
            }

            pageSelectionArrayAdapter.onPageClicked = {
                parentActivityViewModel.selectedPagesChanged()
            }
            binding.spinner.adapter = pageSelectionArrayAdapter
        }

    }

    private fun checkPermission() {
        when {
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED -> {
                // You can use the API that requires the permission.
                viewModel.contentToShareLocally?.let { content ->
                    postToNextSocialMediaApp(content, true)
                }
            }
            shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE) -> {
                // In an educational UI, explain to the user why your app requires this
                // permission for a specific feature to behave as expected. In this UI,
                // include a "cancel" or "no thanks" button that allows the user to
                // continue using your app without granting the permission.
                showSimpleDialog(
                    titleId = R.string.storage_permission_title,
                    messageId = R.string.storage_permission_message,
                    positiveButtonTextId = R.string.storage_permission_accept,
                    negativeButtonTextId = R.string.storage_permission_deny,
                    onConfirm = {
                        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                        val uri: Uri = Uri.fromParts("package", getPackageName(), null)
                        intent.data = uri
                        startActivity(intent)
                    }
                )
            }
            else -> {
                // You can directly ask for the permission.
                // The registered ActivityResultCallback gets the result of this request.
                requestPermissionLauncher.launch(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
        }

    }

    private fun postToNextSocialMediaApp(content: ContentToBeSharedLocally, firstShare: Boolean) {
        content.smAppList.firstOrNull()?.let { currentSmAppInfo ->
            Timber.tag(TAG_DEBUG).d("Start posting")
            requireActivity().copyToClipboard(content.text)
            showToast(R.string.copied_to_clipboard)
            if (firstShare) {
                viewModel.onContentSharedLocally()
            }
            if (viewModel.isAppInstalled(currentSmAppInfo)) {
                showPostingConfirmationDialog(smAppAndInfo = currentSmAppInfo, firstShare = firstShare, onConfirm = {
                    if (content.imageFile != null) {
                        showNextDialogOrShareDirectly(smAppAndInfo = currentSmAppInfo, imageFile = content.imageFile)
                    } else {
                        showNextDialogOrShareDirectly(smAppAndInfo = currentSmAppInfo, text = content.text)
                    }
                })
            } else {
                showAppNotFoundDialog(smAppAndInfo = currentSmAppInfo, onConfirm = {
                    removeFirstSmAppFromList()
                    openBrowserForApp(currentSmAppInfo)
                })
            }
        } ?: run {
            Timber.tag(TAG_DEBUG).d("Posting done")
        }
    }

    private fun showPostingConfirmationDialog(smAppAndInfo: SmAppAndInfo, firstShare: Boolean, onConfirm: () -> Unit) {
        showSimpleDialog(
            title = getString(R.string.post_ready),
            message = if (firstShare) getString(R.string.publish_post_now) else "${getString(R.string.continue_post_to)} ${getString(smAppAndInfo.appNameResId)}",
            positiveButtonText = "${getString(R.string.post_now_to)} ${getString(smAppAndInfo.appNameResId)}",
            negativeButtonTextId = R.string.cancel,
            onConfirm = {
                onConfirm()
            },
            dismissible = false
        )
    }

    private fun showAppNotFoundDialog(smAppAndInfo: SmAppAndInfo, onConfirm: () -> Unit) {
        showSimpleDialog(
            title = "${getString(smAppAndInfo.appNameResId)} ${getString(R.string.app_not_found)}",
            messageId = R.string.publish_post_via_browser,
            positiveButtonTextId = R.string.open_browser,
            negativeButtonTextId = R.string.cancel,
            onConfirm = {
                onConfirm()
            }
        )
    }

    private fun showNextDialogOrShareDirectly(smAppAndInfo: SmAppAndInfo, imageFile: File? = null, text: String? = null) {
        if (smAppAndInfo.titleResId != null) {
            showSimpleDialog(
                titleId = smAppAndInfo.titleResId,
                messageId = smAppAndInfo.descriptionResId,
                positiveButtonTextId = R.string.continue_string,
                negativeButtonTextId = R.string.go_back,
                onConfirm = {
                    shareContent(imageFile, text)
                },
                dismissible = false
            )
        } else {
            shareContent(imageFile, text)
        }
    }

    private fun shareContent(imageFile: File? = null, text: String? = null) {
        Timber.tag(TAG_DEBUG).d("shareContent")
        removeFirstSmAppFromList()
        if (imageFile != null) {
            socialMediaAppOrBrowserLauncher.launch(intentForImageSharing(imageFile))
        } else {
            socialMediaAppOrBrowserLauncher.launch(intentForTextSharing(text.orEmpty()))
        }
    }

    private fun openBrowserForApp(currentSmAppInfo: SmAppAndInfo) {
        socialMediaAppOrBrowserLauncher.launch(intentForLaunchingBrowser(currentSmAppInfo.websiteUrl))
    }

    private fun removeFirstSmAppFromList() {
        viewModel.removeFirstSmApp()
    }

    override fun onCalendarClosed() {
        binding.dateSelectionTextView.selectingState = false
    }

    override fun onDateSelected(date: Instant) {
        viewModel.dateFilterSelectedByUser(date)
    }
}