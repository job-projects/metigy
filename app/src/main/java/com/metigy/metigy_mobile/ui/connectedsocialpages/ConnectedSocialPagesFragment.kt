package com.metigy.metigy_mobile.ui.connectedsocialpages

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.extension.*
import com.metigy.metigy_mobile.core.result.EventObserver
import com.metigy.metigy_mobile.core.ui.StackedButtonDialogFragment
import com.metigy.metigy_mobile.databinding.FragmentConnectedSocialPagesBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ConnectedSocialPagesFragment : Fragment(), StackedButtonDialogFragment.OnButtonClickListener {
    private val viewModel: ConnectedSocialPagesViewModel by viewModels()
    private lateinit var binding: FragmentConnectedSocialPagesBinding

    private val adapter: ConnectedSocialPageAdapter by lazy {
        ConnectedSocialPageAdapter(requireContext())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentConnectedSocialPagesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUi()
        startObserving()
    }

    private fun setupUi() {
        binding.connectedPagesRecyclerView.apply {
            verticalLayoutManager()
            binding.connectedPagesRecyclerView.adapter = this@ConnectedSocialPagesFragment.adapter
        }

        binding.editButton.setOnClickListener {
            setRemoveState(adapter.removeState.not())
        }

        binding.doneButton.setOnClickListener {
            launchHomeScreen()
        }

        binding.connectNewProfileLayout.setOnClickListener {
            navigateUp()
        }

        binding.helpButton.setOnClickListener {
            StackedButtonDialogFragment.launchHelpDialog(this)
        }

        binding.swipeRefresh.setOnRefreshListener {
            viewModel.onLoadingConnectedPages()
        }

        adapter.onRemoveClicked = { page ->
            viewModel.removePage(pageId = page.id)
        }
    }

    private fun startObserving() {
        viewModel.connectedPages.observe(viewLifecycleOwner) { connectedSocialPages ->
            adapter.data = connectedSocialPages
            binding.connectedPagesCountTextView.text = getString(R.string.connected_n, connectedSocialPages.size)
        }

        viewModel.loading.observe(viewLifecycleOwner) { loading ->
            binding.swipeRefresh.isRefreshing = loading
        }

        viewModel.error.observe(viewLifecycleOwner, EventObserver { error ->
            showErrorToast(error)
        })
    }

    private fun setRemoveState(remove: Boolean) {
        adapter.removeState = remove
        if (remove) {
            binding.editButton.setText(R.string.done)
        } else {
            binding.editButton.setText(R.string.edit)
        }
    }

    override fun onFirstButtonClicked(tag: String?) {
        openUrl(viewModel.helpUrl)
    }

    override fun onSecondButtonClicked(tag: String?) {
        composeEmail(listOf(viewModel.supportEmail))
    }
}