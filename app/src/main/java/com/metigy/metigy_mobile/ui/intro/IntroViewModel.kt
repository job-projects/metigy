package com.metigy.metigy_mobile.ui.intro

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.data.appmodels.IntroPagerContent
import com.metigy.metigy_mobile.data.preferences.AppPreferences
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class IntroViewModel @Inject constructor(private val appPreferences: AppPreferences) : ViewModel() {
    private val _currentTabPosition = MutableLiveData<Int>()
    val currentTabPosition: LiveData<Int>
        get() = _currentTabPosition

    var contentList: List<IntroPagerContent> = listOf(
        IntroPagerContent(headingResId = R.string.intro_one_heading, bodyResId = R.string.intro_one_body, imageResId = R.drawable.intro_1),
        IntroPagerContent(headingResId = R.string.intro_two_heading, bodyResId = R.string.intro_two_body, imageResId = R.drawable.intro_2),
        IntroPagerContent(headingResId = R.string.intro_three_heading, bodyResId = R.string.intro_three_body, imageResId = R.drawable.intro_3),
        IntroPagerContent(headingResId = R.string.intro_four_heading, bodyResId = R.string.intro_four_body, imageResId = R.drawable.intro_4),
    )

    private val tabCount = contentList.size + 1

    val lastIntroScreenPosition = tabCount - 2

    val loginScreenPosition = tabCount - 1

    fun setCurrentTabPosition(position: Int) {
        _currentTabPosition.value = position
    }

    fun getIfPreviouslyLoggedIn() = appPreferences.getIsPreviouslyLoggedIn()
}