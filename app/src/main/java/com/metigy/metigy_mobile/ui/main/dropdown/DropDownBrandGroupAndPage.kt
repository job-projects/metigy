package com.metigy.metigy_mobile.ui.main.dropdown

import androidx.annotation.DrawableRes

sealed class DropDownBrandGroupAndPage {
    data class BrandGroup(val id: Int, val name: String, val logoUrl: String) : DropDownBrandGroupAndPage()
    data class PlatformPage(val id: Int, val name: String, @DrawableRes val logoResId: Int) : DropDownBrandGroupAndPage()
    object ConnectNewProfile : DropDownBrandGroupAndPage()
}