package com.metigy.metigy_mobile.ui.contentlist

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.metigy.metigy_mobile.core.utils.TimeProvider
import com.metigy.metigy_mobile.databinding.ModalCalendarBinding
import dagger.hilt.android.AndroidEntryPoint
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import javax.inject.Inject

@AndroidEntryPoint
class CalendarModal : BottomSheetDialogFragment() {

    @Inject
    lateinit var timeProvider: TimeProvider

    private lateinit var binding: ModalCalendarBinding

    private val calendarActions: CalendarActions
        get() = (parentFragment as CalendarActions)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = ModalCalendarBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.calendarView.apply {

            date = getInitialDateFromBundle()

            setOnDateChangeListener { _, year, month, dayOfMonth ->
                calendarActions.onDateSelected(ZonedDateTime.of(year, month + 1, dayOfMonth, 0, 0, 0, 0, ZoneId.systemDefault()).toInstant())
                dismiss().also {
                    calendarActions.onCalendarClosed()
                }
            }
        }
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        calendarActions.onCalendarClosed()
    }

    interface CalendarActions {
        fun onCalendarClosed()
        fun onDateSelected(date: Instant)
    }

    private fun getInitialDateFromBundle() = arguments?.getLong(BUNDLE_KEY, timeProvider.nowAsEpochSecond()) ?: timeProvider.nowAsEpochSecond()

    companion object {
        const val BUNDLE_KEY = "InitialDate"

        fun createAndShow(fragmentManager: FragmentManager, initialDate: Instant) {
            CalendarModal().apply {
                arguments = Bundle().apply {
                    putLong(BUNDLE_KEY, initialDate.toEpochMilli())
                }
            }.show(fragmentManager, null)
        }
    }
}