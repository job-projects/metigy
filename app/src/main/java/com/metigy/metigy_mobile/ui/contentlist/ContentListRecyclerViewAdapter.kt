package com.metigy.metigy_mobile.ui.contentlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.extension.*
import com.metigy.metigy_mobile.core.utils.TimezoneProvider
import com.metigy.metigy_mobile.data.appmodels.BrandContentModel
import com.metigy.metigy_mobile.data.appmodels.SocialPlatform
import com.metigy.metigy_mobile.databinding.ItemBrandContentBinding
import com.metigy.metigy_mobile.databinding.ItemSocialPageContentBinding
import kotlin.math.absoluteValue
import kotlin.math.pow


class ContentListRecyclerViewAdapter(private val timezoneProvider: TimezoneProvider) : RecyclerView.Adapter<ContentListRecyclerViewAdapter.ContentItemViewHolder>() {

    var data: List<BrandContentAndState> = listOf()
        set(value) {
            val diffResult = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun getOldListSize(): Int = field.size

                override fun getNewListSize(): Int = value.size

                override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = field[oldItemPosition].brandContent.id == value[newItemPosition].brandContent.id

                override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = value[newItemPosition] == field[oldItemPosition]
            })

            diffResult.dispatchUpdatesTo(this)
            field = value
        }

    var onApprovedClicked: (brandContent: BrandContentModel) -> Unit = {}
    var onDeclineClicked: (brandContent: BrandContentModel) -> Unit = {}
    var onPostNowClicked: (brandContent: BrandContentModel) -> Unit = {}
    var onKeepScheduledClicked: () -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContentItemViewHolder =
        ContentItemViewHolder(ItemBrandContentBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ContentItemViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    inner class ContentItemViewHolder(private val binding: ItemBrandContentBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(contentAndState: BrandContentAndState) {
            val content = contentAndState.brandContent
            setupViewPager(content)
        }

        private fun setupViewPager(content: BrandContentModel) {
            binding.contentViewPager.apply {

                offscreenPageLimit = 3
                val sliderGap: Float = getDimension(R.dimen.slider_gap)
                val sliderMarginFromScreenEdge: Float = getDimension(R.dimen.slider_margin_from_screen_edge)

                setPageTransformer { page, position ->
                    page.translationX = position * -(2 * sliderMarginFromScreenEdge - sliderGap)
                    page.scaleY = (0.92.pow(position.absoluteValue.toDouble())).toFloat()
                }
                adapter = BrandContentRecyclerViewAdapter(timezoneProvider).apply {
                    data = content
                }
            }

        }
    }
}

class BrandContentRecyclerViewAdapter(private val timezoneProvider: TimezoneProvider) : RecyclerView.Adapter<BrandContentRecyclerViewAdapter.ContentViewHolder>() {
    var data: BrandContentModel? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContentViewHolder =
        ContentViewHolder(ItemSocialPageContentBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ContentViewHolder, position: Int) {
        holder.bind(data ?: error("Illegal State. Data must not be null"), position)
    }

    override fun getItemCount(): Int = data?.pageCount().orZero()

    inner class ContentViewHolder(private val binding: ItemSocialPageContentBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(content: BrandContentModel, position: Int) {
            binding.root.clipToOutline = true
            content.imageUrl()?.let { url ->
                binding.contentImageView.loadImage(url)
            } ?: run {
                binding.contentImageView.gone()
            }
            when (content.pageInfoList[position].socialPlatform) {
                SocialPlatform.FACEBOOK -> {
                    binding.facebookContentTextView.visible()
                    binding.instagramContentTextView.gone()
                    binding.facebookContentTextView.text = content.message
                }
                SocialPlatform.INSTAGRAM -> {
                    binding.facebookContentTextView.gone()
                    binding.instagramContentTextView.visible()
                    binding.instagramContentTextView.text = content.message
                }
            }
            binding.platformImageView.setImageResource(content.pageInfoList[position].socialPlatform.toSocialPlatformForView().coloredIconId)
            binding.pageTypeTextView.setText(content.pageInfoList[position].socialPageType.toSocialPageTypeForView().typeNameForContentPreviewResId)
            binding.postMethodTextView.setText(content.pageInfoList[position].socialPageType.toSocialPageTypeForView().postMethod.methodNameResId)
            binding.postMethodTextView.setStartDrawableStart(
                drawableId = content.pageInfoList[position].socialPageType.toSocialPageTypeForView().postMethod.methodIconResId,
                sizeInDP = 12,
                drawablePaddingInDP = 4
            )

            binding.timeTextView.text = content.publishTime.formatToScheduledPreview(timezoneProvider)
        }
    }

}

data class BrandContentAndState(
    val brandContent: BrandContentModel,
    val loading: Boolean = false,
)