package com.metigy.metigy_mobile.ui.launcher

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.metigy.metigy_mobile.BuildConfig
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.result.Event
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.data.airship.NotificationContent
import com.metigy.metigy_mobile.data.appInfoRepository.AppInfoRepository
import com.metigy.metigy_mobile.data.appconfigrepository.AppConfig
import com.metigy.metigy_mobile.data.appmodels.AuthenticationRequirementStatus
import com.metigy.metigy_mobile.data.appmodels.ForceUpdate
import com.metigy.metigy_mobile.data.appmodels.Prompt
import com.metigy.metigy_mobile.data.appmodels.Version
import com.metigy.metigy_mobile.data.authentication.Authentication
import com.metigy.metigy_mobile.data.authentication.LocalUserInfoRepository
import com.metigy.metigy_mobile.data.authentication.RemoteUserInfoRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LauncherViewModel @Inject constructor(
    private val authentication: Authentication,
    private val remoteUserInfoRepository: RemoteUserInfoRepository,
    private val localUserInfoRepository: LocalUserInfoRepository,
    private val appConfig: AppConfig,
    private val appInfoRepository: AppInfoRepository
) : ViewModel() {

    private val _navigateToLoginScreen = MutableLiveData<AuthenticationRequirementStatus>()
    val navigateToLoginScreen: LiveData<AuthenticationRequirementStatus>
        get() = _navigateToLoginScreen

    private val _navigateToHomeScreen = MutableLiveData<NotificationContent?>()
    val navigateToHomeScreen: LiveData<NotificationContent?>
        get() = _navigateToHomeScreen

    private val _navigateToSocialProfileConnect = MutableLiveData<Unit>()
    val navigateToSocialProfileConnect: LiveData<Unit>
        get() = _navigateToSocialProfileConnect

    private val _error = MutableLiveData<Event<Result.Error>>()
    val error: LiveData<Event<Result.Error>>
        get() = _error

    private val _forceUpdateMessage = MutableLiveData<ForceUpdate>()
    val forceUpdateMessage: LiveData<ForceUpdate>
        get() = _forceUpdateMessage

    private val _showPromotionMessage = MutableLiveData<Prompt>()
    val showPromotionMessage: LiveData<Prompt>
        get() = _showPromotionMessage

    private val _appIsGoodToGo = MutableLiveData<Event<Unit>>()
    val appIsGoodToGo: LiveData<Event<Unit>>
        get() = _appIsGoodToGo

    //-------------------------------------------------------Opened by user---------------------------------------------------------------------------------------------------------
    fun findAuthenticationStatusAndNavigate() {
        when (val status = authentication.authenticationStatus()) {
            is AuthenticationRequirementStatus.AuthenticationNeededCredentialsAvailable -> {
                _navigateToLoginScreen.value = status
            }
            AuthenticationRequirementStatus.AuthenticationNeededCredentialsNotAvailable -> {
                _navigateToLoginScreen.value = status
            }
            AuthenticationRequirementStatus.AuthenticationNotNeeded -> {
                _navigateToHomeScreen.value = null
            }
        }
    }

    //-------------------------------------------------------Email verification area------------------------------------------------------------------------------------------------
    //TODO Needs to be unit tested
    fun callVerificationUrlAndNavigate(url: String) {

        viewModelScope.launch {
            if (authentication.isUserLoggedInAtLaunch()) {
                _error.value = Event(Result.Error(messageId = R.string.user_already_logged_in))
                _navigateToHomeScreen.value = null
            } else {
                when (val verificationKeyResult = authentication.getVerificationKey(url)) {
                    is Result.Success -> {
                        when (val userCredentials = authentication.getCachedSignUpCredentials()) {
                            null -> {
                                _navigateToLoginScreen.value = AuthenticationRequirementStatus.AuthenticationNeededCredentialsNotAvailable
                            }
                            else -> {
                                when (val loginResult = authentication.login(email = userCredentials.userName, password = userCredentials.password)) {
                                    is Result.Success -> {
                                        when (val verificationResult = authentication.verifyEmail(key = verificationKeyResult.data, token = loginResult.data)) {
                                            is Result.Success -> {
                                                when (val userInfoResult = remoteUserInfoRepository.getUserInfo(loginResult.data)) {
                                                    is Result.Success -> {
                                                        authentication.saveToken(loginResult.data)
                                                        localUserInfoRepository.saveUserInfo(userInfoModel = userInfoResult.data)
                                                        authentication.clearSignUpCredentialsCache()
                                                        _navigateToSocialProfileConnect.value = Unit
                                                    }
                                                    is Result.Error -> {
                                                        //userInfo() failed
                                                        _error.value = Event(userInfoResult)
                                                    }
                                                }
                                            }
                                            is Result.Error -> {
                                                //verify() failed
                                                _error.value = Event(verificationResult)
                                            }
                                        }
                                    }
                                    is Result.Error -> {
                                        //login() failed
                                        _error.value = Event(loginResult)
                                    }
                                }

                            }
                        }
                    }
                    is Result.Error -> {
                        //Getting verification key failed
                        _error.value = Event(verificationKeyResult)
                    }
                }
            }
        }
    }

    //----------------------------------------------------------Push notification area---------------------------------------------------------------------------------------------\
    fun handlePushNotification(notificationContent: NotificationContent, isAppRunning: Boolean) {
        if (isAppRunning) {
            //Authentication not needed
            _navigateToHomeScreen.value = notificationContent
        } else {
            when (val status = authentication.authenticationStatus()) {
                is AuthenticationRequirementStatus.AuthenticationNeededCredentialsAvailable -> {
                    _navigateToLoginScreen.value = status
                }
                AuthenticationRequirementStatus.AuthenticationNeededCredentialsNotAvailable -> {
                    _navigateToLoginScreen.value = status
                }
                AuthenticationRequirementStatus.AuthenticationNotNeeded -> {
                    _navigateToHomeScreen.value = notificationContent
                }
            }
        }
    }

    //---------------------------------------------------------------App Config area------------------------------------------------------------------------------------------------\
    fun checkAppConfig() {
        viewModelScope.launch {
            when (val result = appConfig.getAppConfig()) {
                is Result.Success -> {
                    val forceUpdate = result.data.forceUpdate
                    val prompt = result.data.prompt
                    if (forceUpdate != null) {

                        //TODO Find another solution later
                        val version = if (BuildConfig.BUILD_TYPE == "release") appInfoRepository.appVersion().versionName else Version("1.1.0")
                        if (forceUpdate.version > version) {
                            _forceUpdateMessage.value = forceUpdate.apply {}
                        } else {
                            //TODO This should call checkPrompt function later
                            goodToGo()
                        }
                    } else {
                        //TODO This should call checkPrompt function later
                        goodToGo()
                    }
                }
                is Result.Error -> {
                    goodToGo()
                }
            }
        }
    }

    private fun checkPrompt(prompt: Prompt?) {
        if (prompt != null) {
            val publishDate = prompt.publishDate
            val latestPublishDate = appConfig.latestPromptCheckDate()
            if (publishDate > latestPublishDate) {
                _showPromotionMessage.value = prompt.apply {}
            } else {
                goodToGo()
            }
        } else {
            goodToGo()
        }
    }

    private fun goodToGo() {
        _appIsGoodToGo.value = Event(Unit)
    }
}