package com.metigy.metigy_mobile

import android.app.Application
import android.content.Context
import com.urbanairship.UAirship
import dagger.hilt.android.HiltAndroidApp
import dagger.hilt.android.qualifiers.ApplicationContext
import timber.log.Timber
import javax.inject.Inject

@HiltAndroidApp
class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        setupTimber()
        enableAirship()
    }

    private fun setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun enableAirship() {
        UAirship.shared().pushManager.userNotificationsEnabled = true
    }
}

class ContextProvider @Inject constructor(@ApplicationContext private val context: Context) {
    fun context() = context
}