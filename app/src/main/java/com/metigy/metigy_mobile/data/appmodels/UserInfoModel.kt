package com.metigy.metigy_mobile.data.appmodels

data class UserInfoModel(
    val email: String,
    val firstName: String? = null,
    val id: Int,
    val isVerified: Boolean,
    val lastName: String? = null
)