package com.metigy.metigy_mobile.data.apimodels

import com.metigy.metigy_mobile.data.appmodels.BrandModel

data class BrandsResponse(
    val ok: Boolean? = null,
    val result: List<BrandResult>? = null
)

data class BrandResult(
    val id: Int? = null,
    val industry: Industry? = null,
    val isSetup: Boolean? = null,
    val name: String? = null,
    val profileUuid: String? = null,
    val timezone: Timezone? = null,
    val uuid: String? = null
) {
    fun toBrandModel(): BrandModel? {
        return if (id != null) {
            BrandModel(
                id = id
            )
        } else null
    }
}

data class Industry(
    val id: Int? = null,
    val name: String? = null
)