package com.metigy.metigy_mobile.data.apimodels

import com.google.gson.annotations.SerializedName
import com.metigy.metigy_mobile.data.apiservice.SocialPlatformAndPageType
import com.metigy.metigy_mobile.data.appmodels.SocialPageInfoModel

data class SocialPageListRequestModel(
    val action: String? = null,
    val payload: PageListPayload? = null,
    val platform: String? = null
)

data class PageListPayload(
    val authUuid: String? = null
)

//--------------------------------------------------------------------------

data class SocialPageListResponseModel(
    @SerializedName("data")
    val socialPageListResult: List<SocialPageListResponseData>? = null,
    val ok: Boolean? = null
)

data class SocialPageListResponseData(
    val externalId: String? = null,
    val imageUrl: String? = null,
    val name: String? = null,
    val pageToken: String? = null,
    val publicUrl: String? = null,
    val type: String? = null
) {
    fun toSocialPageInfoModel(): SocialPageInfoModel? {
        val pageType = SocialPlatformAndPageType.fromApiName(type.orEmpty())
        return if (pageType != null && externalId != null) {
            SocialPageInfoModel(
                externalId = externalId,
                imageUrl = imageUrl.orEmpty(),
                name = name.orEmpty(),
                socialPlatform = pageType.socialPlatform,
                socialPageType = pageType.socialPageType
            )
        } else null
    }
}