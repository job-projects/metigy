package com.metigy.metigy_mobile.data.appmodels

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class IntroPagerContent(
    val headingResId: Int,
    val bodyResId: Int,
    val imageResId: Int,
) : Parcelable