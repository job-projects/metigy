package com.metigy.metigy_mobile.data.appmodels

import java.time.Instant

data class AndroidAppConfigModel(
    val forceUpdate: ForceUpdate?,
    val prompt: Prompt?
)

data class ForceUpdate(
    val description: String,
    val title: String,
    val version: Version
)

data class Prompt(
    val description: String,
    val publishDate: Instant,
    val title: String
)