package com.metigy.metigy_mobile.data.appmodels

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserCredentialsModel(
    val userName: String,
    val password: String
) : Parcelable