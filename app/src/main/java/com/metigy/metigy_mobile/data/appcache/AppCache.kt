package com.metigy.metigy_mobile.data.appcache

import com.metigy.metigy_mobile.data.appmodels.UserCredentialsModel
import javax.inject.Inject

interface AppCache {
    fun saveSignUpCredentials(signUpCredentials: UserCredentialsModel)
    fun getSignUpCredentials(): UserCredentialsModel?
    fun clearSignUpCredentials()
}

class DefaultAppCache @Inject constructor() : AppCache {
    private var appCache: AppCacheModel = AppCacheModel()

    override fun saveSignUpCredentials(signUpCredentials: UserCredentialsModel) {
        appCache.signUpCredentials = signUpCredentials
    }

    override fun getSignUpCredentials(): UserCredentialsModel? = appCache.signUpCredentials

    override fun clearSignUpCredentials() {
        appCache.signUpCredentials = null
    }
}

data class AppCacheModel(
    var signUpCredentials: UserCredentialsModel? = null
)