package com.metigy.metigy_mobile.data.apiservice

import com.metigy.metigy_mobile.data.apimodels.SocialPageListRequestModel
import com.metigy.metigy_mobile.data.apimodels.SocialPageListResponseModel
import com.metigy.metigy_mobile.data.apimodels.SocialTokenRequestModel
import com.metigy.metigy_mobile.data.apimodels.SocialTokenResponseModel
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface ApiServiceForServiceUrl {

    @POST(RepoUrl.SOCIAL_AUTH)
    suspend fun sendSocialToken(@Header(ApiParamKey.AUTHORIZATION) authorization: String, @Body requestModel: SocialTokenRequestModel): SocialTokenResponseModel

    @POST(RepoUrl.LIST_PAGES)
    suspend fun listSocialPages(@Header(ApiParamKey.AUTHORIZATION) authorization: String, @Body requestModel: SocialPageListRequestModel): SocialPageListResponseModel

}