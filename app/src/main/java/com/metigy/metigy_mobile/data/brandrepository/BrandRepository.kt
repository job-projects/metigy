package com.metigy.metigy_mobile.data.brandrepository

import com.metigy.metigy_mobile.core.extension.toAppError
import com.metigy.metigy_mobile.core.extension.toRequestBody
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.data.apiservice.ApiAction
import com.metigy.metigy_mobile.data.apiservice.ApiParamKey
import com.metigy.metigy_mobile.data.apiservice.ApiServiceForAppUrl
import com.metigy.metigy_mobile.data.appmodels.BrandGroupModel
import com.metigy.metigy_mobile.data.appmodels.BrandModel
import com.metigy.metigy_mobile.data.preferences.AppPreferences
import okhttp3.RequestBody
import javax.inject.Inject

interface BrandRepository {
    suspend fun listBrandGroups(): Result<List<BrandGroupModel>>
    suspend fun getBrand(brandGroupId: Int): Result<List<BrandModel>>
}

class DefaultBrandRepository @Inject constructor(
    private val apiService: ApiServiceForAppUrl,
    private val preferences: AppPreferences
) : BrandRepository {
    val token: String
        get() = preferences.getUserToken().orEmpty()

    override suspend fun listBrandGroups(): Result<List<BrandGroupModel>> {
        return try {
            val body = brandGroupListBody()
            val brandGroupList = apiService.brandGroups(
                authorization = token,
                brandGroups = body
            )
            Result.Success(
                brandGroupList.result?.mapNotNull {
                    it.toBrandGroupModel()
                } ?: listOf()
            )
        } catch (e: Exception) {
            e.toAppError()
        }
    }

    override suspend fun getBrand(brandGroupId: Int): Result<List<BrandModel>> {
        return try {
            Result.Success(
                apiService.getBrand(authorization = token, getBrandBody(brandGroupId)).result?.mapNotNull { brandResult ->
                    brandResult.toBrandModel()
                } ?: listOf()
            )
        } catch (e: Exception) {
            e.toAppError()
        }
    }

    private fun brandGroupListBody(): RequestBody = mapOf(
        ApiParamKey.ACTION to ApiAction.LIST_BRAND_GROUPS.action
    ).toRequestBody()

    private fun getBrandBody(brandGroupId: Int) = mapOf(
        ApiParamKey.ACTION to ApiAction.LIST.action,
        ApiParamKey.CONTEXT_BRAND_GROUP_ID to brandGroupId.toString()
    ).toRequestBody()
}