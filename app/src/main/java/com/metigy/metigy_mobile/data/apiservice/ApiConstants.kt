package com.metigy.metigy_mobile.data.apiservice

import com.metigy.metigy_mobile.data.appmodels.ContentStatus
import com.metigy.metigy_mobile.data.appmodels.SocialPageType
import com.metigy.metigy_mobile.data.appmodels.SocialPlatform

const val API_AUTHORIZATION_PREFIX = "JWT "

const val API_CONTENT_LIST_FIELDS =
    "id, type, title, message, image, video, link_url, publish_time, categories, content_type, end_time, status_id, brand_ids, page_ids, page_types, created_user, updated_user, created_user_id, updated_user_id, is_optin, brand_group_id, optin_status, ads_summary, has_dark_post, page_content_external_ids"

const val ANDROID = "android"

enum class ApiAction(val action: String) {
    LOGIN("login"),
    ME("me"),
    ADD_PUSH_TOKEN("addPushToken"),
    LIST_BRAND_GROUPS("listBrandGroups"),
    LIST("list"),
    CHANGE_STATUS("changeStatus"),
    CHANGE_PAGE_CONTENT_STATUS("changePageContentStatus"),
    SAVE_V2("saveV2"),
    GET("get"),
    RESET_PASSWORD("resetPassword"),
    REGISTER("register"),
    CHANGE_PLAN("changePlan"),
    RESEND_EMAIL_CODE("resendEmailCode"),
    VERIFY("verify"),
    CALLBACK("callback"),
    PAGE_LIST("page:list"),
    ADD("add"),
    DELETE_TOKEN("deleteToken")
}

object ApiParamKey {
    // common
    const val ACTION = "action"
    const val AUTHORIZATION = "authorization"

    // login - sign up
    const val EMAIL_KEY = "email"
    const val PASSWORD_KEY = "password"
    const val REMEMBER_KEY = "remember"
    const val DATA = "data"
    const val FIRST_NAME = "firstName"
    const val LAST_NAME = "lastName"

    // content list
    const val CONTEXT_BRAND_GROUP_ID = "contextBrandGroupId"
    const val SINCE = "since"
    const val LIMIT = "limit"
    const val STATUS = "status"
    const val FIELDS = "fields"

    // change content status
    const val ID = "id"
    const val STATUS_ID = "statusId"

    // change page content status
    const val CONTENT_ID = "content_id"
    const val LOCATION_ID = "location_id"
    const val PROCESSED_AT = "processed_at"

    // post device id
    const val TOKEN = "token"

    // post now
    const val TYPE_ID = "typeId"
    const val SCHEDULED_FOR = "scheduledFor"
    const val IS_PUBLISH_NOW = "isPublishNow"
    const val PAGES = "pages"

    // change plan
    const val PLAN = "plan"

    // Verify email
    const val KEY = "key"

    const val SYSTEM_TYPE = "systemType"

    const val PAGE_EXTERNAL_IDS = "pageExternalIds"
    const val BRAND_ID = "brandId"
    const val UUID = "uuid"
    const val PLATFORM = "platform"
}

object RepoUrl {
    const val LOGIN_URL = "/api/auth"

    const val LOGOUT_URL = "/auth/logout"

    const val USER_INFO_URL = "/api/user"

    const val DEVICE_URL = "/api/device"

    const val CLIENT_URL = "/api/client"

    const val LIST_BRAND_GROUP_URL = "/api/user"

    const val LIST_CONTENT_URL = "/api/hub"

    const val LIST_PLATFORM_PAGES_URL = "/api/platformPage"

    const val CONTENT_URL = "/api/content"

    const val SOCIAL_AUTH = "/auth/v1.1"

    const val LIST_PAGES = "proxy/proxy"

    const val BRAND = "api/brand"
}

object DataForPostKey {
    const val RESULT = "result"
    const val PAGES = "pages"
    const val SCHEDULED_FOR = "scheduledFor"
    const val TYPE = "type"
    const val ID = "id"
    const val TYPE_ID = "typeId"
}

enum class ApiContentStatus(val statusId: Int, val statusName: String, val businessContentStatus: ContentStatus?) {
    PENDING(statusId = 1, statusName = "pending", businessContentStatus = ContentStatus.DRAFT),
    INTERNAL_PENDING(statusId = 118, "internal-pending", businessContentStatus = ContentStatus.DRAFT),
    DRAFT(statusId = 71, statusName = "draft", businessContentStatus = ContentStatus.DRAFT),
    APPROVED(statusId = 11, statusName = "approved", businessContentStatus = ContentStatus.SCHEDULED),
    POSTED(statusId = -1, statusName = "posted", businessContentStatus = null),
    POSTING(statusId = 125, statusName = "posting", businessContentStatus = ContentStatus.POSTED),
    DECLINE(statusId = 21, statusName = "", businessContentStatus = null),
    SUCCEEDED(statusId = -1, statusName = "succeeded", businessContentStatus = null);

    companion object {
        fun fromId(id: Int): ApiContentStatus? = values().associateBy(ApiContentStatus::statusId)[id]

        fun fromBusinessContentStatus(businessContentStatus: ContentStatus) = values().filter {
            it.businessContentStatus == businessContentStatus
        }
    }
}

enum class SocialPlatformAndPageType(val socialPlatform: SocialPlatform, val apiName: String, val socialPageType: SocialPageType) {
    FACEBOOK_BUSINESS(socialPlatform = SocialPlatform.FACEBOOK, apiName = "facebook", socialPageType = SocialPageType.BUSINESS),
    FACEBOOK_PERSONAL(socialPlatform = SocialPlatform.FACEBOOK, apiName = "facebook-personal", socialPageType = SocialPageType.PERSONAL),
    INSTAGRAM_PERSONAL(socialPlatform = SocialPlatform.INSTAGRAM, apiName = "instagram-personal", socialPageType = SocialPageType.PERSONAL),
    INSTAGRAM_BUSINESS(socialPlatform = SocialPlatform.INSTAGRAM, apiName = "instagram-business", socialPageType = SocialPageType.BUSINESS);

    companion object {
        fun fromApiName(type: String) = values().associateBy(SocialPlatformAndPageType::apiName)[type]
    }
}

enum class ApiPlan(val type: String) {
    FREE("free")
}

enum class ApiSocialPlatform(val socialPlatform: SocialPlatform, val platformName: String) {
    FACEBOOK(socialPlatform = SocialPlatform.FACEBOOK, platformName = "facebook"),
    INSTAGRAM(socialPlatform = SocialPlatform.INSTAGRAM, platformName = "instagram"),
    TWITTER(socialPlatform = SocialPlatform.TWITTER, platformName = "twitter"),
    LINKEDIN(socialPlatform = SocialPlatform.LINKEDIN, platformName = "linkedin");

    companion object {
        fun fromSocialPlatform(socialPlatform: SocialPlatform) = values().associateBy(ApiSocialPlatform::socialPlatform)[socialPlatform] ?: error("Illegal state")
    }
}