package com.metigy.metigy_mobile.data.appconfigrepository

import com.metigy.metigy_mobile.core.extension.toAppError
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.core.utils.TimezoneProvider
import com.metigy.metigy_mobile.data.apiservice.ApiServiceForAppUrl
import com.metigy.metigy_mobile.data.appmodels.AndroidAppConfigModel
import com.metigy.metigy_mobile.data.preferences.AppPreferences
import java.time.Instant
import javax.inject.Inject

interface AppConfig {
    suspend fun getAppConfig(): Result<AndroidAppConfigModel>
    fun latestPromptCheckDate(): Instant?
}

class DefaultAppConfig @Inject constructor(
    private val apiService: ApiServiceForAppUrl,
    private val preferences: AppPreferences,
    private val timezoneProvider: TimezoneProvider
) : AppConfig {
    override suspend fun getAppConfig(): Result<AndroidAppConfigModel> =
        try {
            Result.Success(apiService.getAppConfig().toAndroidAppConfigModel(timezoneProvider))
        } catch (e: Exception) {
            e.toAppError()
        }

    override fun latestPromptCheckDate(): Instant? = preferences.getLatestPromptDate()
}