package com.metigy.metigy_mobile.data.apimodels

import com.metigy.metigy_mobile.core.extension.orFalse
import com.metigy.metigy_mobile.data.appmodels.UserInfoModel

data class UserInfoResponse(
    val ok: Boolean? = null,
    val result: UserInfoResult? = null
)

data class UserInfoResult(
    val createdAt: String? = null,
    val email: String? = null,
    val firstName: String? = null,
    val id: Int? = null,
    val image: Any? = null,
    val isActive: Boolean? = null,
    val isSuperuser: Any? = null,
    val isVerified: Boolean? = null,
    val isSocialOnlyLogin: Boolean? = null,
    val lastLogin: String? = null,
    val lastName: String? = null,
    val lastSeen: Any? = null,
    val timezone: Timezone? = null,
    val updatedAt: String? = null,
    val useEmail: Boolean? = null,
    val uuid: String? = null
) {
    fun toUserInfoModel(): UserInfoModel? =
        if (id != null && email != null) {
            UserInfoModel(id = id, email = email, firstName = firstName, lastName = lastName, isVerified = isVerified.orFalse())
        } else {
            null
        }

}