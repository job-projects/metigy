package com.metigy.metigy_mobile.data.appmodels

import com.metigy.metigy_mobile.data.apiservice.ApiSocialPlatform
import com.metigy.metigy_mobile.data.uimodels.SocialPageTypeForView
import com.metigy.metigy_mobile.data.uimodels.SocialPlatformForView

enum class SocialPlatform(val allPageTypes: List<SocialPageType>) {
    FACEBOOK(allPageTypes = listOf(SocialPageType.BUSINESS, SocialPageType.PERSONAL)),
    INSTAGRAM(allPageTypes = listOf(SocialPageType.BUSINESS, SocialPageType.PERSONAL)),
    TWITTER(allPageTypes = listOf(SocialPageType.BUSINESS)),
    LINKEDIN(allPageTypes = listOf(SocialPageType.BUSINESS, SocialPageType.PERSONAL));

    fun toSocialPlatformForApi() = ApiSocialPlatform.fromSocialPlatform(this)

    fun toSocialPlatformForView() = SocialPlatformForView.fromSocialPlatform(this)
}

enum class SocialPageType {
    BUSINESS,
    PERSONAL;

    fun toSocialPageTypeForView() = SocialPageTypeForView.fromPageType(this)
}
