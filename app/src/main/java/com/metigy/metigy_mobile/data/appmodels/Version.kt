package com.metigy.metigy_mobile.data.appmodels

import kotlin.math.max

/**
 * Represents a compiled and comparable version
 */
data class Version(private val version: String?) : Comparable<Version> {
    init {
        requireNotNull(version) { "Version can not be null" }
        require(version.matches(Regex("[0-9]+(\\.[0-9]+)*"))) { "Invalid version format" }
    }

    override operator fun compareTo(other: Version): Int {
        val thisParts = this.version?.split(Regex("\\.")) ?: listOf()
        val thatParts = other.version?.split(Regex("\\.")) ?: listOf()
        val length = max(thisParts.size, thatParts.size)
        for (i in 0 until length) {
            val thisPart = if (i < thisParts.size) thisParts[i].toInt() else EQUAL
            val thatPart = if (i < thatParts.size) thatParts[i].toInt() else EQUAL
            if (thisPart < thatPart) return LESS
            if (thisPart > thatPart) return GREATER
        }
        return EQUAL
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null) return false
        return if (this.javaClass != other.javaClass) false else this.compareTo(other as Version) == 0
    }

    override fun hashCode(): Int {
        return version?.hashCode() ?: 0
    }

    override fun toString(): String = version.orEmpty()

    companion object {
        const val GREATER = 1
        const val EQUAL = 0
        const val LESS = -1
    }
}

