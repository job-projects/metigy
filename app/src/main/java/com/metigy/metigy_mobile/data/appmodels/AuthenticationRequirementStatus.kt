package com.metigy.metigy_mobile.data.appmodels

sealed class AuthenticationRequirementStatus {
    object AuthenticationNotNeeded : AuthenticationRequirementStatus()
    data class AuthenticationNeededCredentialsAvailable(val userCredentials: UserCredentialsModel) : AuthenticationRequirementStatus()
    object AuthenticationNeededCredentialsNotAvailable : AuthenticationRequirementStatus()
}