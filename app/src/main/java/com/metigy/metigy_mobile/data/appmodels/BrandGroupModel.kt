package com.metigy.metigy_mobile.data.appmodels

data class BrandGroupModel(
    val id: Int,
    val name: String,
    val url: String? = null,
)