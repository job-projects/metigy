package com.metigy.metigy_mobile.data.apiservice

import com.google.gson.JsonObject
import com.metigy.metigy_mobile.data.apimodels.*
import com.metigy.metigy_mobile.data.appconfigrepository.APP_CONFIG
import okhttp3.RequestBody
import retrofit2.http.*

interface ApiServiceForAppUrl {

    @POST(RepoUrl.LOGIN_URL)
    suspend fun login(@Body login: RequestBody): LoginResponse

    @POST(RepoUrl.LOGOUT_URL)
    suspend fun logout(@Header(ApiParamKey.AUTHORIZATION) authorization: String)

    @POST(RepoUrl.LIST_BRAND_GROUP_URL)
    suspend fun brandGroups(
        @Header(ApiParamKey.AUTHORIZATION) authorization: String,
        @Body brandGroups: RequestBody
    ): ListBrandGroup

    @POST(RepoUrl.LIST_CONTENT_URL)
    suspend fun contentList(@Header(ApiParamKey.AUTHORIZATION) authorization: String, @Body contentRequestBody: RequestBody): BrandContentsResponse

    @POST(RepoUrl.CONTENT_URL)
    suspend fun changeContentStatus(@Header(ApiParamKey.AUTHORIZATION) authorization: String, @Body contentRequestBody: RequestBody)

    @POST(RepoUrl.LIST_PLATFORM_PAGES_URL)
    suspend fun platformPages(@Header(ApiParamKey.AUTHORIZATION) authorization: String, @Body platformPagesRequestBody: RequestBody): PlatformPagesResponse

    @POST(RepoUrl.USER_INFO_URL)
    suspend fun userInfo(@Header(ApiParamKey.AUTHORIZATION) authorization: String, @Body userInfoRequestBody: RequestBody): UserInfoResponse

    @POST(RepoUrl.CONTENT_URL)
    suspend fun getDataForPost(@Header(ApiParamKey.AUTHORIZATION) authorization: String, @Body dataRequestBody: RequestBody): JsonObject

    @POST(RepoUrl.CONTENT_URL)
    suspend fun postContent(@Header(ApiParamKey.AUTHORIZATION) authorization: String, @Body dataRequestBody: RequestBody)

    @POST(RepoUrl.USER_INFO_URL)
    suspend fun signUp(@Body signUpBody: RequestBody): UserInfoResponse

    @POST(RepoUrl.DEVICE_URL)
    suspend fun sendDeviceInfo(@Header(ApiParamKey.AUTHORIZATION) authorization: String, @Body sendDeviceInfoBody: RequestBody)

    @POST(RepoUrl.USER_INFO_URL)
    suspend fun verifyEmail(@Header(ApiParamKey.AUTHORIZATION) authorization: String, @Body verifyEmailBody: RequestBody)

    @POST(RepoUrl.USER_INFO_URL)
    suspend fun resendEmail(@Header(ApiParamKey.AUTHORIZATION) authorization: String, @Body resendEmailBody: RequestBody)

    @POST(RepoUrl.LOGIN_URL)
    suspend fun resetPassword(@Body resetPasswordBody: RequestBody)

    //To change status after local sharing
    @POST(RepoUrl.CONTENT_URL)
    suspend fun changeStatusForLocalSharing(@Header(ApiParamKey.AUTHORIZATION) authorization: String, @Body changeStatusForLocalSharingBody: RequestBody)

    @GET
    suspend fun getAppConfig(@Url url: String = APP_CONFIG): AppConfigApiModel

    @POST(RepoUrl.LIST_PLATFORM_PAGES_URL)
    suspend fun addPage(@Header(ApiParamKey.AUTHORIZATION) authorization: String, @Body addPageBody: RequestBody)

    @POST(RepoUrl.BRAND)
    suspend fun getBrand(@Header(ApiParamKey.AUTHORIZATION) authorization: String, @Body getBrandBody: RequestBody): BrandsResponse

    @POST(RepoUrl.LIST_PLATFORM_PAGES_URL)
    suspend fun removePage(@Header(ApiParamKey.AUTHORIZATION) authorization: String, @Body removePageBody: RequestBody)
}