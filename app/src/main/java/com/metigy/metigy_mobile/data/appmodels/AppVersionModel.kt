package com.metigy.metigy_mobile.data.appmodels

data class AppVersionModel(val versionCode: Long, val versionName: Version)