package com.metigy.metigy_mobile.data.apimodels

import com.metigy.metigy_mobile.data.apiservice.SocialPlatformAndPageType
import com.metigy.metigy_mobile.data.appmodels.PlatformPageModel

data class PlatformPagesResponse(
    val ok: Boolean? = null,
    val result: List<PlatformPagesResult>? = null
)

data class PlatformPagesResult(
    val brand_id: Int? = null,
    val can_prepublish: Boolean? = null,
    val can_publish_activation: Boolean? = null,
    val can_publish_album: Boolean? = null,
    val can_publish_carousel: Boolean? = null,
    val can_publish_content: Boolean? = null,
    val can_publish_content_placeholder: Boolean? = null,
    val can_renew: Boolean? = null,
    val can_rich_text: Boolean? = null,
    val can_unpublish_content: Boolean? = null,
    val can_video_post: Boolean? = null,
    val created_at: Int? = null,
    val external_id: Any? = null,
    val has_ad_management: Boolean? = null,
    val has_section: Boolean? = null,
    val has_token: Boolean? = null,
    val id: Int? = null,
    val is_valid: Boolean? = null,
    val management_url: String? = null,
    val meta: Meta? = null,
    val name: String? = null,
    val require_connect: Boolean? = null,
    val stats_page_id: Any? = null,
    val subscribers: Int? = null,
    val timezone: Timezone? = null,
    val token_updated_at: Any? = null,
    val type: String? = null,
    val url: String? = null
) {
    fun toPlatformPage(): PlatformPageModel? {
        val platformAndType = type?.let {
            SocialPlatformAndPageType.fromApiName(it)
        }

        return if (id != null && platformAndType != null && brand_id != null) {
            PlatformPageModel(brandId = brand_id, id = id, socialPlatform = platformAndType.socialPlatform, socialPageType = platformAndType.socialPageType, name = name)
        } else {
            null
        }
    }
}

data class Meta(
    val account_type: String? = null,
    val business_id: String? = null,
    val description: String? = null,
    val image: String? = null,
    val name: String? = null,
    val url: String? = null,
    val username: String? = null
)

