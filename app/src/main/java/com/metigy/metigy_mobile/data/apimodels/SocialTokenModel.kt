package com.metigy.metigy_mobile.data.apimodels

import com.google.gson.annotations.SerializedName

data class SocialTokenRequestModel(
    val action: String? = null,
    val payload: SocialTokenPayload? = null,
    val platform: String? = null
)

data class SocialTokenPayload(
    val token: String? = null
)

//------------------------------------

data class SocialTokenResponseModel(
    @SerializedName("data")
    val resultData: SocialTokenResponseData? = null,
    val ok: Boolean? = null
)

data class SocialTokenResponseData(
    val code: String? = null,
    val domain: String? = null,
    val hasUser: Boolean? = null,
    val isDev: Boolean? = null,
    val isStaging: Boolean? = null,
    val missingScope: Boolean? = null,
    val success: Boolean? = null
)

