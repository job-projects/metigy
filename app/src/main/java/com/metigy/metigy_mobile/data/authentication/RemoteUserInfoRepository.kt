package com.metigy.metigy_mobile.data.authentication

import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.extension.toAppError
import com.metigy.metigy_mobile.core.extension.toRequestBody
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.data.apiservice.ApiAction
import com.metigy.metigy_mobile.data.apiservice.ApiParamKey
import com.metigy.metigy_mobile.data.apiservice.ApiServiceForAppUrl
import com.metigy.metigy_mobile.data.appmodels.UserInfoModel
import com.metigy.metigy_mobile.data.preferences.AppPreferences
import javax.inject.Inject

//TODO Remote and Local User Info Repositories should be handled in a better way

interface RemoteUserInfoRepository {
    suspend fun getUserInfo(token: String): Result<UserInfoModel>
}

class DefaultRemoteUserInfoRepository @Inject constructor(
    private val apiService: ApiServiceForAppUrl
) : RemoteUserInfoRepository {
    override suspend fun getUserInfo(token: String): Result<UserInfoModel> {
        return try {
            apiService.userInfo(authorization = token, userInfoRequestBody()).result?.toUserInfoModel()?.let { userInfo ->
                Result.Success(userInfo)
            } ?: run {
                Result.Error(messageId = R.string.user_not_found)
            }
        } catch (e: Exception) {
            e.toAppError()
        }
    }

    private fun userInfoRequestBody() =
        mapOf(
            ApiParamKey.ACTION to ApiAction.ME.action
        ).toRequestBody()
}

interface LocalUserInfoRepository {
    fun getUserInfo(): Result<UserInfoModel>
    fun saveUserInfo(userInfoModel: UserInfoModel)
}

class DefaultLocalUserInfoRepository @Inject constructor(private val preferences: AppPreferences) : LocalUserInfoRepository {
    override fun getUserInfo(): Result<UserInfoModel> {
        return preferences.getUserInfo()?.let { userInfo ->
            Result.Success(userInfo)
        } ?: run {
            Result.Error(messageId = R.string.user_not_found)
        }
    }

    override fun saveUserInfo(userInfoModel: UserInfoModel) {
        preferences.saveUserInfo(userInfoModel)
    }
}