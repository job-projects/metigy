package com.metigy.metigy_mobile.data.apimodels

data class Timezone(
    val code: String? = null,
    val label: String? = null,
    val offset: String? = null
)