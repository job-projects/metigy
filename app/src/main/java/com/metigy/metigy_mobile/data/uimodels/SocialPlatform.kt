package com.metigy.metigy_mobile.data.uimodels

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.data.appmodels.SocialPageType
import com.metigy.metigy_mobile.data.appmodels.SocialPlatform

enum class SocialPageTypeForView(val pageType: SocialPageType, val typeNameResId: Int, val typeNameForContentPreviewResId: Int, val postMethod: PostMethod) {
    PERSONAL(
        pageType = SocialPageType.PERSONAL,
        typeNameResId = R.string.personal,
        typeNameForContentPreviewResId = R.string.personal_page,
        postMethod = PostMethod(methodIconResId = R.drawable.ic_notifications_active, methodNameResId = R.string.notification_with_bullet)
    ),
    BUSINESS(
        pageType = SocialPageType.BUSINESS,
        typeNameResId = R.string.business,
        typeNameForContentPreviewResId = R.string.business_page,
        postMethod = PostMethod(methodIconResId = R.drawable.ic_auto_posting, methodNameResId = R.string.auto_with_bullet)
    );

    companion object {
        fun fromPageType(pageType: SocialPageType) = values().associateBy(SocialPageTypeForView::pageType)[pageType] ?: error("Illegal state")
    }
}

enum class SocialPlatformForView(val socialPlatform: SocialPlatform, @DrawableRes val coloredIconId: Int, @StringRes val platformNameResId: Int) {
    FACEBOOK(socialPlatform = SocialPlatform.FACEBOOK, coloredIconId = R.drawable.ic_facebook, platformNameResId = R.string.facebook),
    INSTAGRAM(socialPlatform = SocialPlatform.INSTAGRAM, coloredIconId = R.drawable.ic_instagram, platformNameResId = R.string.instagram),
    TWITTER(socialPlatform = SocialPlatform.TWITTER, coloredIconId = R.drawable.ic_twitter, platformNameResId = R.string.twitter),
    LINKEDIN(socialPlatform = SocialPlatform.LINKEDIN, coloredIconId = R.drawable.ic_linkedin, platformNameResId = R.string.linkedin);

    companion object {
        fun fromSocialPlatform(socialPlatform: SocialPlatform) = values().associateBy(SocialPlatformForView::socialPlatform)[socialPlatform] ?: error("Illegal state")
    }
}

data class PostMethod(val methodIconResId: Int, val methodNameResId: Int)
