package com.metigy.metigy_mobile.data.preferences

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.metigy.metigy_mobile.core.extension.editAndApply
import com.metigy.metigy_mobile.core.extension.inSecondsToInstant
import com.metigy.metigy_mobile.data.appmodels.UserCredentialsModel
import com.metigy.metigy_mobile.data.appmodels.UserInfoModel
import dagger.hilt.android.qualifiers.ApplicationContext
import java.time.Instant
import javax.inject.Inject

interface AppPreferences {
    fun saveUserToken(token: String)
    fun getUserToken(): String?
    fun clearToken()

    fun saveLastAuthenticationDate(instant: Instant)
    fun getLastAuthenticationDate(): Instant?
    fun clearLastAuthenticationDate()

    fun saveUserCredential(userCredentials: UserCredentialsModel)
    fun getUserCredentials(): UserCredentialsModel?
    fun clearCredentials()

    fun saveUserInfo(userInfo: UserInfoModel)
    fun getUserInfo(): UserInfoModel?
    fun clearUserInfo()


    fun savePushChannelId(pushToken: String)
    fun getPushChannelId(): String?

    fun setPushInfoSent(isSent: Boolean)
    fun isPushInfoSent(): Boolean

    fun setLatestPromptDate(instant: Instant)
    fun getLatestPromptDate(): Instant?
    fun clearLatestPromptDate()

    fun getIsPreviouslyLoggedIn(): Boolean
    fun setIsPreviouslyLoggedIn()
}

//TODO Needs a review about the way of using preferences. The code could be cleaner
class SharedPreferencesHelper @Inject constructor(@ApplicationContext private val context: Context) : AppPreferences {

    private val prefs: SharedPreferences
        get() = context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE)

    override fun saveUserToken(token: String) {
        prefs.editAndApply {
            putString(PREFS_KEY_TOKEN, token)
        }
    }

    //If returns null, means that user is logged out
    override fun getUserToken(): String? = prefs.getString(PREFS_KEY_TOKEN, null)

    override fun clearToken() {
        prefs.editAndApply {
            remove(PREFS_KEY_TOKEN)
        }
    }

    override fun saveLastAuthenticationDate(instant: Instant) {
        prefs.editAndApply {
            putString(PREFS_KEY_LAST_LOGIN_DATE, instant.epochSecond.toString())
        }
    }

    override fun getLastAuthenticationDate(): Instant? = prefs.getString(PREFS_KEY_LAST_LOGIN_DATE, null)?.toLongOrNull()?.inSecondsToInstant()

    override fun clearLastAuthenticationDate() {
        prefs.editAndApply {
            remove(PREFS_KEY_LAST_LOGIN_DATE)
        }
    }

    override fun saveUserCredential(userCredentials: UserCredentialsModel) {
        val json = Gson().toJson(userCredentials)
        prefs.editAndApply {
            putString(PREFS_KEY_USER_CREDENTIALS, json)
        }
    }

    override fun getUserCredentials(): UserCredentialsModel? = prefs.getString(PREFS_KEY_USER_CREDENTIALS, null)?.let { json ->
        Gson().fromJson(json, UserCredentialsModel::class.java)
    }

    override fun clearCredentials() {
        prefs.editAndApply {
            remove(PREFS_KEY_USER_CREDENTIALS)
        }
    }

    override fun saveUserInfo(userInfo: UserInfoModel) {
        val json = Gson().toJson(userInfo)
        prefs.editAndApply {
            putString(PREFS_KEY_USER_INFO, json)
        }
    }

    override fun getUserInfo(): UserInfoModel? = prefs.getString(PREFS_KEY_USER_INFO, null)?.let { json ->
        Gson().fromJson(json, UserInfoModel::class.java)
    }

    override fun clearUserInfo() {
        prefs.editAndApply {
            remove(PREFS_KEY_USER_INFO)
        }
    }

    override fun savePushChannelId(pushToken: String) {
        prefs.editAndApply {
            putString(PREFS_KEY_PUSH_CHANNEL_ID, pushToken)
        }
    }

    override fun getPushChannelId(): String? = prefs.getString(PREFS_KEY_PUSH_CHANNEL_ID, null)

    override fun setPushInfoSent(isSent: Boolean) {
        prefs.editAndApply {
            putBoolean(PREFS_KEY_PUSH_INFO_SENT, isSent)
        }
    }

    override fun isPushInfoSent(): Boolean = prefs.getBoolean(PREFS_KEY_PUSH_INFO_SENT, false)

    override fun setLatestPromptDate(instant: Instant) {
        prefs.editAndApply {
            putString(PREFS_KEY_LATEST_PROMPT_DATE, instant.epochSecond.toString())
        }
    }

    override fun getLatestPromptDate(): Instant? = prefs.getString(PREFS_KEY_LATEST_PROMPT_DATE, null)?.toLongOrNull()?.inSecondsToInstant()

    override fun clearLatestPromptDate() {
        prefs.editAndApply {
            remove(PREFS_KEY_LATEST_PROMPT_DATE)
        }
    }

    override fun getIsPreviouslyLoggedIn(): Boolean = prefs.getBoolean(PREFS_KEY_IS_PREVIOUSLY_LOGGED_IN, false)

    override fun setIsPreviouslyLoggedIn() {
        prefs.editAndApply { putBoolean(PREFS_KEY_IS_PREVIOUSLY_LOGGED_IN, true) }
    }

    companion object {
        const val SHARED_PREFS_NAME = "AppPreferences"
        const val PREFS_KEY_TOKEN = "Token"
        const val PREFS_KEY_USER_CREDENTIALS = "UserCredentials"
        const val PREFS_KEY_USER_INFO = "UserInfo"
        const val PREFS_KEY_TEMP_SIGN_UP_CREDENTIALS = "TempSignUpCredentials"
        const val PREFS_KEY_PUSH_CHANNEL_ID = "PushChannelId"
        const val PREFS_KEY_PUSH_INFO_SENT = "PushInfoSent"
        const val PREFS_KEY_LAST_LOGIN_DATE = "LastLoginDate"
        const val PREFS_KEY_LATEST_PROMPT_DATE = "LatestPromptDate"
        const val PREFS_KEY_IS_PREVIOUSLY_LOGGED_IN = "IsPreviouslyLoggedIn"
    }
}