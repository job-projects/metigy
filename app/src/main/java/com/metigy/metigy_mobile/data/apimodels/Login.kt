package com.metigy.metigy_mobile.data.apimodels

data class ErrorResponse(
    val error: String? = null,
    val ok: Boolean? = null
)

data class LoginResponse(
    val ok: Boolean? = null,
    val result: LoginResult? = null
)

data class LoginResult(
    val name: String? = null,
    val scj: String? = null
)