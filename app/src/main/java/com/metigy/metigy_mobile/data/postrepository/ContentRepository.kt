package com.metigy.metigy_mobile.data.postrepository

import com.google.gson.JsonObject
import com.metigy.metigy_mobile.core.consts.TAG_CHANGE_STATUS
import com.metigy.metigy_mobile.core.extension.isSameDayAtDeviceZone
import com.metigy.metigy_mobile.core.extension.toAppError
import com.metigy.metigy_mobile.core.extension.toRequestBody
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.core.utils.TimeProvider
import com.metigy.metigy_mobile.core.utils.TimezoneProvider
import com.metigy.metigy_mobile.data.apiservice.*
import com.metigy.metigy_mobile.data.appmodels.BrandContentModel
import com.metigy.metigy_mobile.data.preferences.AppPreferences
import okhttp3.RequestBody
import timber.log.Timber
import java.time.Instant
import javax.inject.Inject

interface ContentRepository {
    suspend fun getBrandPosts(brandGroupId: Int, forDate: Instant): Result<List<BrandContentModel>>
    suspend fun declineContent(contentId: Int, brandGroupId: Int): Result<Unit>
    suspend fun approveContent(contentId: Int, brandGroupId: Int): Result<Unit>
    suspend fun postContent(brandGroupId: Int, contentId: Int): Result<Unit>
    suspend fun changeStatusForLocalSharing(brandGroupId: Int, contentId: Int, pageId: Int)
}

class DefaultContentRepository @Inject constructor(
    private val apiService: ApiServiceForAppUrl,
    private val preferences: AppPreferences,
    private val timeProvider: TimeProvider,
    private val timezoneProvider: TimezoneProvider
) : ContentRepository {

    private val token: String
        get() = preferences.getUserToken().orEmpty()

    override suspend fun getBrandPosts(brandGroupId: Int, forDate: Instant): Result<List<BrandContentModel>> {
        val body = brandContentsBody(brandGroupId, forDate)
        return try {
            val result = apiService.contentList(authorization = token, contentRequestBody = body).result?.values?.mapNotNull { value ->
                value.toBrandContentModel()
            }?.filter {
                it.publishTime.isSameDayAtDeviceZone(forDate, timezoneProvider)
            }.orEmpty()

            Result.Success(result)
        } catch (e: Exception) {
            e.toAppError()
        }
    }

    override suspend fun declineContent(contentId: Int, brandGroupId: Int): Result<Unit> {
        val body = changeContentStatusBody(brandGroupId, contentId, ApiContentStatus.DECLINE.statusId)
        return changeContentStatus(authorization = token, contentRequestBody = body)
    }

    override suspend fun approveContent(contentId: Int, brandGroupId: Int): Result<Unit> {
        val body = changeContentStatusBody(brandGroupId, contentId, ApiContentStatus.APPROVED.statusId)
        return changeContentStatus(authorization = token, contentRequestBody = body)
    }

    private suspend fun changeContentStatus(authorization: String, contentRequestBody: RequestBody): Result<Unit> {
        return try {
            apiService.changeContentStatus(authorization = authorization, contentRequestBody = contentRequestBody)
            Result.Success(Unit)
        } catch (e: Exception) {
            e.toAppError()
        }
    }

    override suspend fun postContent(brandGroupId: Int, contentId: Int): Result<Unit> =
        when (val updatedDataJsonObjectResult = getAndUpdateDataForPost(brandGroupId, contentId)) {
            is Result.Success -> {
                try {
                    Result.Success(apiService.postContent(authorization = token, getPostContentBody(brandGroupId, updatedDataJsonObjectResult.data)))
                } catch (e: Exception) {
                    e.toAppError()
                }
            }
            is Result.Error -> {
                updatedDataJsonObjectResult
            }
        }

    override suspend fun changeStatusForLocalSharing(brandGroupId: Int, contentId: Int, pageId: Int) {
        try {
            apiService.changeStatusForLocalSharing(
                authorization = token, changeStatusForLocalSharingBody = getChangeLocalSharingBody(
                    brandGroupId = brandGroupId,
                    contentId = contentId,
                    pageId = pageId,
                    processedAt = timeProvider.now()
                )
            )
            Timber.tag(TAG_CHANGE_STATUS).d("Change status for local sharing is done!")
        } catch (e: Exception) {
            Timber.tag(TAG_CHANGE_STATUS).e(e)
        }
    }

    private suspend fun getAndUpdateDataForPost(brandGroupId: Int, contentId: Int): Result<JsonObject> =
        try {
            val rootJsonObject = apiService.getDataForPost(authorization = token, dataRequestBody = getDataForPostStatusBody(brandGroupId, contentId))
            Result.Success(buildNewDataJson(rootJsonObject))
        } catch (e: Exception) {
            e.toAppError()
        }

    private fun buildNewDataJson(rootJsonObject: JsonObject): JsonObject {
        val resultFirstJsonObject = rootJsonObject.getAsJsonArray(DataForPostKey.RESULT).get(0).asJsonObject
        val pagesJsonObject = resultFirstJsonObject.getAsJsonObject(DataForPostKey.PAGES)
        pagesJsonObject.keySet().forEach { key ->
            pagesJsonObject.add(key, null)
        }
        resultFirstJsonObject.addProperty(DataForPostKey.SCHEDULED_FOR, timeProvider.nowAsEpochSecond())

        val typeId = resultFirstJsonObject.getAsJsonObject(DataForPostKey.TYPE).get(DataForPostKey.ID).asBigInteger
        resultFirstJsonObject.addProperty(DataForPostKey.TYPE_ID, typeId)
        return resultFirstJsonObject
    }

    private fun brandContentsBody(brandGroupId: Int, since: Instant) = mapOf(
        ApiParamKey.ACTION to ApiAction.LIST.action,
        ApiParamKey.CONTEXT_BRAND_GROUP_ID to brandGroupId.toString(),
        ApiParamKey.SINCE to since.epochSecond.toString(),
        ApiParamKey.LIMIT to 9000.toString(), //FIXME
        ApiParamKey.STATUS to ApiContentStatus.values().joinToString { status ->
            status.statusName
        },
        ApiParamKey.FIELDS to API_CONTENT_LIST_FIELDS
    ).toRequestBody()

    private fun changeContentStatusBody(brandGroupId: Int, contentId: Int, newStatusId: Int) = mapOf(
        ApiParamKey.ACTION to ApiAction.CHANGE_STATUS.action,
        ApiParamKey.CONTEXT_BRAND_GROUP_ID to brandGroupId.toString(),
        ApiParamKey.ID to contentId.toString(),
        ApiParamKey.STATUS_ID to newStatusId.toString()
    ).toRequestBody()

    private fun getDataForPostStatusBody(brandGroupId: Int, contentId: Int) = mapOf(
        ApiParamKey.ACTION to ApiAction.GET.action,
        ApiParamKey.CONTEXT_BRAND_GROUP_ID to brandGroupId.toString(),
        ApiParamKey.ID to contentId.toString()
    ).toRequestBody()

    private fun getPostContentBody(brandGroupId: Int, data: JsonObject) = mapOf(
        ApiParamKey.ACTION to ApiAction.SAVE_V2.action,
        ApiParamKey.CONTEXT_BRAND_GROUP_ID to brandGroupId.toString(),
        ApiParamKey.IS_PUBLISH_NOW to true.toString(),
        ApiParamKey.DATA to data.toString()
    ).toRequestBody()

    private fun getChangeLocalSharingBody(brandGroupId: Int, contentId: Int, pageId: Int, processedAt: Instant) = mapOf(
        ApiParamKey.ACTION to ApiAction.CHANGE_PAGE_CONTENT_STATUS.action,
        ApiParamKey.CONTEXT_BRAND_GROUP_ID to brandGroupId.toString(),
        ApiParamKey.CONTENT_ID to contentId.toString(),
        ApiParamKey.LOCATION_ID to pageId.toString(),
        ApiParamKey.PROCESSED_AT to processedAt.epochSecond.toString(),
        ApiParamKey.STATUS to ApiContentStatus.SUCCEEDED.statusName
    ).toRequestBody()
}