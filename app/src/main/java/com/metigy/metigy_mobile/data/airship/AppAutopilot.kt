package com.metigy.metigy_mobile.data.airship

import android.content.Context
import com.metigy.metigy_mobile.core.consts.TAG_DEBUG
import com.metigy.metigy_mobile.core.consts.TAG_PUSH_NOTIFICATION
import com.metigy.metigy_mobile.data.preferences.AppPreferences
import com.urbanairship.AirshipConfigOptions
import com.urbanairship.Autopilot
import com.urbanairship.UAirship
import com.urbanairship.channel.AirshipChannelListener
import com.urbanairship.push.NotificationActionButtonInfo
import com.urbanairship.push.NotificationInfo
import com.urbanairship.push.NotificationListener
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.android.EntryPointAccessors
import dagger.hilt.components.SingletonComponent
import timber.log.Timber

class AppAutopilot : Autopilot() {

    lateinit var preferences: AppPreferences
    lateinit var pushNotification: AppPushNotification
    lateinit var notificationHandler: NotificationHandler

    override fun onAirshipReady(airship: UAirship) {
        super.onAirshipReady(airship)
        val autoPilotEntryPoint = EntryPointAccessors.fromApplication(UAirship.getApplicationContext(), AppAutoPilotEntryPoint::class.java)
        airship.pushManager.userNotificationsEnabled = true

        preferences = autoPilotEntryPoint.preferences()
        pushNotification = autoPilotEntryPoint.pushNotification()
        notificationHandler = autoPilotEntryPoint.notificationHandler()

        airship.channel.addChannelListener(object : AirshipChannelListener {
            override fun onChannelCreated(channelId: String) {
                Timber.tag(TAG_PUSH_NOTIFICATION).d("Channel id: $channelId")
                preferences.savePushChannelId(channelId)
                pushNotification.trySendingDeviceInfoToServer()
            }

            override fun onChannelUpdated(channelId: String) {
                Timber.tag(TAG_PUSH_NOTIFICATION).d("Channel id updated to: $channelId")
                preferences.savePushChannelId(channelId)
                pushNotification.trySendingDeviceInfoToServer()
            }
        })

        airship.pushManager.notificationListener =
            object : NotificationListener {
                override fun onNotificationPosted(notificationInfo: NotificationInfo) {
                    Timber.tag(TAG_PUSH_NOTIFICATION).d("Push notification just received:\t${notificationInfo.message}")
                }

                override fun onNotificationOpened(notificationInfo: NotificationInfo): Boolean {
                    notificationInfo.message.getExtra(PUSH_KEY_BRAND_GROUP_ID)?.toIntOrNull()?.let { brandGroupId ->
                        notificationHandler.handleNotification(NotificationContent(brandGroupId = brandGroupId))
                    }
                    Timber.tag(TAG_PUSH_NOTIFICATION).d("Notification opened")

                    return true
                }

                override fun onNotificationForegroundAction(notificationInfo: NotificationInfo, actionButtonInfo: NotificationActionButtonInfo): Boolean {

                    return true
                }

                override fun onNotificationBackgroundAction(notificationInfo: NotificationInfo, actionButtonInfo: NotificationActionButtonInfo) {

                }

                override fun onNotificationDismissed(notificationInfo: NotificationInfo) {

                }
            }

    }

    override fun createAirshipConfigOptions(context: Context): AirshipConfigOptions? {
        Timber.tag(TAG_DEBUG).d("createAirshipConfigOptions")

        return super.createAirshipConfigOptions(context)
    }

    override fun allowEarlyTakeOff(context: Context): Boolean {
        Timber.tag(TAG_DEBUG).d("allowEarlyTakeOff")

        return super.allowEarlyTakeOff(context)
    }

    override fun isReady(context: Context): Boolean {
        Timber.tag(TAG_DEBUG).d("isReady")
        return super.isReady(context)
    }

    @EntryPoint
    @InstallIn(SingletonComponent::class)
    interface AppAutoPilotEntryPoint {
        fun preferences(): AppPreferences
        fun pushNotification(): AppPushNotification
        fun notificationHandler(): NotificationHandler
    }

    companion object {
        const val PUSH_KEY_BRAND_GROUP_ID = "brandGroupId"
    }
}