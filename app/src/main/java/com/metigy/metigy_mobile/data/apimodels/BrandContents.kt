package com.metigy.metigy_mobile.data.apimodels

import com.metigy.metigy_mobile.core.extension.inSecondsToInstant
import com.metigy.metigy_mobile.data.apiservice.ApiContentStatus
import com.metigy.metigy_mobile.data.apiservice.SocialPlatformAndPageType
import com.metigy.metigy_mobile.data.appmodels.BrandContentModel

data class BrandContentsResponse(
    val ok: Boolean? = null,
    val result: BrandContentsResult? = null
)

data class BrandContentsResult(
    val total: Int? = null,
    val values: List<Value>? = null
)

data class Value(
    val ads_summary: Any? = null,
    val brand_group_id: Int? = null,
    val brand_ids: List<Int>? = null,
    val categories: List<Any>? = null,
    val content_type: ContentType? = null,
    val created_user: String? = null,
    val created_user_id: Int? = null,
    val end_time: Any? = null,
    val id: Int? = null,
    val image: Image? = null,
    val is_optin: Int? = null,
    val link_url: Any? = null,
    val message: String? = null,
    val optin_status: Any? = null,
    val page_content_external_ids: Any? = null,
    val page_ids: List<Int>? = null,
    val page_types: List<String>? = null,
    val publish_time: Long? = null,
    val status_id: Int? = null,
    val title: Any? = null,
    val type: String? = null,
    val updated_user: String? = null,
    val updated_user_id: Int? = null
) {
    fun toBrandContentModel(): BrandContentModel? {
        val pageTypeList = page_types?.mapNotNull { pageType ->
            SocialPlatformAndPageType.fromApiName(pageType)
        }

        val contentStatus = status_id?.let {
            ApiContentStatus.fromId(it)
        }

        return if (id != null && pageTypeList != null && pageTypeList.isNotEmpty() && publish_time != null && contentStatus != null && brand_group_id != null) {
            BrandContentModel(
                id = id,
                publishTime = publish_time.inSecondsToInstant(),
                createdUser = created_user.orEmpty(),
                image = com.metigy.metigy_mobile.data.appmodels.Image(
                    url = image?.url
                ),
                message = message,
                contentStatus = contentStatus,
                pageInfoList = listOf(),
                pageIds = page_ids ?: listOf(),
                brandGroupId = brand_group_id
            )
        } else {
            null
        }
    }
}

data class ContentType(
    val id: Int? = null,
    val name: String? = null,
    val slug: String? = null
)

data class Image(
    val id: String? = null,
    val thumbUrl: String? = null,
    val url: String? = null
)