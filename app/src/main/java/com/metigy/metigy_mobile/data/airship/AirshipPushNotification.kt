package com.metigy.metigy_mobile.data.airship

import com.metigy.metigy_mobile.core.consts.TAG_PUSH_NOTIFICATION
import com.metigy.metigy_mobile.core.extension.toRequestBody
import com.metigy.metigy_mobile.core.utils.HostInfoUtils
import com.metigy.metigy_mobile.data.apiservice.ANDROID
import com.metigy.metigy_mobile.data.apiservice.ApiAction
import com.metigy.metigy_mobile.data.apiservice.ApiParamKey
import com.metigy.metigy_mobile.data.apiservice.ApiServiceForAppUrl
import com.metigy.metigy_mobile.data.preferences.AppPreferences
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

interface AppPushNotification {
    fun trySendingDeviceInfoToServer()
}

class AirshipPushNotification @Inject constructor(
    private val apiService: ApiServiceForAppUrl,
    private val preferences: AppPreferences,
    private val hostInfoUtils: HostInfoUtils,
) : AppPushNotification {
    override fun trySendingDeviceInfoToServer() {
        val userToken = preferences.getUserToken()
        val pushToken = preferences.getPushChannelId()
        val pushHasAlreadyBeenSent = preferences.isPushInfoSent()
        if (pushHasAlreadyBeenSent.not() && userToken != null && pushToken != null) {
            GlobalScope.launch {
                try {
                    apiService.sendDeviceInfo(authorization = userToken, sendDeviceInfoBody = sendDeviceInfoRequestBody(hostInfoUtils.getUniqueId(), pushToken))
                    preferences.setPushInfoSent(true)
                    Timber.tag(TAG_PUSH_NOTIFICATION).d("Device info sent successfully. Push Channel id: $pushToken")
                } catch (e: Exception) {
                    Timber.tag(TAG_PUSH_NOTIFICATION).e(e, "Failed to send device info\t. Push Channel id: $pushToken")
                }
            }
        } else {
            Timber.tag(TAG_PUSH_NOTIFICATION)
                .d("Device info not sent.\tUser token is $userToken\t Push channel id is $pushToken. Device info had${if (pushHasAlreadyBeenSent.not()) " not" else ""} been sent")
        }
    }

    private fun sendDeviceInfoRequestBody(uuid: String, pushToken: String) = mapOf(
        ApiParamKey.ACTION to ApiAction.ADD_PUSH_TOKEN.action,
        ApiParamKey.ID to uuid,
        ApiParamKey.TOKEN to pushToken,
        ApiParamKey.SYSTEM_TYPE to ANDROID
    ).toRequestBody()
}