package com.metigy.metigy_mobile.data.airship

import android.content.Context
import android.content.Intent
import android.os.Parcelable
import com.metigy.metigy_mobile.core.extension.addFlagsToClearBackstack
import com.metigy.metigy_mobile.ui.launcher.LauncherActivity
import com.metigy.metigy_mobile.ui.main.MainActivity
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.parcelize.Parcelize
import javax.inject.Inject

class NotificationHandler @Inject constructor(
    @ApplicationContext private val context: Context
) {
    fun handleNotification(notificationContent: NotificationContent) {
        val intent = Intent(context, LauncherActivity::class.java).apply {
            addFlagsToClearBackstack()
            putExtra(LauncherActivity.BUNDLE_KEY_PUSH_NOTIFICATION_CONTENT, notificationContent)
            putExtra(LauncherActivity.BUNDLE_KEY_PUSH_NOTIFICATION_APP_RUNNING, MainActivity.isRunning)
        }
        context.startActivity(intent)
    }
}

@Parcelize
data class NotificationContent(
    val brandGroupId: Int
) : Parcelable