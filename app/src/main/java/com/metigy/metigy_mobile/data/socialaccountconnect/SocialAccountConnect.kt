package com.metigy.metigy_mobile.data.socialaccountconnect

import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.extension.toAppError
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.data.apimodels.PageListPayload
import com.metigy.metigy_mobile.data.apimodels.SocialPageListRequestModel
import com.metigy.metigy_mobile.data.apimodels.SocialTokenPayload
import com.metigy.metigy_mobile.data.apimodels.SocialTokenRequestModel
import com.metigy.metigy_mobile.data.apiservice.ApiAction
import com.metigy.metigy_mobile.data.apiservice.ApiServiceForServiceUrl
import com.metigy.metigy_mobile.data.appmodels.SocialPageInfoModel
import com.metigy.metigy_mobile.data.appmodels.SocialPlatform
import com.metigy.metigy_mobile.data.preferences.AppPreferences
import javax.inject.Inject

interface SocialAccountConnect {
    suspend fun sendSocialTokenToServer(socialToken: String, socialPlatform: SocialPlatform): Result<String>
    suspend fun getPages(uuid: String, socialPlatform: SocialPlatform): Result<List<SocialPageInfoModel>>
}

class DefaultSocialAccountConnect @Inject constructor(
    private val apiService: ApiServiceForServiceUrl,
    private val preferences: AppPreferences
) : SocialAccountConnect {

    val authToken: String
        get() = preferences.getUserToken().orEmpty()

    override suspend fun sendSocialTokenToServer(socialToken: String, socialPlatform: SocialPlatform): Result<String> {
        val requestModel = SocialTokenRequestModel(
            action = ApiAction.CALLBACK.action,
            platform = socialPlatform.toSocialPlatformForApi().platformName,
            payload = SocialTokenPayload(
                token = socialToken
            )
        )

        return try {
            apiService.sendSocialToken(authorization = authToken, requestModel = requestModel).resultData?.code?.let { uuid ->
                Result.Success(uuid)
            } ?: Result.Error(messageId = R.string.unknown_error)
        } catch (e: Exception) {
            e.toAppError()
        }
    }

    override suspend fun getPages(uuid: String, socialPlatform: SocialPlatform): Result<List<SocialPageInfoModel>> {
        val requestModel = SocialPageListRequestModel(
            action = ApiAction.PAGE_LIST.action,
            payload = PageListPayload(
                authUuid = uuid
            ),
            platform = socialPlatform.toSocialPlatformForApi().platformName
        )
        return try {
            val pageList = apiService.listSocialPages(authorization = authToken, requestModel = requestModel)
            Result.Success(
                pageList.socialPageListResult?.mapNotNull { socialPage ->
                    socialPage.toSocialPageInfoModel()
                } ?: listOf()
            )
        } catch (e: Exception) {
            e.toAppError()
        }

    }
}