package com.metigy.metigy_mobile.data.postrepository

import com.metigy.metigy_mobile.ContextProvider
import com.metigy.metigy_mobile.core.extension.asImageUrlToBitmap
import com.metigy.metigy_mobile.core.extension.saveToFile
import com.metigy.metigy_mobile.data.file.FileProvider
import java.io.File
import javax.inject.Inject

interface ContentPersistence {
    suspend fun saveContentImage(imageUrl: String, contentId: Int): File?
}

class DefaultContentPersistence @Inject constructor(
    private val contextProvider: ContextProvider,
    private val fileProvider: FileProvider
) : ContentPersistence {
    override suspend fun saveContentImage(imageUrl: String, contentId: Int): File? {
        val bitmap = imageUrl.asImageUrlToBitmap(contextProvider.context())
        val contentImageRelativeFolder = fileProvider.contentImageRelativeFolderAlsoMake()
        val savingSuccessful = bitmap?.saveToFile(context = contextProvider.context(), relativeFile = contentImageRelativeFolder, contentId.toString())
        return if (savingSuccessful == true) fileProvider.getContentImageAbsoluteFile(contentId) else null
    }
}