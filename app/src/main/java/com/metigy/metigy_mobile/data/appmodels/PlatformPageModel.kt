package com.metigy.metigy_mobile.data.appmodels

data class PlatformPageModel(
    val brandId: Int,
    val id: Int,
    val name: String? = null,
    val socialPlatform: SocialPlatform,
    val socialPageType: SocialPageType
)