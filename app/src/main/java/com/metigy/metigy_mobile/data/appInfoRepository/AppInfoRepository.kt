package com.metigy.metigy_mobile.data.appInfoRepository

import com.metigy.metigy_mobile.core.utils.HostInfoUtils
import com.metigy.metigy_mobile.data.appmodels.AppVersionModel
import com.metigy.metigy_mobile.ui.contentlist.SmAppAndInfo
import javax.inject.Inject

//TODO Move it to a better place
const val ABOUT_APP_VIDEO_URL = "https://dz7d6n0c3k1px.cloudfront.net/MetigySignupIntro.mp4"

interface AppInfoRepository {
    fun appVersion(): AppVersionModel
    fun isAppInstalled(app: SmAppAndInfo): Boolean
    val helpUrl: String
    val contactEmailUrl: String
    val learnMoreUrlForFacebookLogin: String
    val learnMoreUrlForInstagramContentPublishing: String
}

class DefaultAppInfoRepository @Inject constructor(private val hostInfoUtils: HostInfoUtils) : AppInfoRepository {
    override fun appVersion(): AppVersionModel = hostInfoUtils.appVersion()

    override fun isAppInstalled(app: SmAppAndInfo): Boolean = hostInfoUtils.isAppInstalled(app)

    override val helpUrl: String
        get() = HELP_URL

    override val contactEmailUrl: String
        get() = CONTACT_EMAIL_URL

    override val learnMoreUrlForFacebookLogin: String
        get() = LEARN_MORE_URL_FOR_FACEBOOK_LOGIN

    override val learnMoreUrlForInstagramContentPublishing: String
        get() = LEARN_MORE_URL_FOR_INSTAGRAM_CONTENT_PUBLISHING

    companion object {
        private const val HELP_URL = "https://metigy.com/support-articles/faqs-tips/faqs/"
        private const val CONTACT_EMAIL_URL = "mobile.product@metigy.com"
        private const val LEARN_MORE_URL_FOR_FACEBOOK_LOGIN = "https://metigy.com/support/why-do-i-have-to-login-to-facebook-to-connect-my-instagram-account/"
        private const val LEARN_MORE_URL_FOR_INSTAGRAM_CONTENT_PUBLISHING = "https://metigy.com/support/how-does-instagram-content-publishing-work-with-metigy/"
    }
}