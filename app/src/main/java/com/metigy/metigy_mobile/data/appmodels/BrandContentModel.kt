package com.metigy.metigy_mobile.data.appmodels

import android.os.Parcelable
import com.metigy.metigy_mobile.core.utils.TimeProvider
import com.metigy.metigy_mobile.data.apiservice.ApiContentStatus
import kotlinx.parcelize.Parcelize
import java.time.Instant
import java.time.temporal.ChronoUnit

@Parcelize
data class BrandContentModel(
    val brandGroupId: Int,
    val createdUser: String,
    val id: Int,
    val image: Image?,
    val message: String?,
    val pageIds: List<Int>,
    val pageInfoList: List<PageInfo>,
    val publishTime: Instant,
    val contentStatus: ApiContentStatus //TODO Needs refactor. Use the Business Model instead.
) : Parcelable {
    fun isPendingApproval() =
        contentStatus == ApiContentStatus.PENDING || contentStatus == ApiContentStatus.INTERNAL_PENDING || contentStatus == ApiContentStatus.DRAFT

    fun isApproved() = contentStatus == ApiContentStatus.APPROVED

    fun pageCount() = pageInfoList.size

    fun isNearPostTime(timeProvider: TimeProvider): Boolean =
        publishTime.minus(30, ChronoUnit.MINUTES) <= timeProvider.now() && publishTime > timeProvider.now()

    fun missedScheduledDate(timeProvider: TimeProvider): Boolean = timeProvider.now() > publishTime

    fun imageUrl() = image?.url
}

@Parcelize
data class Image(
    val id: String? = null,
    val thumbUrl: String? = null,
    val url: String? = null
) : Parcelable

@Parcelize
data class PageInfo(
    val id: Int,
    val socialPlatform: SocialPlatform,
    val socialPageType: SocialPageType,
    var pageName: String?
) : Parcelable

enum class ContentStatus {
    SCHEDULED,
    POSTED,
    DRAFT;

    fun toApiContentStatusList(status: ContentStatus) = ApiContentStatus.fromBusinessContentStatus(status)
}
