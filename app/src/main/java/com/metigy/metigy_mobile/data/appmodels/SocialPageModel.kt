package com.metigy.metigy_mobile.data.appmodels

data class SocialPageInfoModel(
    val externalId: String,
    val imageUrl: String,
    val name: String,
    val socialPlatform: SocialPlatform,
    val socialPageType: SocialPageType
)