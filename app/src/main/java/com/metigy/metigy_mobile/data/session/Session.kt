package com.metigy.metigy_mobile.data.session

import com.metigy.metigy_mobile.data.airship.AppPushNotification
import com.metigy.metigy_mobile.data.preferences.AppPreferences
import javax.inject.Inject

class Session @Inject constructor(
    private val appPushNotification: AppPushNotification,
    private val preferences: AppPreferences
) {
    fun onLogin() {
        preferences.setPushInfoSent(false)
        appPushNotification.trySendingDeviceInfoToServer()
    }
}
