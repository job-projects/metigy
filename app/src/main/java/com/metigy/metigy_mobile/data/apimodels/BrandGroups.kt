package com.metigy.metigy_mobile.data.apimodels

import com.metigy.metigy_mobile.data.appmodels.BrandGroupModel

data class ListBrandGroup(
    val ok: Boolean? = null,
    val result: List<BrandGroup>? = null
)

data class BrandGroup(
    val SSL: String? = null,
    val clientId: Int? = null,
    val id: Int? = null,
    val isMasterGroup: Boolean? = null,
    val name: String? = null,
    val slug: String? = null,
    val url: String? = null,
    val uuid: String? = null
) {
    fun toBrandGroupModel(): BrandGroupModel? =
        if (id != null) {
            BrandGroupModel(
                id = id,
                name = name.orEmpty(),
                url = url
            )
        } else {
            null
        }
}