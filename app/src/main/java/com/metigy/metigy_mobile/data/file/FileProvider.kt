package com.metigy.metigy_mobile.data.file

import android.content.Context
import android.os.Environment
import dagger.hilt.android.qualifiers.ApplicationContext
import java.io.File
import javax.inject.Inject

const val FOLDER_APP_ROOT = "Metigy"
const val FOLDER_CONTENT = "Content"
const val IMAGE_SUFFIX = ".jpg"

class FileProvider @Inject constructor(@ApplicationContext private val context: Context) {

    private fun fileToExternalAppFolder(): File? = context.getExternalFilesDir(FOLDER_APP_ROOT)

    private fun fileToContentImageFolderAlsoMake(): File = File(fileToExternalAppFolder(), FOLDER_CONTENT).also {
        it.mkdirs()
    }

    fun contentImageFile(contentId: Int): File = File(fileToContentImageFolderAlsoMake(), contentImageFileName(contentId))

    fun contentImageRelativeFolderAlsoMake(): File = File(appRootRelativeFolder(), FOLDER_CONTENT).also { file ->
        file.mkdirs()
    }

    private fun contentImageAbsoluteFolderAlsoMake(): File = File(appRootAbsoluteFolder(), FOLDER_CONTENT).also { file ->
        file.mkdirs()
    }

    fun getContentImageAbsoluteFile(contentId: Int): File = File(contentImageAbsoluteFolderAlsoMake(), "${contentId}.jpg")

    private fun appRootAbsoluteFolder(): File = Environment.getExternalStoragePublicDirectory(appRootRelativeFolder().path)

    private fun appRootRelativeFolder(): File = File(Environment.DIRECTORY_DCIM + "/" + FOLDER_APP_ROOT)

    private fun contentImageFileName(contentId: Int) = "$contentId$IMAGE_SUFFIX"
}