package com.metigy.metigy_mobile.data.apimodels

import com.google.gson.annotations.SerializedName
import com.metigy.metigy_mobile.core.extension.DATE_TIME_PATTERN_APP_CONFIG
import com.metigy.metigy_mobile.core.extension.toInstant
import com.metigy.metigy_mobile.core.extension.toVersion
import com.metigy.metigy_mobile.core.utils.TimezoneProvider
import com.metigy.metigy_mobile.data.appmodels.AndroidAppConfigModel

data class AppConfigApiModel(
    @SerializedName("android")
    val androidConfig: Android? = null,
    val ios: Ios? = null
) {
    fun toAndroidAppConfigModel(timezoneProvider: TimezoneProvider): AndroidAppConfigModel {
        val appForceUpdate = androidConfig?.forceUpdate?.let { forceUpdate ->
            val version = forceUpdate.version.toVersion()
            if (version != null) {
                com.metigy.metigy_mobile.data.appmodels.ForceUpdate(
                    description = forceUpdate.description.orEmpty(),
                    title = forceUpdate.title.orEmpty(),
                    version = version
                )
            } else {
                null
            }
        }
        val appPrompt = androidConfig?.prompt?.let { prompt ->
            val dateTime = prompt.publishDate?.toInstant(DATE_TIME_PATTERN_APP_CONFIG, timezoneProvider)
            if (dateTime != null) {
                com.metigy.metigy_mobile.data.appmodels.Prompt(
                    description = prompt.description.orEmpty(),
                    publishDate = dateTime,
                    title = prompt.title.orEmpty()
                )
            } else {
                null
            }
        }
        return AndroidAppConfigModel(
            forceUpdate = appForceUpdate,
            prompt = appPrompt
        )
    }
}

data class Android(
    val forceUpdate: ForceUpdate? = null,
    val prompt: Prompt? = null
)

data class Ios(
    val forceUpdate: ForceUpdateX? = null,
    val prompt: PromptX? = null
)

data class ForceUpdate(
    val description: String? = null,
    val title: String? = null,
    val type: String? = null,
    val version: String? = null
)

data class Prompt(
    val description: String? = null,
    val publishDate: String? = null,
    val title: String? = null,
    val type: String? = null
)

data class ForceUpdateX(
    val description: String? = null,
    val title: String? = null,
    val type: String? = null,
    val version: String? = null
)

data class PromptX(
    val description: String? = null,
    val publishDate: String? = null,
    val title: String? = null,
    val type: String? = null
)