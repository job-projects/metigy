package com.metigy.metigy_mobile.data.platformpagesrepository

import com.metigy.metigy_mobile.core.extension.toAppError
import com.metigy.metigy_mobile.core.extension.toRequestBody
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.data.apiservice.ApiAction
import com.metigy.metigy_mobile.data.apiservice.ApiParamKey
import com.metigy.metigy_mobile.data.apiservice.ApiServiceForAppUrl
import com.metigy.metigy_mobile.data.appmodels.PlatformPageModel
import com.metigy.metigy_mobile.data.appmodels.SocialPlatform
import com.metigy.metigy_mobile.data.preferences.AppPreferences
import javax.inject.Inject

interface PlatformPagesRepository {
    suspend fun getPlatformPages(brandGroupId: Int): Result<List<PlatformPageModel>>
    suspend fun addPage(pageExternalIds: List<String>, brandId: Int, uuid: String, socialPlatform: SocialPlatform, brandGroupId: Int): Result<Unit>
    suspend fun removePage(pageId: Int, brandGroupId: Int): Result<Unit>
}

class DefaultPlatformPagesRepository @Inject constructor(
    private val apiService: ApiServiceForAppUrl,
    private val preferences: AppPreferences
) : PlatformPagesRepository {
    val token: String
        get() = preferences.getUserToken().orEmpty()

    override suspend fun getPlatformPages(brandGroupId: Int): Result<List<PlatformPageModel>> {
        val requestBody = platformPagesRequestBody(brandGroupId)
        return try {
            Result.Success(apiService.platformPages(authorization = token, platformPagesRequestBody = requestBody).result?.mapNotNull {
                it.toPlatformPage()
            } ?: listOf())
        } catch (e: Exception) {
            e.toAppError()
        }
    }

    private fun platformPagesRequestBody(brandGroupId: Int) = mapOf(
        ApiParamKey.ACTION to ApiAction.LIST.action,
        ApiParamKey.CONTEXT_BRAND_GROUP_ID to brandGroupId.toString()
    ).toRequestBody()

    override suspend fun addPage(pageExternalIds: List<String>, brandId: Int, uuid: String, socialPlatform: SocialPlatform, brandGroupId: Int): Result<Unit> {
        return try {
            apiService.addPage(
                authorization = token,
                addPageRequestBody(pageExternalIds = pageExternalIds, brandId = brandId, uuid = uuid, platform = socialPlatform, brandGroupId = brandGroupId)
            )
            Result.Success(Unit)
        } catch (e: Exception) {
            e.toAppError()
        }
    }

    override suspend fun removePage(pageId: Int, brandGroupId: Int): Result<Unit> {
        return try {
            apiService.removePage(authorization = token, removePageRequestBOdy(pageId = pageId, brandGroupId = brandGroupId))
            Result.Success(Unit)
        } catch (e: Exception) {
            e.toAppError()
        }
    }

    private fun addPageRequestBody(pageExternalIds: List<String>, brandId: Int, uuid: String, platform: SocialPlatform, brandGroupId: Int) = mapOf(
        ApiParamKey.ACTION to ApiAction.ADD.action,
        ApiParamKey.PAGE_EXTERNAL_IDS to pageExternalIds.toString(),
        ApiParamKey.BRAND_ID to brandId.toString(),
        ApiParamKey.UUID to uuid,
        ApiParamKey.PLATFORM to platform.toSocialPlatformForApi().platformName,
        ApiParamKey.CONTEXT_BRAND_GROUP_ID to brandGroupId.toString()
    ).toRequestBody()

    private fun removePageRequestBOdy(pageId: Int, brandGroupId: Int) = mapOf(
        ApiParamKey.ACTION to ApiAction.DELETE_TOKEN.action,
        ApiParamKey.ID to pageId.toString(),
        ApiParamKey.CONTEXT_BRAND_GROUP_ID to brandGroupId.toString()
    ).toRequestBody()
}