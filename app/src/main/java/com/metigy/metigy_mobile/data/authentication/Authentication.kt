package com.metigy.metigy_mobile.data.authentication

import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.consts.TAG_AUTHENTICATION
import com.metigy.metigy_mobile.core.extension.jwtPrefix
import com.metigy.metigy_mobile.core.extension.toAppError
import com.metigy.metigy_mobile.core.extension.toRequestBody
import com.metigy.metigy_mobile.core.extension.toUriWrapper
import com.metigy.metigy_mobile.core.result.Result
import com.metigy.metigy_mobile.core.utils.TimeProvider
import com.metigy.metigy_mobile.core.utils.UrlProvider
import com.metigy.metigy_mobile.data.apiservice.ApiAction
import com.metigy.metigy_mobile.data.apiservice.ApiParamKey
import com.metigy.metigy_mobile.data.apiservice.ApiPlan
import com.metigy.metigy_mobile.data.apiservice.ApiServiceForAppUrl
import com.metigy.metigy_mobile.data.appcache.AppCache
import com.metigy.metigy_mobile.data.appmodels.AuthenticationRequirementStatus
import com.metigy.metigy_mobile.data.appmodels.UserCredentialsModel
import com.metigy.metigy_mobile.data.appmodels.UserInfoModel
import com.metigy.metigy_mobile.data.preferences.AppPreferences
import com.metigy.metigy_mobile.data.session.Session
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.RequestBody
import timber.log.Timber
import java.net.HttpURLConnection
import java.time.Instant
import javax.inject.Inject

interface Authentication {
    val token: String?
    fun isUserLoggedInAtLaunch(): Boolean
    suspend fun login(email: String, password: String): Result<String>
    suspend fun reAuthenticate(): Result<String>
    fun saveToken(token: String)
    fun saveLoginCredentials(loginCredentials: UserCredentialsModel)
    fun clearLoginCredentials()
    fun cacheSignUpCredentials(signUpCredentials: UserCredentialsModel)
    fun getCachedSignUpCredentials(): UserCredentialsModel?
    fun clearSignUpCredentialsCache()
    fun authenticationStatus(): AuthenticationRequirementStatus
    fun lastAuthenticationDate(): Instant?
    suspend fun logout()
    suspend fun signUp(firstName: String, lastName: String, email: String, password: String): Result<UserInfoModel>
    suspend fun getVerificationKey(url: String): Result<String>
    suspend fun verifyEmail(key: String, token: String): Result<Unit>
    suspend fun resendEmail(token: String, email: String): Result<Unit>
    suspend fun resetPassword(email: String): Result<Unit>
    fun setIsPreviouslyLoggedIn()
}

class DefaultAuthentication @Inject constructor(
    private val apiService: ApiServiceForAppUrl,
    private val preferences: AppPreferences,
    private val appCache: AppCache,
    private val session: Session,
    private val timeProvider: TimeProvider,
    private val urlProvider: UrlProvider
) : Authentication {

    override val token: String?
        get() = preferences.getUserToken()

    //If user do not check the Remember Me checkbox while logging in, they will be considered locally logged in until they exit the app. After exiting the app they
    //would be considered as locally logged out. This function checks if the user is locally logged in at launch time (as launch always would be the next step after
    //exiting the app)
    override fun isUserLoggedInAtLaunch(): Boolean = token != null && preferences.getUserCredentials() != null

    override suspend fun login(email: String, password: String): Result<String> =
        try {
            val loginResponse = apiService.login(loginRequestBody(email = email, password = password))
            loginResponse.result?.scj?.let { token ->
                Result.Success(token.jwtPrefix())
            } ?: Result.Error(messageId = R.string.log_in_failed)
        } catch (e: Exception) {
            e.toAppError()
        }

    override suspend fun reAuthenticate(): Result<String> = preferences.getUserCredentials()?.let { userCredentials ->
        login(email = userCredentials.userName, password = userCredentials.password)
    } ?: Result.Error(Exception("Unknown"))


    override fun saveToken(token: String) {
        preferences.saveUserToken(token)
        preferences.saveLastAuthenticationDate(timeProvider.now())
        session.onLogin()
    }

    override fun saveLoginCredentials(loginCredentials: UserCredentialsModel) {
        preferences.saveUserCredential(userCredentials = loginCredentials)
    }

    override fun clearLoginCredentials() {
        preferences.clearCredentials()
    }

    override fun cacheSignUpCredentials(signUpCredentials: UserCredentialsModel) {
        appCache.saveSignUpCredentials(signUpCredentials)
    }

    override fun getCachedSignUpCredentials(): UserCredentialsModel? = appCache.getSignUpCredentials()

    override fun clearSignUpCredentialsCache() {
        appCache.clearSignUpCredentials()
    }

    override suspend fun logout() {
        val tempToken = token.orEmpty()
        preferences.clearToken()
        preferences.setPushInfoSent(false)
        try {
            apiService.logout(authorization = tempToken)
        } catch (e: Exception) {
            Timber.tag(TAG_AUTHENTICATION).e(e, "Logout failed (api)")
        }
    }

    override fun lastAuthenticationDate(): Instant? = preferences.getLastAuthenticationDate()

    override fun authenticationStatus(): AuthenticationRequirementStatus {
        val credentials = preferences.getUserCredentials()
        return if (credentials != null) {
            if (token != null) {
                AuthenticationRequirementStatus.AuthenticationNotNeeded.also {
                    Timber.tag(TAG_AUTHENTICATION).i("No Authentication is needed")
                }
            } else {
                AuthenticationRequirementStatus.AuthenticationNeededCredentialsAvailable(credentials).also {
                    Timber.tag(TAG_AUTHENTICATION).i("Authentication is needed. Credentials are saved")
                }
            }
        } else {
            AuthenticationRequirementStatus.AuthenticationNeededCredentialsNotAvailable.also {
                Timber.tag(TAG_AUTHENTICATION).i("Authentication is needed. No credentials available")
            }
        }
    }

    override suspend fun signUp(firstName: String, lastName: String, email: String, password: String): Result<UserInfoModel> =
        try {
            apiService.signUp(
                signUpRequestBody(firstName, lastName, email, password)
            ).result?.toUserInfoModel()?.let {
                Result.Success(it)
            } ?: Result.Error(messageId = R.string.unknown_error)

        } catch (e: Exception) {
            e.toAppError()
        }

    override suspend fun getVerificationKey(url: String): Result<String> =
        withContext(Dispatchers.IO) {
            try {
                val con: HttpURLConnection = urlProvider.url(url).openConnection() as HttpURLConnection
                con.requestMethod = VERIFICATION_KEY_REQUEST_METHOD

                con.instanceFollowRedirects = true

                con.connect()
                val key = con.getHeaderField(VERIFICATION_KEY_HEADER_FIELD).toUriWrapper().getQueryParameter(VERIFICATION_KEY_QUERY_PARAM).orEmpty()
                Result.Success(key)
            } catch (e: Exception) {
                Timber.tag(TAG_AUTHENTICATION).e(e)
                e.toAppError()
            }
        }

    override suspend fun verifyEmail(key: String, token: String): Result<Unit> =
        try {
            Result.Success(apiService.verifyEmail(authorization = token, verifyEmailBody = verifyEmailRequestBody(key)))
        } catch (e: Exception) {
            e.toAppError()
        }


    override suspend fun resendEmail(token: String, email: String): Result<Unit> =
        try {
            Result.Success(apiService.resendEmail(authorization = token, resendEmailBody = resendEmailRequestBody(email = email)))
        } catch (e: Exception) {
            e.toAppError()
        }

    override suspend fun resetPassword(email: String): Result<Unit> =
        try {
            Result.Success(apiService.resetPassword(resetPasswordRequestBody(email = email)))
        } catch (e: Exception) {
            e.toAppError()
        }

    override fun setIsPreviouslyLoggedIn() {
        preferences.setIsPreviouslyLoggedIn()
    }

    private fun loginRequestBody(email: String, password: String): RequestBody = mapOf(
        ApiParamKey.ACTION to ApiAction.LOGIN.action,
        ApiParamKey.EMAIL_KEY to email,
        ApiParamKey.PASSWORD_KEY to password
    ).toRequestBody()

    private fun signUpRequestBody(firstName: String, lastName: String, email: String, password: String): RequestBody = mapOf(
        ApiParamKey.ACTION to ApiAction.REGISTER.action,
        ApiParamKey.FIRST_NAME to firstName,
        ApiParamKey.LAST_NAME to lastName,
        ApiParamKey.EMAIL_KEY to email,
        ApiParamKey.PASSWORD_KEY to password,
        ApiParamKey.PLAN to ApiPlan.FREE.type
    ).toRequestBody()

    private fun verifyEmailRequestBody(key: String) = mapOf(
        ApiParamKey.ACTION to ApiAction.VERIFY.action,
        ApiParamKey.KEY to key
    ).toRequestBody()

    private fun resendEmailRequestBody(email: String) = mapOf(
        ApiParamKey.ACTION to ApiAction.RESEND_EMAIL_CODE.action,
        ApiParamKey.EMAIL_KEY to email
    ).toRequestBody()

    private fun resetPasswordRequestBody(email: String) = mapOf(
        ApiParamKey.ACTION to ApiAction.RESET_PASSWORD.action,
        ApiParamKey.EMAIL_KEY to email
    ).toRequestBody()

    companion object {
        const val VERIFICATION_KEY_REQUEST_METHOD = "GET"
        const val VERIFICATION_KEY_HEADER_FIELD = "Location"
        const val VERIFICATION_KEY_QUERY_PARAM = "key"
    }
}