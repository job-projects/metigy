package com.metigy.metigy_mobile.testUtils.mockapiresponses

object MockResponseBody {
    fun signupSuccessful() = MockResponseFileReader.readFile(SIGNUP_SUCCESSFUL)

    fun changePasswordSuccessful() = MockResponseFileReader.readFile(FORGOT_PASSWORD_SUCCESSFUL)

    fun signInSuccessful() = MockResponseFileReader.readFile(SIGN_IN_SUCCESSFUL)

    fun userSuccessful() = MockResponseFileReader.readFile(USER_SUCCESSFUL)

    fun listBrandGroupSuccessful() = MockResponseFileReader.readFile(LIST_BRAND_GROUP_SUCCESSFUL)

    fun listPlatformPages() = MockResponseFileReader.readFile(LIST_PLATFORM_PAGES_SUCCESSFUL)

    private const val SIGNUP_SUCCESSFUL = "mockresponses/signup_successful"
    private const val FORGOT_PASSWORD_SUCCESSFUL = "mockresponses/forgot_password_successful"
    private const val SIGN_IN_SUCCESSFUL = "mockresponses/sign_in_successful"
    private const val USER_SUCCESSFUL = "mockresponses/user_successful"
    private const val LIST_BRAND_GROUP_SUCCESSFUL = "mockresponses/list_brand_groups_successful"
    private const val LIST_PLATFORM_PAGES_SUCCESSFUL = "mockresponses/platform_pages_successful"

}