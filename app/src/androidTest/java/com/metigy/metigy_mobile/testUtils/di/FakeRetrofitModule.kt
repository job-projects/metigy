package com.metigy.metigy_mobile.testUtils.di

import com.metigy.metigy_mobile.core.di.ApplicationModule
import com.metigy.metigy_mobile.core.di.RetrofitModule
import com.metigy.metigy_mobile.testUtils.mockapiresponses.MockWebServerAddress
import dagger.Module
import dagger.Provides
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
@TestInstallIn(
    components = [SingletonComponent::class],
    replaces = [RetrofitModule::class]
)
class FakeRetrofitModule {

    @Singleton
    @Provides
    @Named(ApplicationModule.NAMED_RETROFIT_APP)
    fun provideAppUrlRetrofit(client: OkHttpClient, gsonConverterFactory: GsonConverterFactory): Retrofit {
        return Retrofit.Builder()
            .baseUrl(MockWebServerAddress.address)
            .addConverterFactory(gsonConverterFactory)
            .client(client)
            .build()
    }

    @Singleton
    @Provides
    @Named(ApplicationModule.NAMED_RETROFIT_SERVICE)
    fun provideServiceUrlRetrofit(client: OkHttpClient, gsonConverterFactory: GsonConverterFactory): Retrofit {
        return Retrofit.Builder()
            .baseUrl(MockWebServerAddress.address)
            .addConverterFactory(gsonConverterFactory)
            .client(client)
            .build()
    }

}