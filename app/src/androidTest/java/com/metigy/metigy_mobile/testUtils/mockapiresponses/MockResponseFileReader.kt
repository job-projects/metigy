package com.metigy.metigy_mobile.testUtils.mockapiresponses

import java.io.InputStreamReader

object MockResponseFileReader {
    fun readFile(path: String): String {
        val reader = InputStreamReader(this.javaClass.classLoader!!.getResourceAsStream(path))
        return reader.readText().also {
            reader.close()
        }
    }
}
