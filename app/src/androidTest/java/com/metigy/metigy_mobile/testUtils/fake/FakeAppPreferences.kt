package com.metigy.metigy_mobile.testUtils.fake

import com.metigy.metigy_mobile.data.appmodels.UserCredentialsModel
import com.metigy.metigy_mobile.data.appmodels.UserInfoModel
import com.metigy.metigy_mobile.data.preferences.AppPreferences
import java.time.Instant

class FakeAppPreferences : AppPreferences {

    companion object {
        var isPreviouslyLoggedIn = false
        var userCredentialsModel: UserCredentialsModel? = null
        var userToken: String? = null
        var lastAuthenticationDate: Instant? = null
        var pushInfoSent = false
        var pushChannelId: String? = null
        var userInfoModel: UserInfoModel? = null
    }

    override fun saveUserToken(token: String) {
        userToken = token
    }

    override fun getUserToken(): String? = userToken

    override fun clearToken() {
        userToken = null
    }

    override fun saveLastAuthenticationDate(instant: Instant) {
        lastAuthenticationDate = instant
    }

    override fun getLastAuthenticationDate(): Instant? = lastAuthenticationDate

    override fun clearLastAuthenticationDate() {
        lastAuthenticationDate = null
    }

    override fun saveUserCredential(userCredentials: UserCredentialsModel) {
        userCredentialsModel = userCredentials
    }

    override fun getUserCredentials(): UserCredentialsModel? = userCredentialsModel

    override fun clearCredentials() {
        userCredentialsModel = null
    }

    override fun saveUserInfo(userInfo: UserInfoModel) {
        userInfoModel = userInfo
    }

    override fun getUserInfo(): UserInfoModel? = userInfoModel

    override fun clearUserInfo() {
        userInfoModel = null
    }

    override fun savePushChannelId(pushToken: String) {
        pushChannelId = pushToken
    }

    override fun getPushChannelId(): String? = pushChannelId

    override fun setPushInfoSent(isSent: Boolean) {
        pushInfoSent = isSent
    }

    override fun isPushInfoSent(): Boolean = pushInfoSent

    override fun setLatestPromptDate(instant: Instant) {
        TODO("Not yet implemented")
    }

    override fun getLatestPromptDate(): Instant? {
        TODO("Not yet implemented")
    }

    override fun clearLatestPromptDate() {
        TODO("Not yet implemented")
    }

    override fun getIsPreviouslyLoggedIn() = isPreviouslyLoggedIn

    override fun setIsPreviouslyLoggedIn() {
        isPreviouslyLoggedIn = true
    }
}