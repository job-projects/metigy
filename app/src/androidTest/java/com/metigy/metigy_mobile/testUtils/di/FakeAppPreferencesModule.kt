package com.metigy.metigy_mobile.testUtils.di

import com.metigy.metigy_mobile.core.di.AppPreferenceModule
import com.metigy.metigy_mobile.data.preferences.AppPreferences
import com.metigy.metigy_mobile.testUtils.fake.FakeAppPreferences
import dagger.Module
import dagger.Provides
import dagger.hilt.components.SingletonComponent
import dagger.hilt.testing.TestInstallIn
import javax.inject.Singleton

@Module
@TestInstallIn(
    components = [SingletonComponent::class],
    replaces = [AppPreferenceModule::class]
)
class FakeAppPreferencesModule {
    @Provides
    @Singleton
    fun provideAppPreference(): AppPreferences = FakeAppPreferences()
}