package com.metigy.metigy_mobile.testUtils.mockapiresponses

object MockWebServerAddress {
    private const val BASE_URL = "http://127.0.0.1"
    const val PORT = 8080

    val address = "$BASE_URL:$PORT"
}