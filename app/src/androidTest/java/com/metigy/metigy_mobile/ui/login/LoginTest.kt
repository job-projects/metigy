package com.metigy.metigy_mobile.ui.login

import android.content.Intent
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.data.appmodels.UserCredentialsModel
import com.metigy.metigy_mobile.testUtils.fake.FakeAppPreferences
import com.metigy.metigy_mobile.testUtils.mockapiresponses.MockResponseBody
import com.metigy.metigy_mobile.testUtils.mockapiresponses.MockWebServerAddress
import com.metigy.metigy_mobile.ui.hostactivities.RegistrationActivity
import com.metigy.metigy_mobile.ui.main.MainActivity
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.core.IsNot.not
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
@LargeTest
class LoginTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    lateinit var activityScenario: ActivityScenario<RegistrationActivity>

    private val intent = Intent(ApplicationProvider.getApplicationContext(), RegistrationActivity::class.java)

    private lateinit var server: MockWebServer

    @Before
    fun setUp() {
        hiltRule.inject()
        server = MockWebServer()
        server.start(MockWebServerAddress.PORT)
        Intents.init()
    }

    @After
    fun tearDown() {
        server.shutdown()
        Intents.release()
        activityScenario.close()
    }

    /*
        As an existing user, I'd like to login to my Metigy account so that I can pick up where I left off with my content

        Given I have downloaded the Metigy app,
        When I open the app for the first time,
        Then by default I see the splash screen fade in,
        And then I'm introduced to and can swipe through the 4 intro screens

        When I swipe past the last intro screen,
        Then by default, I land into the login screen

        When I tap on Log in
        Then I'm redirected into the app experience and land into my home screen
        And then I receive a push notification opt in popup (@Sam to review and validate flow for sign in)
        */
    @Test
    fun login_existing_user() = runBlocking {
        FakeAppPreferences.isPreviouslyLoggedIn = false
        activityScenario = ActivityScenario.launch(intent)

        onView(withId(R.id.introViewPager)).perform(swipeLeft(), swipeLeft(), swipeLeft(), swipeLeft())

        delay(1000)

        onView(withId(R.id.userNameEditText)).perform(typeText("rahul.chhetri@metigy.com"))
        Espresso.closeSoftKeyboard()

        onView(withId(R.id.passwordEditText)).perform(typeText("Password1"))
        Espresso.closeSoftKeyboard()

        server.enqueue(MockResponse().setResponseCode(200).setBody(MockResponseBody.signInSuccessful()))
        server.enqueue(MockResponse().setResponseCode(200).setBody(MockResponseBody.userSuccessful()))

        onView(withId(R.id.loginButton)).perform(click())

        intended(hasComponent(MainActivity::class.java.name))
    }


    /*
    When I return to the Metigy app,
    Then I can see my email pre-filled and my password inputted (but hidden)
    And I [optional] can tap to toggle to view and unhide the password if needed
    */
    @Test
    fun login_remember_me_selected() = runBlocking {
        FakeAppPreferences.isPreviouslyLoggedIn = false
        intent.putExtra(RegistrationActivity.BUNDLE_KEY_USER_CREDENTIALS, UserCredentialsModel("rahul.chhetri@metigy.com", "Password1"))
        activityScenario = ActivityScenario.launch(intent)

        onView(withId(R.id.introViewPager)).perform(swipeLeft(), swipeLeft(), swipeLeft(), swipeLeft())

        server.enqueue(MockResponse().setResponseCode(200).setBody(MockResponseBody.signInSuccessful()))
        server.enqueue(MockResponse().setResponseCode(200).setBody(MockResponseBody.userSuccessful()))

        onView(withId(R.id.loginButton)).perform(click())

        intended(hasComponent(MainActivity::class.java.name))
    }

    /*
    When I enter an invalid email address,
    I am presented with an inline error message: "Please enter a valid email address".
    When I leave the password field blank,
    I am presented with an inline error message: "Password must not be blank".
    */
    @Test
    fun login_validate_error_message(): Unit = runBlocking {
        FakeAppPreferences.isPreviouslyLoggedIn = true
        activityScenario = ActivityScenario.launch(intent)

        delay(500)

        onView(withId(R.id.userNameEditText)).perform(typeText("username"))

        onView(withText(R.string.error_invalid_email)).check(matches(isDisplayed()))

        onView(withId(R.id.userNameEditText)).perform(typeText("test@email.com"))

        onView(withText(R.string.error_invalid_email)).check(doesNotExist())

        onView(withId(R.id.passwordEditText)).perform(clearText())

        onView(withText(R.string.error_blank_password)).check(matches(isDisplayed()))

        onView(withId(R.id.passwordEditText)).perform(typeText("pass"))

        onView(withText(R.string.error_blank_password)).check(doesNotExist())
    }

    /*
    Test to check button is enabled only when email and password is valid
    */
    @Test
    fun enable_disable_login_button(): Unit = runBlocking {
        FakeAppPreferences.isPreviouslyLoggedIn = true
        activityScenario = ActivityScenario.launch(intent)

        delay(500)

        onView(withId(R.id.loginButton)).check(matches(not(isEnabled())))

        onView(withId(R.id.userNameEditText)).perform(typeText("test"))

        onView(withId(R.id.loginButton)).check(matches(not(isEnabled())))

        onView(withId(R.id.userNameEditText)).perform(typeText("test@email.com"))

        onView(withId(R.id.loginButton)).check(matches(not(isEnabled())))

        onView(withId(R.id.passwordEditText)).perform(typeText("pass"))

        onView(withId(R.id.loginButton)).check(matches(isEnabled()))

        onView(withId(R.id.passwordEditText)).perform(clearText())

        onView(withId(R.id.loginButton)).check(matches(not(isEnabled())))
    }
}