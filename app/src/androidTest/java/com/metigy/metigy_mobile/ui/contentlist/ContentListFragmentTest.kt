package com.metigy.metigy_mobile.ui.contentlist

import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.extension.orFalse
import com.metigy.metigy_mobile.testUtils.launchFragmentInHiltContainer
import com.metigy.metigy_mobile.testUtils.mockapiresponses.MockResponseBody
import com.metigy.metigy_mobile.testUtils.mockapiresponses.MockWebServerAddress
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class ContentListFragmentTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    private lateinit var server: MockWebServer

    @Before
    fun setUp() {
        hiltRule.inject()
        server = MockWebServer()
        server.start(MockWebServerAddress.PORT)
    }

    @After
    fun tearDown() {
        server.shutdown()
    }

    /*
        Given I login using shelton.han@metigy.com
        And I land on content Preview list view
        And I click view header
        And I see selected brand group is "Sancheng Hotpot King"
        When I click view header
        And I click "Sancheng Hotpot King" cell
        And I select "Shelton Han" cell
        And I click view header
        Then I see selected brand group is "Shelton Han"
        */
    @Test
    fun test_brand_group_selection(): Unit = runBlocking {
        server.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                // Returning response for brand groups when api/user is called else returning success response with empty body
                if (request.path?.contains("api/user").orFalse()) {
                    return MockResponse().setResponseCode(200).setBody(MockResponseBody.listBrandGroupSuccessful())
                }
                return MockResponse().setResponseCode(200).setBody("{}")
            }
        }

        launchFragmentInHiltContainer<ContentListFragment> { }

        onView(withId(R.id.spinner)).perform(click())

        delay(1000)

        onView(withText("Sancheng Hotpot King")).check(matches(isDisplayed()))

        onView(withText("Sancheng Hotpot King")).perform(click())

        delay(1000)

        onView(withText("Shelton Han")).perform(click())

        Espresso.pressBack()

        onView(withId(R.id.spinner)).perform(click())

        delay(1000)

        onView(withText("Shelton Han")).check(matches(isDisplayed()))
    }
}