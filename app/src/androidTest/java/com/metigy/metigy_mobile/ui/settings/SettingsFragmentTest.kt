package com.metigy.metigy_mobile.ui.settings

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.swipeUp
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.testUtils.launchFragmentInHiltContainer
import com.metigy.metigy_mobile.ui.launcher.LauncherActivity
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class SettingsFragmentTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @Before
    fun init() {
        hiltRule.inject()
    }

    /*
    Scenario: User is able to logout
    Given: User is on the Settings screen
    When: User press on logout button
    Then: User should be taken to to Splash screen
    */
    @Test
    fun logout() = runBlocking {
        launchFragmentInHiltContainer<SettingsFragment> {
        }

        Intents.init()

        onView(withId(R.id.nestedScroll)).perform(swipeUp())
        delay(1000)
        onView(withId(R.id.logoutLayout)).perform(click())

        intended(hasComponent(LauncherActivity::class.java.name))

        Intents.release()
    }
}