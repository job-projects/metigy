package com.metigy.metigy_mobile.ui.signup

import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.testUtils.launchFragmentInHiltContainer
import com.metigy.metigy_mobile.testUtils.mockapiresponses.MockResponseBody
import com.metigy.metigy_mobile.testUtils.mockapiresponses.MockWebServerAddress
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class SignUpFragmentTest {

    @get:Rule(order = 0)
    val hiltRule = HiltAndroidRule(this)

    private lateinit var server: MockWebServer

    @Before
    fun setUp() {
        hiltRule.inject()
        server = MockWebServer()
        server.start(MockWebServerAddress.PORT)
    }

    @After
    fun tearDown() {
        server.shutdown()
    }

    /*
    Scenario: User is able to create an account
    Given: I am on Sign Up screen
    When: I enter first name, last name, valid email and valid password
    And: I click on ‘Create free account’ button
    Then: I should be taken to new screen that has message and instructions
    */
    @Test
    fun signup_success_navigates_to_verification_fragment() = runBlocking {

        val navController = mockk<NavController>()

        launchFragmentInHiltContainer<SignUpFragment> {
            Navigation.setViewNavController(requireView(), navController)
        }

        server.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(MockResponseBody.signupSuccessful())
        )

        onView(withId(R.id.firstNameEditText)).perform(typeText("Jon"))
        onView(withId(R.id.lastNameEditText)).perform(typeText("Demo"))

        Espresso.pressBack()
        onView(withId(R.id.emailAddressEditText)).perform(typeText("jon3@email.com"))

        Espresso.pressBack()
        onView(withId(R.id.passwordEditText)).perform(typeText("Password1"))

        Espresso.pressBack()
        onView(withId(R.id.signUpButton)).perform(click())

        verify {
            navController.navigate(SignUpFragmentDirections.toEmailVerificationFragment())
        }
    }
}