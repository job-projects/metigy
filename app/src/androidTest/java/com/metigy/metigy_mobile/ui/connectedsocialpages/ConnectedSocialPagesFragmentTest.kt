package com.metigy.metigy_mobile.ui.connectedsocialpages

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.core.extension.orFalse
import com.metigy.metigy_mobile.testUtils.launchFragmentInHiltContainer
import com.metigy.metigy_mobile.testUtils.mockapiresponses.MockResponseBody
import com.metigy.metigy_mobile.testUtils.mockapiresponses.MockWebServerAddress
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class ConnectedSocialPagesFragmentTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    private lateinit var server: MockWebServer

    @Before
    fun setUp() {
        hiltRule.inject()
        server = MockWebServer()
        server.start(MockWebServerAddress.PORT)
    }

    @After
    fun tearDown() {
        server.shutdown()
    }

    /*
    Given I land on Connected Pages screen
    When there are 3 pages as connected
    Then I see the text view on top of the list is "Connected (3)"
    */
    @Test
    fun connected_pages_count() = runBlocking {
        server.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when {
                    request.path?.contains("api/user").orFalse() -> {
                        MockResponse().setResponseCode(200).setBody(MockResponseBody.listBrandGroupSuccessful())
                    }
                    request.path?.contains("api/platformPage").orFalse() -> {
                        MockResponse().setResponseCode(200).setBody(MockResponseBody.listPlatformPages())
                    }
                    else -> {
                        MockResponse().setResponseCode(200).setBody("{}")
                    }
                }
            }
        }

        launchFragmentInHiltContainer<ConnectedSocialPagesFragment> { }
        onView(withId(R.id.connectedPagesCountTextView)).check(matches(withText("Connected (3)")))
        delay(1000)
    }
}