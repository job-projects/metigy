package com.metigy.metigy_mobile.ui.intro

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.swipeLeft
import androidx.test.espresso.action.ViewActions.swipeRight
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.testUtils.fake.FakeAppPreferences
import com.metigy.metigy_mobile.testUtils.launchFragmentInHiltContainer
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class IntroFragmentTest {

    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @Before
    fun setUp() {
        hiltRule.inject()
    }

    /*
        Scenario: User is able to swipe to left to see different intro text
        Given : I land on intro view
        When : I swipe left
        Then : Text should be "Create, plan and schedule Facebook and Instagram posts on your desktop app"
        */
    @Test
    fun verify_pages_texts_in_intro_not_logged_in_previously() {
        FakeAppPreferences.isPreviouslyLoggedIn = false
        launchFragmentInHiltContainer<IntroFragment> {}

        onView(withText(R.string.intro_one_heading)).check(matches(isDisplayed()))
        onView(withText(R.string.intro_one_body)).check(matches(isDisplayed()))

        onView(withId(R.id.introViewPager)).perform(swipeLeft())

        onView(withText(R.string.intro_two_heading)).check(matches(isDisplayed()))
        onView(withText(R.string.intro_two_body)).check(matches(isDisplayed()))

        onView(withId(R.id.introViewPager)).perform(swipeLeft())

        onView(withText(R.string.intro_three_heading)).check(matches(isDisplayed()))
        onView(withText(R.string.intro_three_body)).check(matches(isDisplayed()))

        onView(withId(R.id.introViewPager)).perform(swipeLeft())

        onView(withText(R.string.intro_four_heading)).check(matches(isDisplayed()))
        onView(withText(R.string.intro_four_body)).check(matches(isDisplayed()))

        onView(withId(R.id.introViewPager)).perform(swipeLeft())

        onView(withText(R.string.login_to_your_metigy_account)).check(matches(isDisplayed()))
        onView(withText(R.string.log_in)).check(matches(isDisplayed()))

        onView(withId(R.id.introViewPager)).perform(swipeRight())

        onView(withText(R.string.intro_four_heading)).check(matches(isDisplayed()))
        onView(withText(R.string.intro_four_body)).check(matches(isDisplayed()))
    }

    /*
    Given I have signed in previously via the Metigy app,
    When I open the app next time,
    Then by default I see the splash screen fade in,
    And I land directly into the login screen
    Then, if I swipe back, I can revisit the intro screens if needed
    */
    @Test
    fun verify_pages_texts_in_intro_logged_in_previously() {
        FakeAppPreferences.isPreviouslyLoggedIn = true
        launchFragmentInHiltContainer<IntroFragment> {}

        onView(withText(R.string.login_to_your_metigy_account)).check(matches(isDisplayed()))
        onView(withText(R.string.log_in)).check(matches(isDisplayed()))

        onView(withId(R.id.introViewPager)).perform(swipeRight())

        onView(withText(R.string.intro_four_heading)).check(matches(isDisplayed()))
        onView(withText(R.string.intro_four_body)).check(matches(isDisplayed()))
    }
}