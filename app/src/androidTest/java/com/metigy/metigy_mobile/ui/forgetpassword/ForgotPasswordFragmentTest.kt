package com.metigy.metigy_mobile.ui.forgetpassword

import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.metigy.metigy_mobile.R
import com.metigy.metigy_mobile.testUtils.launchFragmentInHiltContainer
import com.metigy.metigy_mobile.testUtils.mockapiresponses.MockResponseBody
import com.metigy.metigy_mobile.testUtils.mockapiresponses.MockWebServerAddress
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import io.mockk.mockk
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.Matchers.not
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class ForgotPasswordFragmentTest {

    @get:Rule(order = 0)
    val hiltRule = HiltAndroidRule(this)

    private lateinit var server: MockWebServer

    @Before
    fun setUp() {
        hiltRule.inject()
        server = MockWebServer()
        server.start(MockWebServerAddress.PORT)
    }

    @After
    fun tearDown() {
        server.shutdown()
    }

    /*
    Scenario: User is able to request for a new password
    Given: I am on Forgot Password screen
    When: I enter valid email
    And: I click on ’Submit’ button
    Then: I should be taken to new screen that has message and instructions
    */
    @Test
    fun forgot_password() {
        val navController = mockk<NavController>()

        launchFragmentInHiltContainer<ForgotPasswordFragment> {
            Navigation.setViewNavController(requireView(), navController)
        }

        server.enqueue(MockResponse().setResponseCode(200).setBody(MockResponseBody.changePasswordSuccessful()))

        onView(withId(R.id.emailEditText)).perform(typeText("rahul.chhetri@metigy.com"))

        Espresso.closeSoftKeyboard()

        onView(withId(R.id.resetPasswordButton)).perform(click())

        onView(withId(R.id.resetPasswordMessageTextView)).check(matches(isDisplayed()))
        onView(withId(R.id.doneButton)).check(matches(isDisplayed()))
        onView(withId(R.id.emailTextInputLayout)).check(matches(not(isDisplayed())))
    }
}